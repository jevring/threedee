/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.primitives.Point3D;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;

/**
 * Checks that all vectors are coherent.
 *
 * @author markus@jevring.net
 */
public class CameraTest {
	@Test
	public void testNoRotation() throws Exception {
		Camera c = new Camera(0, 0, 0, 0, 0, 0);
		assertEquals(new Point3D(0, 0, -1), c.normal);
	}

	@Test
	public void testOriginNoRotation() throws Exception {
		assertVectorsOrthogonal(new Camera(0, 0, 0, 0, 0, 0));
	}

	@Test
	public void testOriginRotation() throws Exception {
		assertVectorsOrthogonal(new Camera(0, 0, 0, 98, 27, 355));
	}

	@Test
	public void testNonOriginNoRotation() throws Exception {
		assertVectorsOrthogonal(new Camera(98, 27, 355, 0, 0, 0));
	}

	@Test
	public void testNonOriginRotation() throws Exception {
		assertVectorsOrthogonal(new Camera(98, 27, 355, 98, 27, 355));
	}

	private void assertVectorsOrthogonal(Camera c) {
		assertEquals("Normal isn't orthogonal with up", 0.0, c.normal.dotProduct(c.up), 0.00000001); 
		assertEquals("Normal isn't orthogonal with right", 0.0, c.normal.dotProduct(c.right), 0.00000001);
		assertEquals("Up isn't orthogonal with right", 0.0, c.up.dotProduct(c.right), 0.00000001);
		Point3D calculatedRight = c.normal.crossProduct(c.up);
		assertEquals(c.right.x, calculatedRight.x, 0.00000001);
		assertEquals(c.right.y, calculatedRight.y, 0.00000001);
		assertEquals(c.right.z, calculatedRight.z, 0.00000001);
	}
}
