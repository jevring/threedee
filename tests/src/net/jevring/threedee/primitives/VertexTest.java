/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.primitives;

import org.junit.Test;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Tests the methods in the {@link net.jevring.threedee.primitives.Vertex} class.
 *
 * @author markus@jevring.net
 */
public class VertexTest {
	private Plane plane;
	private Vertex a;
	private Vertex b;
	private Vertex.Intersection intersection;
	
	@Test
	public void testIntersectionsFloor() throws Exception {
		// here the plane lies flat on the "floor", "pointing" "up" towards positive y values
		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 1, 0));
		a = new Vertex(new Point3D(0, -1, 1));
		b = new Vertex(new Point3D(0, 1, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertTrue(intersection.intersects());
		assertEquals(new Point3D(0, 0, 1), intersection.getVertex().p);
		
		// lets raise the plane a bit up off of the floor
		plane = new Plane(new Point3D(0, 0.5, 0), new Point3D(0, 1, 0));
		a = new Vertex(new Point3D(0, -1, 1));
		b = new Vertex(new Point3D(0, 1, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertTrue(intersection.intersects());
		assertEquals(new Point3D(0, 0.5, 1), intersection.getVertex().p);
		
		// down below the floor
		plane = new Plane(new Point3D(0, -0.5, 0), new Point3D(0, 1, 0));
		a = new Vertex(new Point3D(0, -1, 1));
		b = new Vertex(new Point3D(0, 1, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertTrue(intersection.intersects());
		assertEquals(new Point3D(0, -0.5, 1), intersection.getVertex().p);
		
	}

	@Test
	public void testIntersectionsWall() throws Exception {
		// here the plane "stands up" along the y axis, pointing towards the x axis
		plane = new Plane(new Point3D(0, 0, 0), new Point3D(1, 0, 0));
		a = new Vertex(new Point3D(-1, 0, 0));
		b = new Vertex(new Point3D(1, 0, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertTrue(intersection.intersects());
		assertEquals(new Point3D(0, 0, 0), intersection.getVertex().p);

		// move the plane to the right (along the x-axis)
		plane = new Plane(new Point3D(0.5, 0, 0), new Point3D(1, 0, 0));
		a = new Vertex(new Point3D(-1, 0, 0));
		b = new Vertex(new Point3D(1, 0, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertTrue(intersection.intersects());
		assertEquals(new Point3D(0.5, 0, 0), intersection.getVertex().p);

		// move the plane to the left (along the x-axis)
		plane = new Plane(new Point3D(-0.5, 0, 0), new Point3D(1, 0, 0));
		a = new Vertex(new Point3D(-1, 0, 0));
		b = new Vertex(new Point3D(1, 0, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertTrue(intersection.intersects());
		assertEquals(new Point3D(-0.5, 0, 0), intersection.getVertex().p);

	}

	@Test
	public void testParallelX() throws Exception {
		plane = new Plane(new Point3D(0, 0, 0), new Point3D(1, 0, 0));
		a = new Vertex(new Point3D(0, -1, 0));
		b = new Vertex(new Point3D(0, 1, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertTrue(intersection.isInside());

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(1, 0, 0));
		a = new Vertex(new Point3D(0, 0, -1));
		b = new Vertex(new Point3D(0, 0, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertTrue(intersection.isInside());

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(1, 0, 0));
		a = new Vertex(new Point3D(1, -1, 0));
		b = new Vertex(new Point3D(1, 1, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertFalse(intersection.isInside());

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(1, 0, 0));
		a = new Vertex(new Point3D(1, 0, -1));
		b = new Vertex(new Point3D(1, 0, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertFalse(intersection.isInside());

	}

	@Test
	public void testParallelY() throws Exception {
		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 1, 0));
		a = new Vertex(new Point3D(-1, 0, 0));
		b = new Vertex(new Point3D(1, 0, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertTrue(intersection.isInside());

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 1, 0));
		a = new Vertex(new Point3D(0, 0, -1));
		b = new Vertex(new Point3D(0, 0, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertTrue(intersection.isInside());
		
		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 1, 0));
		a = new Vertex(new Point3D(-1, 1, 0));
		b = new Vertex(new Point3D(1, 1, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertFalse(intersection.isInside());

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 1, 0));
		a = new Vertex(new Point3D(0, 1, -1));
		b = new Vertex(new Point3D(0, 1, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertFalse(intersection.isInside());

	}

	@Test
	public void testParallelZ() throws Exception {
		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 0, 1));
		a = new Vertex(new Point3D(-1, 0, 0));
		b = new Vertex(new Point3D(1, 0, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertTrue(intersection.isInside());

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 0, 1));
		a = new Vertex(new Point3D(0, -1, 0));
		b = new Vertex(new Point3D(0, 1, 0));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertTrue(intersection.isInside());

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 0, 1));
		a = new Vertex(new Point3D(-1, 0, 1));
		b = new Vertex(new Point3D(1, 0, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertFalse(intersection.isInside());

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 0, 1));
		a = new Vertex(new Point3D(0, -1, 1));
		b = new Vertex(new Point3D(0, 1, 1));
		intersection = Vertex.intersection(a, b, plane);
		assertFalse(intersection.intersects());
		assertTrue(intersection.isParallel());
		assertFalse(intersection.isInside());
	}
}
