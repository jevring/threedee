/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.primitives;

import org.junit.Test;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Tests the methods in the {@link net.jevring.threedee.primitives.Plane} class.
 *
 * @author markus@jevring.net
 */
public class PlaneTest {
	@Test
	public void testIsInFrontOf() throws Exception {
		Plane plane;
		plane = new Plane(new Point3D(0, 0, 0), new Point3D(1, 0, 0));
		assertTrue(plane.isInFrontOf(new Point3D(1, 0, 0)));
		assertFalse(plane.isInFrontOf(new Point3D(-1, 0, 0)));

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 1, 0));
		assertTrue(plane.isInFrontOf(new Point3D(0, 1, 0)));
		assertFalse(plane.isInFrontOf(new Point3D(0, -1, 0)));

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 0, 1));
		assertTrue(plane.isInFrontOf(new Point3D(0, 0, 1)));
		assertFalse(plane.isInFrontOf(new Point3D(0, 0, -1)));

		plane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 1, 1));
		assertTrue(plane.isInFrontOf(new Point3D(0, 1, 1)));
		assertFalse(plane.isInFrontOf(new Point3D(0, 1, -1)));  
	}
}
