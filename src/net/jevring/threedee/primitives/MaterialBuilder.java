/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.primitives;

import java.awt.*;

public class MaterialBuilder {

	private String name;
	private Color ambientColor;
	private Color diffuseColor;
	private Color specularColor;
	private float specularCoefficient;
	private float transparency;
	private float transmissionFilter;
	private IlluminationModel illuminationModel;
	private Texture mapAmbientColor;
	private Texture mapDiffuseColor;
	private Texture mapSpecularColor;
	private float mapSpecularCoefficient;
	private float mapTransparency;
	private boolean clamp;

	public MaterialBuilder setName(String name) {
		this.name = name;
		return this;
	}

	public MaterialBuilder setAmbientColor(Color ambientColor) {
		this.ambientColor = ambientColor;
		return this;
	}

	public MaterialBuilder setDiffuseColor(Color diffuseColor) {
		this.diffuseColor = diffuseColor;
		return this;
	}

	public MaterialBuilder setSpecularColor(Color specularColor) {
		this.specularColor = specularColor;
		return this;
	}

	public MaterialBuilder setSpecularCoefficient(float specularCoefficient) {
		this.specularCoefficient = specularCoefficient;
		return this;
	}

	public MaterialBuilder setTransparency(float transparency) {
		this.transparency = transparency;
		return this;
	}

	public MaterialBuilder setTransmissionFilter(float transmissionFilter) {
		this.transmissionFilter = transmissionFilter;
		return this;
	}

	public MaterialBuilder setIlluminationModel(IlluminationModel illuminationModel) {
		this.illuminationModel = illuminationModel;
		return this;
	}

	public MaterialBuilder setMapAmbientColor(Texture mapAmbientColor) {
		this.mapAmbientColor = mapAmbientColor;
		return this;
	}

	public MaterialBuilder setMapDiffuseColor(Texture mapDiffuseColor) {
		this.mapDiffuseColor = mapDiffuseColor;
		return this;
	}

	public MaterialBuilder setMapSpecularColor(Texture mapSpecularColor) {
		this.mapSpecularColor = mapSpecularColor;
		return this;
	}

	public MaterialBuilder setMapSpecularCoefficient(float mapSpecularCoefficient) {
		this.mapSpecularCoefficient = mapSpecularCoefficient;
		return this;
	}

	public MaterialBuilder setMapTransparency(float mapTransparency) {
		this.mapTransparency = mapTransparency;
		return this;
	}

	public Material createMaterial() {
		return new Material(name,
		                    ambientColor,
		                    diffuseColor,
		                    specularColor,
		                    specularCoefficient,
		                    transparency,
		                    transmissionFilter,
		                    illuminationModel,
		                    mapAmbientColor,
		                    mapDiffuseColor,
		                    mapSpecularColor,
		                    mapSpecularCoefficient,
		                    mapTransparency, clamp);
	}

	public String getName() {
		return name;
	}

	public MaterialBuilder setClamp(boolean clamp) {
		this.clamp = clamp;
		return this;
	}

	public boolean getClamp() {
		return clamp;
	}
}