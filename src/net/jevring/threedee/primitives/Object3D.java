/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.primitives;

import net.jevring.threedee.Size;
import net.jevring.threedee.transformations.Mover;
import net.jevring.threedee.transformations.Rotator;

import java.util.*;

/**
 * @author markus@jevring.net
 */
public class Object3D implements WorldObject {
	private final List<Face> faces;
	private final String id;
	private final int numberOfVertices;
	private Point3D position = new Point3D(0, 0, 0);
	private Rotator rotator = new Rotator(0, 0, 0);
	private Mover mover = new Mover(0, 0, 0, true);
	private Material material;
	private double scale = 1.0d;
	private boolean smoothing;
	private Size size;

	public Object3D(List<Face> faces, String id) {
		this(faces, id, true);
	}

	public Object3D(List<Face> faces, String id, boolean calculateVertexNormals) {
		this.faces = faces;
		this.id = id;
		int n = 0;
		for (Face face : faces) {
			n += face.getOriginalVertices().size();
		}
		numberOfVertices = n;
		if (calculateVertexNormals) {
			calculateVertexNormals();
		}
	}

	public Object3D cloneWithCulledFaces(List<Face> faces) {
		Object3D o = new Object3D(faces, id, false);
		o.position = position;
		o.rotator = rotator;
		o.mover = mover;
		o.material = material;
		o.scale = scale;
		o.smoothing = smoothing;
		o.size = o.calculateSize();
		return o;
	}

	public int getNumberOfVertices() {
		return numberOfVertices;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setSmoothing(boolean smoothing) {
		this.smoothing = smoothing;
	}

	@Override
	public Rotator getRotator() {
		return rotator;
	}

	@Override
	public Mover getMover() {
		return mover;
	}

	@Override
	public double getScale() {
		return scale;
	}

	@Override
	public Point3D getPosition() {
		return position;
	}

	public synchronized List<Face> getFaces() {
		return Collections.unmodifiableList(faces);
	}

	public int getNumberOfFaces() {
		return faces.size();
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public void scale(double factor) {
		this.scale = factor;
		for (Face face : faces) {
			face.scale(factor);
		}
		this.size = calculateSize();
	}

	@Override
	public void rotate(Rotator r) {
		this.rotator = r;
		for (Face face : faces) {
			face.rotate(r);
		}
		this.size = calculateSize();
	}

	@Override
	public void move(Mover m) {
		this.mover = m;
		// this may seem wrong, but it is actually correct.
		// first of all, it works.
		// second of all, when this mover is applied to the vertices,
		// it moves the ORIGINAL vertices, not the already moved
		// vertices.
		// thus, this is like saying m.move(new Point3D(0, 0, 0))
		// this way, we're creating fewer objects
		this.position = new Point3D(m.x, m.y, m.z);
		for (Face face : faces) {
			face.move(mover);
		}
		this.size = calculateSize();
	}

	@Override
	public Size getSize() {
		if (size == null) {
			// this shouldn't happen often
			this.size = calculateSize();
		}
		return size;
	}

	private Size calculateSize() {
		double xMax, xMin, yMax, yMin, zMax, zMin;
		xMin = yMin = zMin = Double.MAX_VALUE;
		xMax = yMax = zMax = -Double.MAX_VALUE;

		for (Face face : faces) {
			for (Vertex vertex : face.getVertices()) {
				xMax = Math.max(xMax, vertex.p.x);
				xMin = Math.min(xMin, vertex.p.x);
				yMax = Math.max(yMax, vertex.p.y);
				yMin = Math.min(yMin, vertex.p.y);
				zMax = Math.max(zMax, vertex.p.z);
				zMin = Math.min(zMin, vertex.p.z);
			}
		}
		return new Size(xMax, xMin, yMax, yMin, zMax, zMin);
	}

	/**
	 * Gets a set of 8 points that bound the object.
	 * This takes the current state of the faces and vertices into account,
	 * i.e. after {@link #rotate(net.jevring.threedee.transformations.Rotator) rotation} and
	 * {@link #move(net.jevring.threedee.transformations.Mover) movement} and
	 * {@link #scale(double) scaling} have taken place.
	 *
	 * @return the bounding box of the object
	 */
	@Override
	public List<Point3D> getBoundingBox() {
		return getSize().toBoundingBox();
	}

	/**
	 * Normalizes the coordinates based on the largest dimension.
	 * NOTE: This assumes that no scaling has taken place.
	 *
	 * @see #getSize()
	 * @see #normalize(net.jevring.threedee.Size)
	 */
	@Override
	public void normalize() {
		Size size = getSize();
		normalize(size);
	}

	/**
	 * Normalizes the coordinates based on the largest dimension.
	 * NOTE: This assumes that no scaling has taken place.
	 *
	 * @see #getSize()
	 * @see #normalize()
	 */
	@Override
	public void normalize(Size size) {
		for (Face face : faces) {
			face.normalize(size);
		}
	}

	/**
	 * Calculates vertex normals <b>if not already present</b>.
	 * The normal of a vertex is the average of the normals of all the faces
	 * the vertex is a part of. As such, this is the only time we can do it, as
	 * this is the only place where we know all the faces.
	 */
	private void calculateVertexNormals() {
		long start = System.currentTimeMillis();
		int nVertices = 0;

		// we use the point as the key here, not the vertex.
		// this means we need two maps, but it lets us avoid having Vertex.hashCode()
		// and Vertex.equals() only take the point into account.
		// the reason it's a "point to list" map is that multiple
		// vertices can lie on the same point. In fact, they are expected to

		int normalsCalculated = 0;
		Map<Point3D, List<Face>> pointsToFaces = new HashMap<>();
		Map<Point3D, List<Vertex>> pointsToVertices = new HashMap<>();
		for (Face face : faces) {
			for (Vertex vertex : face.getOriginalVertices()) {
				nVertices++;
				if (vertex.n != null) {
					// we already have a vertex normal, nothing to calculate here
					continue;
				}
				List<Face> facesForPoint = pointsToFaces.get(vertex.p);
				if (facesForPoint == null) {
					facesForPoint = new ArrayList<>();
					pointsToFaces.put(vertex.p, facesForPoint);
				}
				facesForPoint.add(face);

				List<Vertex> verticesForPoint = pointsToVertices.get(vertex.p);
				if (verticesForPoint == null) {
					verticesForPoint = new ArrayList<>();
					pointsToVertices.put(vertex.p, verticesForPoint);
				}
				verticesForPoint.add(vertex);
			}
		}
		for (Map.Entry<Point3D, List<Face>> entry : pointsToFaces.entrySet()) {
			// average these normals, then set the normal of this point in each face
			final List<Face> facesForPoint = entry.getValue();
			final Point3D point3d = entry.getKey();
			double x = 0;
			double y = 0;
			double z = 0;
			double i = 0;

			for (Face face : facesForPoint) {
				final Point3D normal = face.normal();
				x += normal.x;
				y += normal.y;
				z += normal.z;
				i++;
			}
			Point3D vertexNormal = new Point3D(x / i, y / i, z / i);
			for (Vertex vertex : pointsToVertices.get(point3d)) {
				normalsCalculated++;
				vertex.setNormal(vertexNormal);
			}
		}
		long end = System.currentTimeMillis();
		long timeTaken = end - start;
		if (normalsCalculated > 0) {
			System.out.println("Calculated vertex normals for " + normalsCalculated + "/" + nVertices + " vertices in " + timeTaken + " ms");
		}
	}

	public boolean hasTextureCoordinates() {
		return material.getMapDiffuseColor() != null;
	}
}
