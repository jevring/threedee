/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.primitives.camera;

import net.jevring.threedee.primitives.Object3D;

import java.util.ArrayList;
import java.util.List;

/**
 * An object, i.e. a collection of faces, in camera space.
 *
 * @author markus@jevring.net
 */
public class CameraObject3D {
	private final List<CameraFace> faces = new ArrayList<>();
	private final Object3D worldSpaceObject;
	private double zMax = -Double.MAX_VALUE;

	public CameraObject3D(Object3D worldSpaceObject) {
		this.worldSpaceObject = worldSpaceObject;
	}

	public void add(CameraFace face) {
		faces.add(face);
		this.zMax = Math.max(this.zMax, face.calculateZMax());
	}

	public boolean hasFaces() {
		return !faces.isEmpty();
	}

	public List<CameraFace> getFaces() {
		return faces;
	}

	public Object3D getWorldSpaceObject() {
		return worldSpaceObject;
	}

	public double getZMax() {
		return zMax;
	}

	@Override
	public String toString() {
		return worldSpaceObject.toString() + "(" + zMax + ")";
	}
}
