/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.primitives;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import net.jevring.threedee.Size;
import net.jevring.threedee.transformations.Mover;
import net.jevring.threedee.transformations.Rotator;

/**
 * A face in space that consists of a set of (potentially colored) vertices.
 *
 * @author markus@jevring.net
 */
public class Face {
	private final List<Vertex> vertices = new ArrayList<>();
	private List<Vertex> actualVertices;
	private double scale = 1d;
	private Mover mover = new Mover(0, 0, 0, true);
	private Rotator rotator = new Rotator(0, 0, 0);
	private Size ownerObjectSize = new Size(0, 0, 0, 0, 0, 0);
	private final Point3D normal;
	
	public Face(Vertex a, Vertex b, Vertex c) {
		Collections.addAll(vertices, a, b, c);
		this.actualVertices = this.vertices;
		this.normal = calculateNormal();
	}

	public Face(List<Vertex> vertices) {
		if (vertices.size() != 3) {
			throw new IllegalArgumentException("Cannot handle faces with more than 3 vertices");
		}
		this.vertices.addAll(vertices);
		this.actualVertices = this.vertices;
		this.normal = calculateNormal();
	}

	public List<Vertex> getVertices() {
		return Collections.unmodifiableList(actualVertices);
	}

	public void normalize(Size size) {
		ownerObjectSize = size;
		alter();
	}

	public void scale(double factor) {
		this.scale = factor;
		alter();
	}

	public void move(Mover mover) {
		if (mover == null) {
			throw new NullPointerException("Mover must not be null");
		}
		this.mover = mover;
		alter();
	}

	public void rotate(Rotator rotator) {
		if (rotator == null) {
			throw new NullPointerException("Rotator must not be null");
		}
		this.rotator = rotator;
		alter();
	}

	private void alter() {
		List<Vertex> actualVertices = new ArrayList<>();
		for (Vertex vertex : vertices) {
			// perform normalization
			double x = vertex.p.x / ownerObjectSize.maxMax;
			double y = vertex.p.y / ownerObjectSize.maxMax;
			double z = vertex.p.z / ownerObjectSize.maxMax;
			// if we add a multiplier to x, y, and z here, that will be magnified by the scale, which is something that we don't want
			vertex = vertex.moveTo(new Point3D(x, y, z));

			// scale must be done BEFORE move, otherwise the movement is amplified by the scale.
			vertex = vertex.scale(scale);
			// note: rotation MUST be performed BEFORE move.
			// if move is performed first, the rotation is around the origin of the WORLD, not the object
			vertex = rotator.rotate(vertex);
			vertex = mover.move(vertex);

			Point3D normal = vertex.n;
			normal = rotator.rotate(normal);
			vertex.setNormal(normal);
			actualVertices.add(vertex);
		}
        this.actualVertices = actualVertices;
	}

	/**
	 * The normal of a surface is the cross product of its points, roughly, as described in
	 * "Method 3" in https://en.wikipedia.org/wiki/Plane_%28geometry%29.
	 *
	 * <p>NOTE: Assumes this face is a triangle
	 *
	 * @return the normal of the surface
	 */
	public Point3D normal() {
		return normal;
	}

	private Point3D calculateNormal() {
		Point3D p1 = actualVertices.get(0).p;
		Point3D p2 = actualVertices.get(1).p;
		Point3D p3 = actualVertices.get(2).p;
		return p2.minus(p1).crossProduct(p3.minus(p1));
	}

	List<Vertex> getOriginalVertices() {
		return vertices;
	}

	/**
	 * Generates a list of faces by splitting up a polygon. Splitting the polygon is done by selecting a
	 * single vertex, and drawing "diagonals" from it to every other vertex. The resulting faces consist
	 * of the vertex chosen, the vertex the diagonal went to, and the vertex "before" the one the diagonal
	 * went to. The vertex immediately following the selected vertex will be skipped, as these two vertices
	 * are already connected.
	 *
	 * <p>For example, if we get a list of 5 vertices in, selecting vertex 0 as the originator, we would
	 * create the following triangles:
	 * <ul>
	 *     <li>0, 2, 1</li>
	 *     <li>0, 3, 2</li>
	 *     <li>0, 4, 3</li>
	 * </ul>
	 *
	 * @param vertices the list of vertices to convert into faces. Must have 3 or more vertices.
	 *                 Must form a convex polygon.
	 * @return a list of faces representing the convex polygon represented by the input data
	 */
	public static List<Face> splitPolygon(List<Vertex> vertices) {
		if (vertices.size() == 3) {
			return Collections.singletonList(new Face(vertices));
		} else {
			List<Face> faces = new ArrayList<>();
			for (int i = 2; i < vertices.size(); i++) {
				faces.add(new Face(Arrays.asList(vertices.get(0), vertices.get(i-1), vertices.get(i))));
			}
			return faces;
		}
	}
}
