/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.primitives;

import java.awt.image.BufferedImage;

/**
 * @author markus@jevring.net
 */
public class Texture {
	private final int height;
	private final int width;
	/**
	 * If we don't clamp, we wrap. Clamp is default off in MTL.
	 */
	private final boolean clamp;
    private final int[] data;

    public Texture(BufferedImage image, boolean clamp) {
		this.height = image.getHeight();
		this.width = image.getWidth();
		this.clamp = clamp;
        // for some reason, this results in an index out of bounds exception...
        //this.data = image.getRaster().getPixels(0, 0, width, height, new int[width * height]);
        this.data = image.getRGB(0, 0, width, height, new int[width * height], 0, width);
        
	}

	/**
	 * Fetches the color specified at these pixel coordinates.
	 *
	 * @param x x
	 * @param y y
	 * @return the color
	 * @throws java.lang.ArrayIndexOutOfBoundsException if the coordinates are outside the texture
	 */
	public int getColor(int x, int y) {
        return data[y * width + x];
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public boolean isClamp() {
		return clamp;
	}
}
