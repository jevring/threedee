/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.primitives;

/**
 * @author markus@jevring.net
 */
public class Point3D {
	public final double x;
	public final double y;
	public final double z;

	public Point3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		//System.out.println(this);
	}

	/**
	 * "Scaling" a point moves it in all coordinates according to a factor.
	 *
	 * @param factor the factor to factor by
	 */
	public Point3D scale(double factor) {
		if (factor == 0) {
			return this;
		} else {
			return new Point3D(x * factor, y * factor, z * factor);
		}
	}

	public Point3D move(double x, double y, double z) {
		return new Point3D(this.x + x, this.y + y, this.z + z);
	}

	public Point3D moveX(double d) {
		return new Point3D(x+d, y, z);
	}

	public Point3D moveY(double d) {
		return new Point3D(x, y+d, z);
	}

	public Point3D moveXY(double dx, double dy) {
		return new Point3D(x + dx, y + dy, z);
	}

	public Point3D moveZ(double d) {
		return new Point3D(x, y, z+d);
	}

	public double distance(Point3D to) {
		double dx = x - to.x;
		double dxs = Math.pow(dx, 2);
		double dy = y - to.y;
		double dys = Math.pow(dy, 2);
		double dz = z - to.z;
		double dzs = Math.pow(dz, 2);
		double sum = dxs + dys + dzs;
		return Math.sqrt(sum);
	}

	public Point3D interpolate(Point3D p1, double howFar) {
		double nx = (x * (1 - howFar)) + (p1.x * howFar);
		double ny = (y * (1 - howFar)) + (p1.y * howFar);
		double nz = (z * (1 - howFar)) + (p1.z * howFar);
		return new Point3D(nx, ny, nz);
	}

	public Point3D minus(Point3D p) {
		return new Point3D(x - p.x, y - p.y, z - p.z);
	}

	public Point3D plus(Point3D p) {
		return new Point3D(x + p.x, y + p.y, z + p.z);
	}

	public Point3D divide(double d) {
		return new Point3D(x / d, y / d, z / d);
	}

	public double magnitude() {
		double xs = x * x;
		double ys = y * y;
		double zs = z * z;
		return Math.sqrt(xs + ys + zs);
	}

	public Point3D toUnit() {
		// divides each coordinate with the magnitude of the vector
		double m = magnitude();
		if (m != 0) {
			return new Point3D(x / m, y / m, z / m);
		} else {
			// if the magnitude is 0, this "vector" is 0 units long
			return new Point3D(0, 0, 0);
		}
	}

	public Point3D crossProduct(Point3D p) {
		// http://www.analyzemath.com/vector_calculators/vector_cross_product.html
		double tx = (y * p.z) - (z * p.y);
		double ty = (z * p.x) - (x * p.z);
		double tz = (x * p.y) - (y * p.x);
		return new Point3D(tx, ty, tz);
	}

	public double dotProduct(Point3D p) {
		// https://en.wikipedia.org/wiki/Dot_product
		return (x * p.x) + (y * p.y) + (z * p.z);
	}

	@Override
	public String toString() {
		return "Point3D{" +
				"x=" + x +
				", y=" + y +
				", z=" + z +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Point3D point3D = (Point3D) o;

		if (Double.compare(point3D.x, x) != 0) {
			return false;
		}
		if (Double.compare(point3D.y, y) != 0) {
			return false;
		}
		if (Double.compare(point3D.z, z) != 0) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		temp = x != +0.0d ? Double.doubleToLongBits(x) : 0L;
		result = (int) (temp ^ (temp >>> 32));
		temp = y != +0.0d ? Double.doubleToLongBits(y) : 0L;
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = z != +0.0d ? Double.doubleToLongBits(z) : 0L;
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}
