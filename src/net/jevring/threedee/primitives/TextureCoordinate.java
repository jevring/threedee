/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.primitives;

/**
 * A texture coordinate is a value between 0 and 1, inclusive, that indicates the location in a
 * texture of the value to look up.
 *
 * @author markus@jevring.net
 */
public class TextureCoordinate {
	public final double u;
	public final double v;
	public final double w;

	public TextureCoordinate(double u) {
		this(u, 0, 0);
	}

	/**
	 * @param u from 0 (left) to 1 (right)
	 * @param v from 0 (bottom) to 1 (top)
	 */
	public TextureCoordinate(double u, double v) {
		this(u, v, 0);
	}

	public TextureCoordinate(double u, double v, double w) {
		this.u = u;
		this.v = v;
		this.w = w;
	}

	public TextureCoordinate interpolate(TextureCoordinate p1, double howFar) {
		double nu = (u * (1 - howFar)) + (p1.u * howFar);
		double nv = (v * (1 - howFar)) + (p1.v * howFar);
		double nw = (w * (1 - howFar)) + (p1.w * howFar);
		return new TextureCoordinate(nu, nv, nw);
	}

	@Override
	public String toString() {
		return "TextureCoordinate{" +
				"u=" + u +
				", v=" + v +
				", w=" + w +
				'}';
	}
}
