/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.primitives;

import net.jevring.threedee.renderer.Colors;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;

/**
 * @author markus@jevring.net
 */
public class Vertex {
	public final Point3D p;
	public final Color c;
	public final TextureCoordinate t;
	/**
	 * This is the vertex normal. It is not final, as we need to be able to set it
	 * after we create the vertex, to avoid recreating the entire object/face structure.
	 * This, of course, makes it vulnerable to fuckups, as it is directly assignable.
	 * We could make a getter, but that would just clutter the code. Especially since
	 * people can simply use the setter anyway.
	 *
	 * It would be nice to have a way of saying "this is 'public final', but 'private non-final'".
	 * 
	 * We could use a copy method, but then we have to reconstruct entire structures of data
	 * in some places where it's not possible, like redoing faces inside and object.
	 *
	 * @see #setNormal(Point3D)
	 */
	public Point3D n;

	public Vertex(@NotNull Point3D p) {
		this(p, null, null, null);
	}

	public Vertex(@NotNull Point3D p, @Nullable Color c) {
		this(p, c, null, null);
	}

	public Vertex(@NotNull Point3D p, @Nullable Point3D n, @Nullable TextureCoordinate t) {
		this(p, Color.BLACK, n, t);
	}

	public Vertex(@NotNull Point3D p, @Nullable Color c, @Nullable Point3D n, @Nullable TextureCoordinate t) {
		this.p = p;
		this.c = (c != null ? c : Color.BLACK);
		this.n = n;
		this.t = t;
	}

	public Vertex scale(double scale) {
		return moveTo(p.scale(scale));
	}

	public Vertex moveTo(Point3D p) {
		return new Vertex(p, c, n, t);
	}

	@Override
	public String toString() {
		return "Vertex{" +
				"p=" + p +
				", c=" + c +
				", t=" + t +
				", n=" + n +
				'}';
	}

	public void setNormal(Point3D vertexNormal) {
		this.n = vertexNormal;
	}

	/**
	 * Finds the intersection between the line from {@code a} to {@code b} and the 
	 * {@code plane}.
	 */
	public static Intersection intersection(Vertex a, Vertex b, Plane plane) {
		
		
		// in the terminology of:
		// http://geomalgorithms.com/a05-_intersect-1.html
		
		Point3D v0 = plane.point;
		Point3D n = plane.normal;
		Point3D p0 = a.p;
		Point3D p1 = b.p;
		Point3D u = p1.minus(p0);
		Point3D w = p0.minus(v0);
		boolean parallel = u.dotProduct(n) == 0;
		boolean insidePlane = n.dotProduct(w) == 0; 
		
		if (!parallel) {
			// sI: how far along the line from a to b is the intersection:
			double sI = -n.dotProduct(w) / n.dotProduct(u);
			if (sI >= 0 && sI <= 1) { // is the "or equals" correct here? We get rid of many error conditions this way
				// there's an intersection
				
				Point3D intersection = p0.plus(u.scale(sI)); // p0 + (sI * u)
				TextureCoordinate textureCoordinate = null;
				if (a.t != null) {
					textureCoordinate = a.t.interpolate(b.t, sI);
				}
				Color color = null;
				if (a.c != null) {
					color = Colors.interpolate(a.c, (float) (1 - sI), b.c, (float) sI);
				}
				Point3D vertexNormal = null;
				if (a.n != null) {
					vertexNormal = a.n.interpolate(b.n, sI);
				}

				return new Intersection(new Vertex(intersection, color, vertexNormal, textureCoordinate));
				
			} else {
				return new Intersection(false, insidePlane);
			}
		} else {
			return new Intersection(true, insidePlane);	
		}
	}
	
	public static final class Intersection {
		private final boolean parallel;
		private final boolean inside;
		private final Vertex vertex;

		private Intersection(Vertex vertex) {
			this.parallel = false;
			this.inside = false;
			this.vertex = vertex;
		}
		
		private Intersection(boolean parallel, boolean inside) {
			this.parallel = parallel;
			this.inside = inside;
			this.vertex = null;
		}
		
		public boolean intersects() {
			return vertex != null;
		}

		public boolean isParallel() {
			return parallel;
		}

		public boolean isInside() {
			return inside;
		}

		public Vertex getVertex() {
			return vertex;
		}

		@Override
		public String toString() {
			return "Intersection{" +
					"parallel=" + parallel +
					", inside=" + inside +
					", vertex=" + vertex +
					'}';
		}
	}
}
