/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.primitives;

import java.awt.*;

/**
 * @author markus@jevring.net
 */
public class Material {
	private final String name;
	/**
	 * Ka
	 */
	private final Color ambientColor;
	/**
	 * Kd
	 */
	private final Color diffuseColor;
	/**
	 * Ks
	 */
	private final Color specularColor;
	/**
	 * Ns
	 */
	private final float specularCoefficient;
	/**
	 * Tr or d
	 */
	private final float transparency;

	/**
	 * Tf
	 */
	private final float transmissionFilter;

	/**
	 * illum
	 */
	private final IlluminationModel illuminationModel;

	// map data
	/**
	 * map_Ka
	 */
	private final Texture mapAmbientColor;
	/**
	 * map_Kd
	 */
	private final Texture mapDiffuseColor;
	/**
	 * map_Ks
	 */
	private final Texture mapSpecularColor;
	/**
	 * map_Ns
	 */
	private final float mapSpecularCoefficient;
	/**
	 * map_Tr or map_d
	 */
	private final float mapTransparency;

	/**
	 * If we don't clamp, we wrap. Clamp is default off in MTL.
	 * http://local.wasp.uwa.edu.au/~pbourke/dataformats/mtl/
	 */
	private final boolean clamp;

	public Material(String name,
	                Color ambientColor,
	                Color diffuseColor,
	                Color specularColor,
	                float specularCoefficient,
	                float transparency,
	                float transmissionFilter,
	                IlluminationModel illuminationModel,
	                Texture mapAmbientColor,
	                Texture mapDiffuseColor,
	                Texture mapSpecularColor,
	                float mapSpecularCoefficient,
	                float mapTransparency, boolean clamp) {
		this.name = name;
		this.ambientColor = ambientColor;
		this.diffuseColor = diffuseColor;
		this.specularColor = specularColor;
		this.specularCoefficient = specularCoefficient;
		this.transparency = transparency;
		this.transmissionFilter = transmissionFilter;
		this.illuminationModel = illuminationModel;
		this.mapAmbientColor = mapAmbientColor;
		this.mapDiffuseColor = mapDiffuseColor;
		this.mapSpecularColor = mapSpecularColor;
		this.mapSpecularCoefficient = mapSpecularCoefficient;
		this.mapTransparency = mapTransparency;
		this.clamp = clamp;
	}

	public String getName() {
		return name;
	}

	public Color getAmbientColor() {
		return ambientColor;
	}

	public Color getDiffuseColor() {
		return diffuseColor;
	}

	public Color getSpecularColor() {
		return specularColor;
	}

	public float getSpecularCoefficient() {
		return specularCoefficient;
	}

	public float getTransparency() {
		return transparency;
	}

	public float getTransmissionFilter() {
		return transmissionFilter;
	}

	public IlluminationModel getIlluminationModel() {
		return illuminationModel;
	}

	public Texture getMapAmbientColor() {
		return mapAmbientColor;
	}

	public Texture getMapDiffuseColor() {
		return mapDiffuseColor;
	}

	public Texture getMapSpecularColor() {
		return mapSpecularColor;
	}

	public float getMapSpecularCoefficient() {
		return mapSpecularCoefficient;
	}

	public float getMapTransparency() {
		return mapTransparency;
	}
}

