/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.primitives;

import net.jevring.threedee.Size;
import net.jevring.threedee.transformations.Mover;
import net.jevring.threedee.transformations.Rotator;

import java.util.Collections;
import java.util.List;

/**
 * A group is a collection of objects, often loaded together.
 * The advantage of groups is that operations can be performed
 * on all members of the group as a unit.
 *
 * @author markus@jevring.net
 */
public class Group implements WorldObject {
    private final List<Object3D> objects;
    private final String id;
    private Point3D position = new Point3D(0, 0, 0);
    private Rotator rotator = new Rotator(0, 0, 0);
    private Mover mover = new Mover(0, 0, 0, true);
    private double scale = 1.0d;


    public Group(List<Object3D> objects, String id) {
        this.objects = objects;
        this.id = id;
    }

    public Group(Object3D object) {
        this.objects = Collections.singletonList(object);
        this.id = object.toString();
    }

    public List<Object3D> getObjects() {
        return objects;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public void scale(double factor) {
        this.scale = factor;
        for (Object3D object : objects) {
            object.scale(factor);
        }
    }

    @Override
    public double getScale() {
        return scale;
    }

    @Override
    public void rotate(Rotator r) {
        this.rotator = r;
        for (Object3D object : objects) {
            object.rotate(r);
        }
    }

    @Override
    public Rotator getRotator() {
        return rotator;
    }

    @Override
    public void move(Mover m) {
        // todo: items that have been moved or rotated away immediately return to their original position when I do this
        this.mover = m;
        this.position = new Point3D(m.x, m.y, m.z);
        for (Object3D object : objects) {
            object.move(m);
        }
    }

    @Override
    public Mover getMover() {
        return mover;
    }

    @Override
    public Size getSize() {
        double xMax, xMin, yMax, yMin, zMax, zMin;
        xMin = yMin = zMin = Double.MAX_VALUE;
        xMax = yMax = zMax = -Double.MAX_VALUE;

        for (Object3D object : objects) {
            Size size = object.getSize();
            xMax = Math.max(xMax, size.xMax);
            xMin = Math.min(xMin, size.xMin);
            yMax = Math.max(yMax, size.yMax);
            yMin = Math.min(yMin, size.yMin);
            zMax = Math.max(zMax, size.zMax);
            zMin = Math.min(zMin, size.zMin);
        }
        return new Size(xMax, xMin, yMax, yMin, zMax, zMin);
    }

    @Override
    public Point3D getPosition() {
        return position;
    }

    @Override
    public List<Point3D> getBoundingBox() {
        return getSize().toBoundingBox();
    }

    @Override
    public void normalize() {
        for (Object3D object : objects) {
            object.normalize();
        }
    }

    @Override
    public void normalize(Size size) {
        for (Object3D object : objects) {
            object.normalize(size);
        }
    }
}
