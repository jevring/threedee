/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.primitives.*;
import net.jevring.threedee.transformations.Mover;
import net.jevring.threedee.transformations.Rotator;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * The world contains all the objects to be rendered and the light sources.
 * NOTE: <b>ALL</b> methods that modify the view must be synchronized. This is because the {@link Engine}
 * will synchronize on the view and the world to avoid getting modifications during rendering,
 * which could, at least, lead to tearing, but at worst, a {@link java.util.ConcurrentModificationException}.
 *
 * @author markus@jevring.net
 */
public class World {
	private final List<Object3D> objects = new ArrayList<>();
	private final List<LightSource> lightSources = new ArrayList<>();
	private final LightSource ambientLight = new LightSource(new Point3D(0, 0, 0), Color.WHITE, 1f);
	private final List<Group> groups = new ArrayList<>();
	private int totalNumberOfFaces = 0;

	public World() {
		lightSources.add(new LightSource(new Point3D(100, 100, 1000), Color.WHITE, 0.5f));
		lightSources.add(new LightSource(new Point3D(100, 100, -1000), Color.BLUE, 0.3f));
		//lightSources.add(new LightSource(new Point3D(100, -100, -500), Color.ORANGE, 0.3f));
	}

	/**
	 * Adds multiple objects to the world and normalizes them using the same values.
	 * This is to avoid multiple objects that are part of the same scene from having
	 * incompatible sizes.
	 *
	 * @param group the objects to add to the world
	 */
	public synchronized void add(Group group, double scale) {
		groups.add(group);
		List<Size> sizes = new ArrayList<>();
		for (Object3D object : group.getObjects()) {
			sizes.add(object.getSize());
		}
		Collections.sort(sizes, new Comparator<Size>() {
			@Override
			public int compare(Size o1, Size o2) {
				if (o1.maxMax > o2.maxMax) {
					return -1;
				} else if (o2.maxMax > o1.maxMax) {
					return 1;
				} else {
					return 0;
				}
			}
		});
		Size biggestSize = sizes.get(0);
		for (Object3D object : group.getObjects()) {
			object.normalize(biggestSize);
			this.objects.add(object);
			object.scale(scale);
			totalNumberOfFaces += object.getNumberOfFaces();
		}
	}

	public synchronized void rotate(WorldObject object, Rotator r) {
		object.rotate(r);
	}

	public synchronized void move(WorldObject object, Mover m) {
		object.move(m);
	}

	public List<LightSource> getLightSources() {
		return lightSources;
	}

	public LightSource getAmbientLight() {
		return ambientLight;
	}

	public List<Object3D> getObjects() {
		return objects;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public int getTotalNumberOfFaces() {
		return totalNumberOfFaces;
	}
}
