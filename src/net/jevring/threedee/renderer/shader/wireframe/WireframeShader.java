/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.wireframe;

import net.jevring.threedee.View;
import net.jevring.threedee.World;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.camera.CameraFace;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.painter.ZBufferedPainter;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.AbstractShader;
import net.jevring.threedee.renderer.shader.Delta;
import net.jevring.threedee.renderer.shader.MutableVertex;

import java.awt.*;
import java.util.List;

/**
 * A wireframe "shader". While there is technically no "shading" when using a wireframe model,
 * it fits in with the other shader.
 *
 * @author markus@jevring.net
 */
public class WireframeShader extends AbstractShader<Delta, MutableVertex<Delta>> {
	private final Graphics graphics;

	// todo: this has stopped working now that the sky box works.
	// this is likely because the skybox is rendered last, which overwrites everything previously rendered that doesn't adhere to the z-buffer

	public WireframeShader(World world, View view, ZBufferedPainter painter, boolean singleThreaded, Graphics graphics) {
		super(world, view, painter, singleThreaded);
		this.graphics = graphics;
		graphics.setColor(Color.BLACK);
	}

	public WireframeShader(World world,
	                       View view,
	                       Graphics graphics,
	                       ZBufferedPainter painter,
	                       Color color,
	                       boolean singleThreaded) {
		super(world, view, painter, singleThreaded);
		this.graphics = graphics;
		graphics.setColor(color);
	}

	@Override
	public void draw(CameraFace face, ReflectionModel reflectionModel, Material material, CameraObject3D object) {
		// todo: this doesn't work when we're drawing the skybox using the z-buffer painter
		List<MutableVertex<Delta>> mvs = prepare(face, reflectionModel, material);
		Polygon p = new Polygon();
		for (MutableVertex<Delta> mv : mvs) {
			p.addPoint(r(mv.x), r(mv.y));
		}

		graphics.setColor(Color.BLACK);
		graphics.drawPolygon(p);
	}

	@Override
	protected Delta createDelta(MutableVertex<Delta> v1, MutableVertex<Delta> v2) {
		throw new UnsupportedOperationException("Should never happen, as we override draw()");
	}

	@Override
	protected MutableVertex<Delta> copy(MutableVertex<Delta> deltaMutableVertex) {
		return deltaMutableVertex.copy();
	}

	@Override
	protected void drawLine(MutableVertex<Delta> s, MutableVertex<Delta> e, CameraObject3D object) {
		throw new UnsupportedOperationException("Should never happen, as we override draw()");
	}

	@Override
	protected MutableVertex<Delta> createVertex(Material material, CameraVertex vertex, ReflectionModel reflectionModel) {
		return new MutableVertex<>(vertex.getScreenX(), vertex.getScreenY(), vertex.getCameraZ());
	}
}
