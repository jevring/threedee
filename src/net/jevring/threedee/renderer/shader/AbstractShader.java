/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader;

import net.jevring.threedee.View;
import net.jevring.threedee.World;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.camera.CameraFace;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.painter.ZBufferedPainter;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Abstract base-class for shaders that provides some vertex operations.
 *
 * @author markus@jevring.net
 */
public abstract class AbstractShader<D extends Delta, V extends MutableVertex<D>> implements Shader {
	private static final ExecutorService executor =
			Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), new ThreadFactory() {
				private final AtomicInteger i = new AtomicInteger();

				@Override
				public Thread newThread(@NotNull Runnable r) {
					Thread t = new Thread(r, "Shader-" + i.getAndIncrement());
					t.setDaemon(true);
					return t;
				}
			});
	protected final World world;
	protected final View view;
	protected final int width;
	protected final int height;
	protected final boolean singleThreaded;
	protected final ZBufferedPainter painter;

	protected AbstractShader(World world, View view, ZBufferedPainter painter, boolean singleThreaded) {
		this.world = world;
		this.view = view;
		this.painter = painter;
		this.singleThreaded = singleThreaded;
		this.width = view.getWidth();
		this.height = view.getHeight();
	}

	protected abstract V copy(V v);

	protected void drawTriangle(V a, V b, V c, D d1, D d2, D d3, CameraObject3D object) {
		// s and e are points between which horizontal lines are drawn
		V s = copy(a);
		V e = copy(a);

		// todo: document how this actually works

		// There's another good thing about this, and that's that we can choose to keep the color on the vertex if present.
		// thus blending the "material" and the existing vertex coloring
		// actually, the vertex coloring could be considered a material in itself. the "ambient" part of each vertex, basically.
		// still need to blend it, though

		int linesToRender = (int) (c.y - a.y);
		CountDownLatch allLinesRendered = new CountDownLatch(linesToRender);

		if (d1.x > d2.x) {
			// draw top part
			while (s.y < b.y) {
				drawLineThreaded(s, e, allLinesRendered, object);
				s.shift(d2);
				e.shift(d1);
				s.y++;
				e.y++;
			}

			// draw bottom half
			e = copy(b);
			while (s.y < c.y) {
				drawLineThreaded(s, e, allLinesRendered, object);
				s.shift(d2);
				e.shift(d3);
				s.y++;
				e.y++;
			}
		} else {
			// draw top part
			while (s.y < b.y) {
				drawLineThreaded(s, e, allLinesRendered, object);
				s.shift(d1);
				e.shift(d2);
				s.y++;
				e.y++;
			}

			// draw bottom half
			s = copy(b);
			while (s.y < c.y) {
				drawLineThreaded(s, e, allLinesRendered, object);
				s.shift(d3);
				e.shift(d2);
				s.y++;
				e.y++;
			}
		}
		if (!singleThreaded) {
			// we need to wait for the threads to finish before we can consider this triangle rendered
			try {
				allLinesRendered.await();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void drawLineThreaded(V s, V e, final CountDownLatch allLinesRendered, CameraObject3D object) {
		if (singleThreaded) {
			drawLine(s, e, object);
		} else {
			// if we dispatch the individual lines to separate threads, we know that ZBufferedPainter.draw() will never
			// run in to problems. as we will be tackling a unique part of the screen in each thread


			final V sc = copy(s);
			final V ec = copy(e);
			executor.execute(new Runnable() {
				@Override
				public void run() {
					drawLine(sc, ec, object);
					allLinesRendered.countDown();
				}
			});
		}
	}

	protected abstract void drawLine(V s, V e, CameraObject3D object);

	protected abstract V createVertex(Material material, CameraVertex vertex, ReflectionModel reflectionModel);

	protected abstract D createDelta(V v1, V v2);

	@Override
	public void draw(CameraFace face, ReflectionModel reflectionModel, Material material, CameraObject3D object) {

		List<V> mvs = prepare(face, reflectionModel, material);

		V a = mvs.get(0);
		V b = mvs.get(1);
		V c = mvs.get(2);

		// values for stepping in the interpolation
		// NOTE: if these REFERENCES are of type Delta, we hit the wrong method and we don't interpolate properly!
		// it doesn't seem to matter what TYPE something is as long as the REFERENCE is the same
		D d1 = createDelta(b, a);
		D d2 = createDelta(c, a);
		D d3 = createDelta(c, b);

		drawTriangle(a, b, c, d1, d2, d3, object);
	}

	protected List<V> prepare(CameraFace face, ReflectionModel reflectionModel, Material material) {
		List<V> vertices = new ArrayList<>();
		for (CameraVertex vertex : face.getVertices()) {
			vertices.add(createVertex(material, vertex, reflectionModel));
		}
		return vertices;
	}

	protected int r(double d) {
		return (int) Math.round(d);
	}
}
