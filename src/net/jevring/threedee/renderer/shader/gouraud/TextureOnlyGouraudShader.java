/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.gouraud;

import net.jevring.threedee.View;
import net.jevring.threedee.World;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.Texture;
import net.jevring.threedee.primitives.Vertex;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.painter.ZBufferedPainter;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.AbstractShader;
import net.jevring.threedee.renderer.texturefilter.TextureFilter;

/**
 * This shader uses only texture data to draw a face. Unlike the {@link TexturedColoredGouraudShader}, which uses both texture and lighting,
 * this shader is significantly faster, as it neither performs any blending operations, and interpolates far fewer values.
 *
 * @author markus@jevring.net
 * @see GouraudShader
 * @see TexturedColoredGouraudShader
 */
public class TextureOnlyGouraudShader extends AbstractShader<TextureOnlyDelta, TextureOnlyMutableVertex> {
    private final TextureFilter textureFilter;

    public TextureOnlyGouraudShader(World world,
                                    View view,
                                    ZBufferedPainter painter,
                                    boolean singleThreaded, TextureFilter textureFilter) {
        super(world, view, painter, singleThreaded);
        this.textureFilter = textureFilter;
    }

    @Override
    protected TextureOnlyMutableVertex createVertex(Material material, CameraVertex vertex, ReflectionModel reflectionModel) {
	    Vertex worldVertex = vertex.getWorldVertex();
	    return new TextureOnlyMutableVertex(vertex.getScreenX(),
	                                        vertex.getScreenY(),
	                                        vertex.getCameraZ(),
	                                        worldVertex.t.u,
	                                        worldVertex.t.v,
	                                        material);
    }

    @Override
    protected TextureOnlyDelta createDelta(TextureOnlyMutableVertex v1, TextureOnlyMutableVertex v2) {
        return new TextureOnlyDelta(v1, v2);
    }

    @Override
    protected TextureOnlyMutableVertex copy(TextureOnlyMutableVertex textureOnlyMutableVertex) {
        return textureOnlyMutableVertex.copy();
    }

    @Override
    protected void drawLine(TextureOnlyMutableVertex ts, TextureOnlyMutableVertex te, CameraObject3D object) {
        final Texture texture = ts.material.getMapDiffuseColor();

        if (ts.x > te.x) {
            // ahh, this did the trick! because the y-ordering when two points are aligned is undefined,
            // we'd get lines going in the "wrong" direction, so we need to flip start and end.
            // this fixes the bug where, at a certain rotation, the "bottom" half of the triangle,
            // being the *only* half at that particular rotation, would "disappear".
            TextureOnlyMutableVertex t = ts;
            ts = te;
            te = t;
        }
        TextureOnlyMutableVertex p = ts.copy();
	    ColorProducer<TextureOnlyMutableVertex> colorProducer = new ColorProducer<TextureOnlyMutableVertex>() {
		    @Override
		    public int getColor(TextureOnlyMutableVertex p) {
			    // http://cg.informatik.uni-freiburg.de/course_notes/graphics_06_texturing.pdf (page 21-25)
			    // https://en.wikipedia.org/wiki/Texture_mapping#Perspective_correctness
			    // https://gamedev.stackexchange.com/questions/71473/uv-interpolation-is-wrong-with-a-wide-fov
			    // http://www.lysator.liu.se/~mikaelk/doc/perspectivetexture/
			    // http://www.cse.ohio-state.edu/~whmin/courses/cse5542-2013-spring/15-texture.pdf (page 33)

			    //double z = 1 / p.iz;
			    double u = p.uiz / p.iz; // (u/z) / (1/z) 
			    double v = p.viz / p.iz; // (v/z) / (1/z)
			    return textureFilter.getColor(u, v, texture).getRGB();
		    }
	    };

        final double dz;
        // texture
        final double diz, duiz, dviz;

        // The *Delta classes interpolates for Y
        // Here we interpolate for X.
        // Perhaps we should make that semantic clearer.
        // Perhaps we should make an XInterpolationStep and
        // a YInterpolationStep that embody these things.

        final double esx = te.x - ts.x;
        if (esx > 0d) {
            dz = (te.z - ts.z) / esx;
            // texture
            diz = (te.iz - ts.iz) / esx;
            duiz = (te.uiz - ts.uiz) / esx;
            dviz = (te.viz - ts.viz) / esx;
        } else {
            dz = 0;
            // texture
            diz = duiz = dviz = 0d;
        }
        // draw a line and get the colors from the texture map
        while (p.x < te.x) {
	        painter.draw(p, colorProducer, object);
	        p.z += dz;
	        // texture
            p.iz += diz;
            p.uiz += duiz;
            p.viz += dviz;

            p.x++;
        }
    }
}
