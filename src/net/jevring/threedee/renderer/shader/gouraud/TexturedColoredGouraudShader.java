/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.gouraud;

import net.jevring.threedee.View;
import net.jevring.threedee.World;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.Texture;
import net.jevring.threedee.primitives.Vertex;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.Colors;
import net.jevring.threedee.renderer.painter.ZBufferedPainter;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.AbstractShader;
import net.jevring.threedee.renderer.texturefilter.TextureFilter;

import java.awt.*;

/**
 * This shader uses both texture and lightning data from the material to render a face. The lighting is interpolated
 * in a standard gouraud manner, as is the texture. Once both texture and lighting values have been found, they are
 * blended using the specified {@link BlendingMode}.
 *
 * todo: parachute_01 is mirrored. Both model and texture. The stitches are on the wrong side. same with the bomb wings
 * this is likely due to the fact that it's designed in a "left hand" coordinate system, while we have a "right hand" coordinate system
 * 
 * @author markus@jevring.net
 * @see GouraudShader
 * @see TextureOnlyGouraudShader
 */
public class TexturedColoredGouraudShader extends AbstractShader<TextureColorDelta, TexturedColoredMutableVertex> {
	private final TextureFilter textureFilter;
	private final BlendingMode blendingMode;

	public TexturedColoredGouraudShader(World world,
                                        View view,
                                        ZBufferedPainter painter,
                                        boolean singleThreaded, TextureFilter textureFilter,
                                        BlendingMode blendingMode) {
		super(world, view, painter, singleThreaded);
		this.textureFilter = textureFilter;
		this.blendingMode = blendingMode;
	}

    @Override
    protected TexturedColoredMutableVertex copy(TexturedColoredMutableVertex texturedColoredMutableVertex) {
        return texturedColoredMutableVertex.copy();
    }

    @Override
    protected TexturedColoredMutableVertex createVertex(Material material, CameraVertex vertex, ReflectionModel reflectionModel) {
		// NOTE: without using unit normals, all the colors are black
		// The color needs to be determined using world-space data, not camera space.
		// This is because all the things that affect the color are in world-coordinates, like the lights
	    Vertex worldVertex = vertex.getWorldVertex();
	    Color vertexColor = reflectionModel.getColor(material,
	                                                 worldVertex.p,
	                                                 worldVertex.n.toUnit(),
	                                                 world.getLightSources(),
	                                                 world.getAmbientLight(),
	                                                 view.getCamera());
	    return new TexturedColoredMutableVertex(vertex.getScreenX(),
	                                            vertex.getScreenY(),
	                                            vertex.getCameraZ(),
	                                            vertexColor,
	                                            worldVertex.t.u,
	                                            worldVertex.t.v,
	                                            material);
	}

	@Override
	protected TextureColorDelta createDelta(TexturedColoredMutableVertex v1, TexturedColoredMutableVertex v2) {
		return new TextureColorDelta(v1, v2);
	}

	@Override
	protected void drawLine(TexturedColoredMutableVertex ts, TexturedColoredMutableVertex te, CameraObject3D object) {

		// _todo: investigate how we should handle specular and ambient maps, if they ever exist
		// Apparent each "map" element should be woven in during the corresponding phase in the reflection model.
		// however, we we don't calculate light for each pixel, we have to add it after the fact
		final Texture texture = ts.material.getMapDiffuseColor();

		if (ts.x > te.x) {
			// ahh, this did the trick! because the y-ordering when two points are aligned is undefined,
			// we'd get lines going in the "wrong" direction, so we need to flip start and end.
			// this fixes the bug where, at a certain rotation, the "bottom" half of the triangle,
			// being the *only* half at that particular rotation, would "disappear".
			TexturedColoredMutableVertex t = ts;
			ts = te;
			te = t;
		}
		TexturedColoredMutableVertex p = ts.copy();
		ColorProducer<TexturedColoredMutableVertex> colorProducer;
		// http://www.learnopengles.com/android-lesson-five-an-introduction-to-blending/
		switch (blendingMode) {
			case Additive:
				colorProducer = new TextureColoredGouraudColorProducer(textureFilter, texture) {
					@Override
					protected Color mixColors(Color textureColor, Color lightingColor) {
						return Colors.add(textureColor, lightingColor);
					}
				};
				break;
			case Interpolative:
				colorProducer = new TextureColoredGouraudColorProducer(textureFilter, texture) {
					@Override
					protected Color mixColors(Color textureColor, Color lightingColor) {
						// is there a recommended ratio or anything here? can I find it in the material maybe?
						return Colors.interpolate(textureColor, 0.8f, lightingColor, 0.2f);
					}
				};
				break;
			case Multiplicative:
				colorProducer = new TextureColoredGouraudColorProducer(textureFilter, texture) {
					@Override
					protected Color mixColors(Color textureColor, Color lightingColor) {
						return Colors.multiply(textureColor, lightingColor);
					}
				};
				break;
			default:
				throw new IllegalArgumentException("Unknown blending mode: " + blendingMode);
		}
		

		final double dz;
		// texture
        final double diz, duiz, dviz;
		// color
		final double dr, dg, db, da;

		// The *Delta classes interpolates for Y
		// Here we interpolate for X.
		// Perhaps we should make that semantic clearer.
		// Perhaps we should make an XInterpolationStep and
		// a YInterpolationStep that embody these things.

		final double esx = te.x - ts.x;
		if (esx > 0d) {
			dz = (te.z - ts.z) / esx;
			// texture
            diz = (te.iz - ts.iz) / esx;
            duiz = (te.uiz - ts.uiz) / esx;
            dviz = (te.viz - ts.viz) / esx;
			// color
			dr = (te.r - ts.r) / esx;
			dg = (te.g - ts.g) / esx;
			db = (te.b - ts.b) / esx;
			da = (te.a - ts.a) / esx;
		} else {
			dz = 0;
			// texture
            diz = duiz = dviz = 0d;
			// color
			dr = dg = db = da = 0;
		}
		// draw a line and get the colors from the texture map
		while (p.x < te.x) {
			painter.draw(p, colorProducer, object);
			p.z += dz;
			// texture
            p.iz += diz;
            p.uiz += duiz;
            p.viz += dviz;

			// color
			p.r += dr;
			p.g += dg;
			p.b += db;
			p.a += da;

			p.x++;
		}
	}

	private Color getLightingColor(TexturedColoredMutableVertex p) {
		return p.getColor();
	}

	private abstract class TextureColoredGouraudColorProducer implements ColorProducer<TexturedColoredMutableVertex> {
		private final TextureFilter textureFilter;
		private final Texture texture;

		private TextureColoredGouraudColorProducer(TextureFilter textureFilter, Texture texture) {
			this.textureFilter = textureFilter;
			this.texture = texture;
		}

		@Override
		public int getColor(TexturedColoredMutableVertex p) {
			/*
	        double z = 1 / p.iz;
            double u = p.uiz * z;
            double v = p.viz * z;
            */

			// http://cg.informatik.uni-freiburg.de/course_notes/graphics_06_texturing.pdf (page 21-25)
			// https://en.wikipedia.org/wiki/Texture_mapping#Perspective_correctness
			// https://gamedev.stackexchange.com/questions/71473/uv-interpolation-is-wrong-with-a-wide-fov
			// http://www.lysator.liu.se/~mikaelk/doc/perspectivetexture/
			// http://www.cse.ohio-state.edu/~whmin/courses/cse5542-2013-spring/15-texture.pdf (page 33)

			//double z = 1 / p.iz;
			double u = p.uiz / p.iz; // (u/z) / (1/z) 
			double v = p.viz / p.iz; // (v/z) / (1/z)
			Color textureColor = textureFilter.getColor(u, v, texture);
			Color lightingColor = getLightingColor(p);
			return mixColors(textureColor, lightingColor).getRGB();

		}

		protected abstract Color mixColors(Color textureColor, Color lightingColor);
		//protected abstract Color getLightingColor(TexturePhongMutableVertex p);
	}
}

