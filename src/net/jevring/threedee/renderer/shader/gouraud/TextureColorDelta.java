/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.shader.gouraud;

/**
 * A delta that, in addition to the difference in its parent class, also carries individual deltas for
 * texture coordinates u and v.
 *
 * @author markus@jevring.net
 * @see TextureOnlyDelta
 */
public class TextureColorDelta extends ColorDelta {
    public final double iz;
    public final double uiz;
    public final double viz;

	TextureColorDelta(TexturedColoredMutableVertex v1, TexturedColoredMutableVertex v2) {
		super(v1, v2);

        if (d > 0d) {
            iz = (v1.iz - v2.iz) / d;
            uiz = (v1.uiz - v2.uiz) / d;
            viz = (v1.viz - v2.viz) / d;
        } else {
            iz = uiz = viz = 0;
        }
	}

	@Override
	public String toString() {
        return super.toString() +
                ", 1/z=" + iz +
                ", u/z=" + uiz +
                ", v/z=" + viz;
	}
}
