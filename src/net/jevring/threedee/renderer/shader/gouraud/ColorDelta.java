/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.shader.gouraud;

import net.jevring.threedee.renderer.shader.Delta;

/**
 * A delta that, in addition to the difference in its parent class, also carries individual deltas for
 * red, green, blue and alpha components.
 *
 * @author markus@jevring.net
 */
public class ColorDelta extends Delta {
	public final double r;
	public final double g;
	public final double b;
	public final double a;

	ColorDelta(ColoredMutableVertex<? extends ColorDelta> v1, ColoredMutableVertex<? extends ColorDelta> v2) {
		super(v1, v2);

		if (d > 0d) {
			r = (v1.r - v2.r) / d;
			g = (v1.g - v2.g) / d;
			b = (v1.b - v2.b) / d;
			a = (v1.a - v2.a) / d;
		} else {
			r = g = b = a = 0;
		}
	}

	@Override
	public String toString() {
		return super.toString() +
				", r=" + r +
				", g=" + g +
				", b=" + b +
				", a=" + a;
	}
}
