/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.shader.gouraud;

import net.jevring.threedee.renderer.Colors;
import net.jevring.threedee.renderer.shader.MutableVertex;

import java.awt.*;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.round;

/**
 * @author markus@jevring.net
 */
public class ColoredMutableVertex<D extends ColorDelta> extends MutableVertex<D> {
	public double r;
	public double g;
	public double b;
	public double a;

	public ColoredMutableVertex(double x, double y, double z, Color c) {
		super(x, y, z);
		setColor(c);
	}

	public ColoredMutableVertex(double x, double y, double z, double r, double g, double b, double a) {
		super(x, y, z);
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	@Override
	public ColoredMutableVertex<D> copy() {
		return new ColoredMutableVertex<>(x, y, z, r, g, b, a);
	}

	@Override
	public void shift(D d) {
		super.shift(d);
		this.r += d.r;
		this.g += d.g;
		this.b += d.b;
		this.a += d.a;
	}

	public Color getColor() {
		try {
			return new Color(r(r), r(g), r(b), r(a));
		} catch (Exception e) {
			return new Color(limit(r), limit(g), limit(b), limit(a));
		}
	}

	private void setColor(Color c) {
		this.r = c.getRed();
		this.g = c.getGreen();
		this.b = c.getBlue();
		this.a = c.getAlpha();
	}

	protected int limit(double d) {
		return min(max(r(d), 0), 255);
	}

	protected static int r(double d) {
		return (int) Math.round(d);
	}

	@Override
	public String toString() {
		return super.toString() + ":r=" + r + ", g = " + g + ", b = " + b + ", a = " + a;
	}
}
