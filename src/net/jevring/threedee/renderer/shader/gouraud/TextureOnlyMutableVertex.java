/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.shader.gouraud;

import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.renderer.shader.MutableVertex;

/**
 * @author markus@jevring.net
 */
public class TextureOnlyMutableVertex extends MutableVertex<TextureOnlyDelta> {
	public final Material material;
    public double iz;
    public double uiz;
    public double viz; 

	public TextureOnlyMutableVertex(double x, double y, double z, double u, double v, Material material) {
		super(x, y, z);
        // Interpolation for texture coordinates is a bit different than X and Y coordinates.
        // Since we don't perform the projection we do for the points, 
        // we have to take the depth into account here, BEFORE we start
        // stepping through each line.
        this.iz = 1 / z;
        this.uiz = u / z;
        this.viz = v / z;
		this.material = material;
	}

    /**
     * This is a copy constructor we use to avoid a) having to carry u and z around, and b) re-inverting the values 
     * for every copy.
     */
    private TextureOnlyMutableVertex(double x, double y, double z, double iz, double uiz, double viz, Material material) {
        super(x, y, z);
        this.iz = iz;
        this.uiz = uiz;
        this.viz = viz;
        this.material = material;
    }

    @Override
	public TextureOnlyMutableVertex copy() {
		return new TextureOnlyMutableVertex(x, y, z, iz, uiz, viz, material);
	}

	@Override
	public void shift(TextureOnlyDelta d) {
		super.shift(d);
        this.iz += d.iz;
        this.uiz += d.uiz;
        this.viz += d.viz;
	}

	@Override
	public String toString() {
		return super.toString() + ":1/z=" + iz + ", u/z=" + uiz + ", v/z = " + viz + ", material = " + material;
	}
}
