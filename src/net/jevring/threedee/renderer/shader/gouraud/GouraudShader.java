/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.gouraud;

import net.jevring.threedee.View;
import net.jevring.threedee.World;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.painter.ZBufferedPainter;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.AbstractShader;

import java.awt.*;

/**
 * Gouraud shading means that the color values at the vertices are calculated, and the values "inside" the triangle
 * are simply interpolated.
 *
 * Basic gouraud triangle filler from <a href="http://www-users.mat.uni.torun.pl/~wrona/3d_tutor/tri_fillers.html">
 *     http://www-users.mat.uni.torun.pl/~wrona/3d_tutor/tri_fillers.html</a>, with some cleaning up.
 * @author markus@jevring.net
 */
public class GouraudShader extends AbstractShader<ColorDelta, ColoredMutableVertex<ColorDelta>> {
	

	public GouraudShader(World world, View view, ZBufferedPainter painter, boolean singleThreaded) {
		super(world, view, painter, singleThreaded);
	}

    @Override
    protected ColoredMutableVertex<ColorDelta> copy(ColoredMutableVertex<ColorDelta> colorDeltaColoredMutableVertex) {
        return colorDeltaColoredMutableVertex.copy();
    }

    @Override
    protected ColoredMutableVertex<ColorDelta> createVertex(Material material,
                                                            CameraVertex vertex,
                                                            ReflectionModel reflectionModel) {
		// NOTE: without using unit normals, all the colors are black
		// The color needs to be determined using world-space data, not camera space.
		// This is because all the things that affect the color are in world-coordinates, like the lights
	    Color vertexColor = reflectionModel.getColor(material,
	                                                 vertex.getWorldVertex().p,
	                                                 vertex.getWorldVertex().n.toUnit(),
	                                                 world.getLightSources(),
	                                                 world.getAmbientLight(),
	                                                 view.getCamera());
	    return new ColoredMutableVertex<>(vertex.getScreenX(), vertex.getScreenY(), vertex.getCameraZ(), vertexColor);
	}

	@Override
	protected ColorDelta createDelta(ColoredMutableVertex<ColorDelta> v1, ColoredMutableVertex<ColorDelta> v2) {
		return new ColorDelta(v1, v2);
	}

    @Override
	protected void drawLine(ColoredMutableVertex<ColorDelta> s, ColoredMutableVertex<ColorDelta> e, CameraObject3D object) {

		// since we're in the gouraud shader, we know that these are ColoredMutableVertices.
		// it would be nice to resolve this through generics, but that doesn't seem to be possible
		// in AbstractShader
		if (s.x > e.x) {
			// ahh, this did the trick! because the y-ordering when two points are aligned is undefined,
			// we'd get lines going in the "wrong" direction, so we need to flip start and end.
			// this fixes the bug where, at a certain rotation, the "bottom" half of the triangle,
			// being the *only* half at that particular rotation, would "disappear".
			ColoredMutableVertex<ColorDelta> t = s;
			s = e;
			e = t;
		}
		double dr, dg, db, da, dz;
		final double esx = e.x - s.x;
		if (esx > 0d) {
			dr = (e.r - s.r) / esx;
			dg = (e.g - s.g) / esx;
			db = (e.b - s.b) / esx;
			da = (e.a - s.a) / esx;
			dz = (e.z - s.z) / esx;
		} else {
			dr = dg = db = da = dz = 0d;
		}
	    final ColoredMutableVertex<ColorDelta> p = s.copy();
	    ColorProducer<ColoredMutableVertex<ColorDelta>> colorProducer = new ColorProducer<ColoredMutableVertex<ColorDelta>>() {
		    @Override
		    public int getColor(ColoredMutableVertex<ColorDelta> p) {
			    return p.getColor().getRGB();
		    }
	    };
		// draw a line of linearly interpolated colors
		while (p.x < e.x) {
			painter.draw(p, colorProducer, object);
			p.r += dr;
			p.g += dg;
			p.b += db;
			p.a += da;
			p.z += dz;
			p.x++;
		}
	}
}
