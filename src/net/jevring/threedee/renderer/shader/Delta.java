/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.shader;

/**
 * Contains values used for interpolation. A delta for a given run through the algorithm contains the size of the steps
 * taken for a given part for the following values: x, r, g, b, a
 */
public class Delta {
	protected final double d;
	public final double x;
	public final double z;

	/**
	 * Create a delta between two vertices.
	 *
	 * @param v1 vertex 1
	 * @param v2 vertex 2
	 */
	public Delta(MutableVertex<? extends Delta> v1, MutableVertex<? extends Delta> v2) {
		d = v1.y - v2.y;
		if (d > 0d) {
			x = (v1.x - v2.x) / d;
			z = (v1.z - v2.z) / d;
		} else {
			x = z = 0;
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ":" +
				"x=" + x +
				", z=" + z;
	}
}
