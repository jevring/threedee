/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.phong;

import net.jevring.threedee.View;
import net.jevring.threedee.World;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.primitives.Texture;
import net.jevring.threedee.primitives.Vertex;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.Colors;
import net.jevring.threedee.renderer.painter.ZBufferedPainter;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.AbstractShader;
import net.jevring.threedee.renderer.shader.gouraud.BlendingMode;
import net.jevring.threedee.renderer.texturefilter.TextureFilter;

import java.awt.*;

/**
 * The phong shader, unlike the {@link net.jevring.threedee.renderer.shader.gouraud.GouraudShader}, will interpolate the normals
 * and calculate the shading and texture for every pixel. This is significantly more costly, but leads to better lighting quality.
 * This shader also takes texture coordinates into account.
 *
 * @author markus@jevring.net
 */
public class TexturePhongShader extends AbstractShader<TexturePhongDelta, TexturePhongMutableVertex> {
    private final TextureFilter textureFilter;
    private final BlendingMode blendingMode;

    public TexturePhongShader(World world, View view, ZBufferedPainter painter, boolean singleThreaded, TextureFilter textureFilter, BlendingMode blendingMode) {
        super(world, view, painter, singleThreaded);
        this.textureFilter = textureFilter;
        this.blendingMode = blendingMode;
    }

    @Override
    protected TexturePhongMutableVertex createVertex(Material material, CameraVertex vertex, ReflectionModel reflectionModel) {
	    Vertex worldVertex = vertex.getWorldVertex();
	    return new TexturePhongMutableVertex(vertex.getScreenX(),
	                                         vertex.getScreenY(),
	                                         vertex.getCameraZ(), worldVertex.n, worldVertex.p,
	                                         material,
	                                         reflectionModel, worldVertex.t.u, worldVertex.t.v);
    }

    @Override
    protected TexturePhongDelta createDelta(TexturePhongMutableVertex v1, TexturePhongMutableVertex v2) {
        return new TexturePhongDelta(v1, v2);
    }

    @Override
    protected TexturePhongMutableVertex copy(TexturePhongMutableVertex v) {
        return v.copy();
    }

    @Override
    protected void drawLine(TexturePhongMutableVertex s, TexturePhongMutableVertex e, CameraObject3D object) {
        final Texture texture = s.material.getMapDiffuseColor();

        if (s.x > e.x) {
            // ahh, this did the trick! because the y-ordering when two points are aligned is undefined,
            // we'd get lines going in the "wrong" direction, so we need to flip start and end.
            // this fixes the bug where, at a certain rotation, the "bottom" half of the triangle,
            // being the *only* half at that particular rotation, would "disappear".
            TexturePhongMutableVertex t = s;
            s = e;
            e = t;
        }
	    TexturePhongMutableVertex p = s.copy();
	    ColorProducer<TexturePhongMutableVertex> colorProducer;
	    // http://www.learnopengles.com/android-lesson-five-an-introduction-to-blending/
	    switch (blendingMode) {
		    case Additive:
			    colorProducer = new TexturePhongColorProducer(textureFilter, texture) {
				    @Override
				    protected Color mixColors(Color textureColor, Color lightingColor) {
					    return Colors.add(textureColor, lightingColor);
				    }
			    };
			    break;
		    case Interpolative:
			    colorProducer = new TexturePhongColorProducer(textureFilter, texture) {
				    @Override
				    protected Color mixColors(Color textureColor, Color lightingColor) {
					    // is there a recommended ratio or anything here? can I find it in the material maybe?
					    return Colors.interpolate(textureColor, 0.8f, lightingColor, 0.2f);
				    }
			    };
			    break;
		    case Multiplicative:
			    colorProducer = new TexturePhongColorProducer(textureFilter, texture) {
				    @Override
				    protected Color mixColors(Color textureColor, Color lightingColor) {
					    return Colors.multiply(textureColor, lightingColor);
				    }
			    };
			    break;
		    default:
			    throw new IllegalArgumentException("Unknown blending mode: " + blendingMode);
	    }

        final double dz;
        final Point3D dn;
        final Point3D dwsp;
        // texture
        final double diz, duiz, dviz;

        final double esx = e.x - s.x;
        if (esx > 0d) {
            dz = (e.z - s.z) / esx;
            dn = e.normal.minus(s.normal).divide(esx);
            dwsp = e.worldSpacePoint.minus(s.worldSpacePoint).divide(esx);
            // texture
            diz = (e.iz - s.iz) / esx;
            duiz = (e.uiz - s.uiz) / esx;
            dviz = (e.viz - s.viz) / esx;
        } else {
            dz = 0d;
            dn = new Point3D(0, 0, 0);
            dwsp = new Point3D(0, 0, 0);
            // texture
            diz = duiz = dviz = 0d;
        }
        // draw a line of linearly solid color
        while (p.x < e.x) {
	        painter.draw(p, colorProducer, object);

            p.z += dz;
            p.normal = p.normal.plus(dn);
            p.worldSpacePoint = p.worldSpacePoint.plus(dwsp);
            // texture
            p.iz += diz;
            p.uiz += duiz;
            p.viz += dviz;

            p.x++;
        }
    }

	private Color getLightingColor(TexturePhongMutableVertex p) {
		// NOTE: without using unit normals, all the colors are black
		// The color needs to be determined using world-space data, not camera space.
		// This is because all the things that affect the color are in world-coordinates, like the lights
		return p.reflectionModel.getColor(p.material,
		                                  p.worldSpacePoint,
		                                  p.normal.toUnit(),
		                                  world.getLightSources(),
		                                  world.getAmbientLight(),
		                                  view.getCamera());
	}

	// todo: merge all things that do texture handling, as there are more of them than things that do color handling.
	// that said, the things that handle only color cannot handle texture, so perhaps this is a step in the wrong direction.
	// none hte less, it would make classes like this common, instead of being scattered in multiple (well, only 2) places
	private abstract class TexturePhongColorProducer implements ColorProducer<TexturePhongMutableVertex> {
		private final TextureFilter textureFilter;
		private final Texture texture;

		private TexturePhongColorProducer(TextureFilter textureFilter, Texture texture) {
			this.textureFilter = textureFilter;
			this.texture = texture;
		}

		@Override
		public int getColor(TexturePhongMutableVertex p) {
			/*
	        double z = 1 / p.iz;
            double u = p.uiz * z;
            double v = p.viz * z;
            */

			// http://cg.informatik.uni-freiburg.de/course_notes/graphics_06_texturing.pdf (page 21-25)
			// https://en.wikipedia.org/wiki/Texture_mapping#Perspective_correctness
			// https://gamedev.stackexchange.com/questions/71473/uv-interpolation-is-wrong-with-a-wide-fov
			// http://www.lysator.liu.se/~mikaelk/doc/perspectivetexture/
			// http://www.cse.ohio-state.edu/~whmin/courses/cse5542-2013-spring/15-texture.pdf (page 33)

			//double z = 1 / p.iz;
			double u = p.uiz / p.iz; // (u/z) / (1/z) 
			double v = p.viz / p.iz; // (v/z) / (1/z)
			Color textureColor = textureFilter.getColor(u, v, texture);
			Color lightingColor = getLightingColor(p);
			return mixColors(textureColor, lightingColor).getRGB();

		}

		protected abstract Color mixColors(Color textureColor, Color lightingColor);
		//protected abstract Color getLightingColor(TexturePhongMutableVertex p);
	}
}
