/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.phong;

import net.jevring.threedee.View;
import net.jevring.threedee.World;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.painter.ZBufferedPainter;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.AbstractShader;

import java.awt.*;

/**
 * The phong shader, unlike the {@link net.jevring.threedee.renderer.shader.gouraud.GouraudShader}, will interpolate the normals
 * and calculate the shading and texture for every pixel. This is significantly more costly, but leads to better lighting quality.
 *
 * @author markus@jevring.net
 */
public class PhongShader extends AbstractShader<PhongDelta, PhongMutableVertex<PhongDelta>> {
	public PhongShader(World world, View view, ZBufferedPainter painter, boolean singleThreaded) {
		super(world, view, painter, singleThreaded);
	}

	@Override
	protected PhongMutableVertex<PhongDelta> createVertex(Material material,
	                                                      CameraVertex vertex,
	                                                      ReflectionModel reflectionModel) {
		return new PhongMutableVertex<>(vertex.getScreenX(),
		                                vertex.getScreenY(),
		                                vertex.getCameraZ(),
		                                vertex.getWorldVertex().n,
		                                vertex.getWorldVertex().p,
		                                material,
		                                reflectionModel);
	}

	@Override
	protected PhongDelta createDelta(PhongMutableVertex<PhongDelta> v1, PhongMutableVertex<PhongDelta> v2) {
		return new PhongDelta(v1, v2);
	}

	@Override
	protected PhongMutableVertex<PhongDelta> copy(PhongMutableVertex<PhongDelta> deltaMutableVertex) {
		return deltaMutableVertex.copy();
	}

	@Override
	protected void drawLine(PhongMutableVertex<PhongDelta> s, PhongMutableVertex<PhongDelta> e, CameraObject3D object) {
		if (s.x > e.x) {
			// ahh, this did the trick! because the y-ordering when two points are aligned is undefined,
			// we'd get lines going in the "wrong" direction, so we need to flip start and end.
			// this fixes the bug where, at a certain rotation, the "bottom" half of the triangle,
			// being the *only* half at that particular rotation, would "disappear".
			PhongMutableVertex<PhongDelta> t = s;
			s = e;
			e = t;
		}
		PhongMutableVertex<PhongDelta> p = s.copy();
		ColorProducer<PhongMutableVertex<PhongDelta>> colorProducer = new ColorProducer<PhongMutableVertex<PhongDelta>>() {
			@Override
			public int getColor(PhongMutableVertex<PhongDelta> p) {
				// NOTE: without using unit normals, all the colors are black
				// The color needs to be determined using world-space data, not camera space.
				// This is because all the things that affect the color are in world-coordinates, like the lights
				Color pixelColor = p.reflectionModel.getColor(p.material,
				                                              p.worldSpacePoint,
				                                              p.normal.toUnit(),
				                                              world.getLightSources(),
				                                              world.getAmbientLight(),
				                                              view.getCamera());
				return pixelColor.getRGB();
			}
		};
		final double esx = e.x - s.x;
		final double dz;
		final Point3D dn;
		final Point3D dwsp;
		if (esx > 0d) {
			dz = (e.z - s.z) / esx;
			dn = e.normal.minus(s.normal).divide(esx);
			dwsp = e.worldSpacePoint.minus(s.worldSpacePoint).divide(esx);
		} else {
			dz = 0d;
			dn = new Point3D(0, 0, 0);
			dwsp = new Point3D(0, 0, 0);
		}
		// draw a line of linearly solid color
		while (p.x < e.x) {
			painter.draw(p, colorProducer, object);
			p.z += dz;
			p.normal = p.normal.plus(dn);
			p.worldSpacePoint = p.worldSpacePoint.plus(dwsp);
			p.x++;
		}
	}
}
