/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.phong;

import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.MutableVertex;

/**
 * A mutable vertex for the phone shader that tracks the normal and the world space point,
 * as they are needed in the reflection model.
 *
 * @author markus@jevring.net
 */
public class PhongMutableVertex<D extends PhongDelta> extends MutableVertex<D> {
    public Point3D normal;
    public Point3D worldSpacePoint;
    public final Material material;
    public final ReflectionModel reflectionModel;

    public PhongMutableVertex(double x, double y, double z, Point3D normal, Point3D worldSpacePoint, Material material, ReflectionModel reflectionModel) {
        super(x, y, z);
        this.normal = normal;
        this.worldSpacePoint = worldSpacePoint;
        this.material = material;
        this.reflectionModel = reflectionModel;
    }

    @Override
    public PhongMutableVertex<D> copy() {
        return new PhongMutableVertex<D>(x, y, z, normal, worldSpacePoint, material, reflectionModel);
    }

    @Override
    public void shift(D phongDelta) {
        super.shift(phongDelta);
        this.normal = this.normal.plus(phongDelta.normal);
        this.worldSpacePoint = this.worldSpacePoint.plus(phongDelta.worldSpacePoint);
    }
}
