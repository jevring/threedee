/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.phong;

import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;

/**
 * A mutable vertex for the phone shader that tracks the normal and the world space point,
 * as they are needed in the reflection model.
 *
 * @author markus@jevring.net
 */
public class TexturePhongMutableVertex extends PhongMutableVertex<TexturePhongDelta> {
    public double iz;
    public double uiz;
    public double viz;

    public TexturePhongMutableVertex(double x, double y, double z, Point3D normal, Point3D worldSpacePoint, Material material, ReflectionModel reflectionModel, double u, double v) {
        super(x, y, z, normal, worldSpacePoint, material, reflectionModel);
        // Interpolation for texture coordinates is a bit different than X and Y coordinates.
        // Since we don't perform the projection we do for the points, 
        // we have to take the depth into account here, BEFORE we start
        // stepping through each line.
        this.iz = 1 / z;
        this.uiz = u / z;
        this.viz = v / z;
    }

    public TexturePhongMutableVertex(double x, double y, double z, Point3D normal, Point3D worldSpacePoint, Material material, ReflectionModel reflectionModel, double iz, double uiz, double viz) {
        super(x, y, z, normal, worldSpacePoint, material, reflectionModel);
        this.iz = iz;
        this.uiz = uiz;
        this.viz = viz;
    }

    @Override
    public TexturePhongMutableVertex copy() {
        return new TexturePhongMutableVertex(x, y, z, normal, worldSpacePoint, material, reflectionModel, iz, uiz, viz);
    }

    @Override
    public void shift(TexturePhongDelta d) {
        super.shift(d);
        this.iz += d.iz;
        this.uiz += d.uiz;
        this.viz += d.viz;
    }
}
