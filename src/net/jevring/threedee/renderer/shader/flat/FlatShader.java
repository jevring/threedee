/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader.flat;

import net.jevring.threedee.View;
import net.jevring.threedee.World;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.camera.CameraFace;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.painter.ZBufferedPainter;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.AbstractShader;
import net.jevring.threedee.renderer.shader.Delta;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A flat shader paints each face using a single, commonly the of the first vertex.
 * A flat shader often delivers worse-looking results than a gouraud shader, but renders faces faster.
 */
public class FlatShader extends AbstractShader<Delta, StaticColorMutableVertex> {
	public FlatShader(World world, View view, ZBufferedPainter painter, boolean singleThreaded) {
		super(world, view, painter, singleThreaded);
	}

	/**
	 * Prepares vertexes using only a single color. This turns out to increase performance by approximately 100% (from ~20
	 * to ~40 fps).
	 *
	 * @param face            the face for which to prepare the vertices
	 * @param reflectionModel the reflection model used to calculate the color
	 * @param material        the material that contains the color information
	 * @return a list of vertices that is ready for drawing or {@code null} if the vertices were culled
	 */
	@Override
	protected List<StaticColorMutableVertex> prepare(CameraFace face, ReflectionModel reflectionModel, Material material) {
		Color color = null;
		List<StaticColorMutableVertex> vertices = new ArrayList<>();
		for (CameraVertex vertex : face.getVertices()) {
			if (color == null) {
				color = reflectionModel.getColor(material, vertex.getWorldVertex().p, vertex.getWorldVertex().n.toUnit(),
				                                 // using face.normal() here looks nicer, but the lights don't work
				                                 world.getLightSources(), world.getAmbientLight(), view.getCamera());
			}
			vertices.add(new StaticColorMutableVertex(vertex.getScreenX(), vertex.getScreenY(), vertex.getCameraZ(), color));
		}
		return vertices;
	}

	@Override
	protected StaticColorMutableVertex copy(StaticColorMutableVertex staticColorMutableVertex) {
		return staticColorMutableVertex.copy();
	}

	@Override
	protected StaticColorMutableVertex createVertex(Material material, CameraVertex vertex, ReflectionModel reflectionModel) {
		throw new UnsupportedOperationException("Should never happen as we override prepare()");
	}

	@Override
	protected Delta createDelta(StaticColorMutableVertex v1, StaticColorMutableVertex v2) {
		return new Delta(v1, v2);
	}

	protected void drawLine(StaticColorMutableVertex s, StaticColorMutableVertex e, CameraObject3D object) {
		if (s.x > e.x) {
			// ahh, this did the trick! because the y-ordering when two points are aligned is undefined,
			// we'd get lines going in the "wrong" direction, so we need to flip start and end.
			// this fixes the bug where, at a certain rotation, the "bottom" half of the triangle,
			// being the *only* half at that particular rotation, would "disappear".
			StaticColorMutableVertex t = s;
			s = e;
			e = t;
		}
		StaticColorMutableVertex p = s.copy();
		ColorProducer<StaticColorMutableVertex> colorProducer = new ColorProducer<StaticColorMutableVertex>() {
			@Override
			public int getColor(StaticColorMutableVertex p) {
				return p.getColor().getRGB();
			}
		};
		final double esx = e.x - s.x;
		final double dz;
		if (esx > 0d) {
			dz = (e.z - s.z) / esx;
		} else {
			dz = 0d;
		}
		// draw a line of linearly solid color
		while (p.x < e.x) {
			painter.draw(p, colorProducer, object);
			p.z += dz;
			p.x++;
		}
	}
}
