/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.shader;

/**
 * This is a convenience class that holds mutable data while we execute the algorithm.
 *
 * @author markus@jevring.net
 */
public class MutableVertex<D extends Delta> {
	public double x;
	public double y;
	public double z;

	public MutableVertex(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public MutableVertex<D> copy() {
		return new MutableVertex<>(x, y, z);
	}

	/**
	 * Shifts a vertex by a set of deltas.
	 *
	 * @param d the deltas for each part
	 */
	public void shift(D d) {
		this.x += d.x;
		this.z += d.z;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ":x=" + x + ", y = " + y + ", z = " + z;
	}
}
