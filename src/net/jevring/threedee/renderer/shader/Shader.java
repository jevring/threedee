/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.shader;

import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.camera.CameraFace;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;

/**
 * A shader is responsible for drawing faces using a {@link net.jevring.threedee.renderer.reflectionmodel.ReflectionModel reflection model}
 * and a {@link Material material} to calculate the colors needed.
 *
 * @author markus@jevring.net
 */
public interface Shader {

	/**
	 * Draws a face. If there is a valid reason for which the face should not be drawn,
	 * for example due to back-face culling, this method will return {@code false}.
	 *
	 * @param face            a face in camera coordinates
	 * @param reflectionModel the reflection model used to get the colors
	 * @param material        the material with which to draw the face
	 * @param object          the object this face belongs to. This goes into a buffer for use in context later.
	 * @return {@code true} if the face was drawn, {@code false} if it wasn't.
	 */
	public void draw(CameraFace face, ReflectionModel reflectionModel, Material material, CameraObject3D object);
}
