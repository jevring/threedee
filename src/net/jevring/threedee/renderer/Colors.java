/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer;

import java.awt.*;

import static java.lang.Math.min;

/**
 * Mathematical operations for colors.<br>
 *
 * @author markus@jevring.net
 */
public class Colors {
	public static Color color(float[] components) {
		return color(components[0], components[1], components[2], components[3]);
	}

	public static Color color(float red, float green, float blue, float alpha) {
		try {
			return new Color(red, green, blue, alpha);
		} catch (Exception e) {
			// todo: should we check this ourselves or rely on this exception?
			// this is what happens when colors are saturated.
			return new Color(min(red, 1f), min(green, 1f), min(blue, 1f), min(alpha, 1f));
		}
	}

	public static Color multiply(Color color, float intensity) {
		// preserves alpha, just like Color.darker() and Color.brighter()
		final float[] components = color.getComponents(null);
		return color(components[0] * intensity, components[1] * intensity, components[2] * intensity, components[3]);
	}

	public static Color add(Color c1, Color c2) {
		final float[] c1c = c1.getComponents(null);
		final float[] c2c = c2.getComponents(null);
		return color(c1c[0] + c2c[0], c1c[1] + c2c[1], c1c[2] + c2c[2], c1c[3] + c2c[3]);
	}

	public static Color add(Color c1, Color c2, Color c3) {
		final float[] c1c = c1.getComponents(null);
		final float[] c2c = c2.getComponents(null);
		final float[] c3c = c3.getComponents(null);
		return color(c1c[0] + c2c[0] + c3c[0], c1c[1] + c2c[1] + c3c[1], c1c[2] + c2c[2] + c3c[2], c1c[3] + c2c[3] + c3c[3]);
	}

	public static Color interpolate(Color c1, float p1, Color c2, float p2) {
		final float[] c1c = c1.getColorComponents(null);
		c1c[0] *= p1;
		c1c[1] *= p1;
		c1c[2] *= p1;
		//c1c[3] *= p1;
		
		final float[] c2c = c2.getColorComponents(null);
		c2c[0] *= p2;
		c2c[1] *= p2;
		c2c[2] *= p2;
		//c2c[3] *= p2;

		return color(c1c[0] + c2c[0], c1c[1] + c2c[1], c1c[2] + c2c[2], 1f);
		
		// changing to not calling color those extra times removed Color<init>(float/4) from being a hotspot, nice
		/*
		Color c1m = multiply(c1, p1);
		Color c2m = multiply(c2, p2);
		return add(c1m, c2m);
		*/
	}

	public static Color multiply(Color c1, Color c2) {
		final float[] c1c = c1.getComponents(null);
		final float[] c2c = c2.getComponents(null);
		return color(c1c[0] * c2c[0], c1c[1] * c2c[1], c1c[2] * c2c[2], c1c[3] * c2c[3]);
	}
}
