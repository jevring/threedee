/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.painter;

import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.shader.MutableVertex;

import java.awt.*;
import java.util.Optional;

/**
 * Paints the scene according to the z-buffer.
 * The color producer is <b>not</b> called when we have a z-miss.
 *
 * @author markus@jevring.net
 */
public interface ZBufferedPainter {
	/**
	 * Draws a pixel on the screen, unless we have a z-miss.
	 */
	public <V extends MutableVertex<?>> void draw(V mutableVertex, ColorProducer<V> colorProducer, CameraObject3D object);

	/**
	 * Fetches the drawn pixels.
	 */
	public int[] getPixels();

	/**
	 * Gets the z-value at the specified screen coordinate.
	 */
	public double getZValueAt(int x, int y);
	
	public Optional<CameraObject3D> getObjectAt(int x, int y);

	public void highlight(CameraObject3D cameraObject3D, Optional<Color> outlineColor, Optional<Color> bodyColor);
}
