/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.painter;

import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.shader.MutableVertex;

import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Paints pixels to the screen and maintains a z-buffer to ensure that only the
 * pixels closest to the camera are painted.
 * 
 * <p>This is a debug painter. It will paint the miss ratio as a grayscale heatmap 
 * instead of the actual picture. The more misses for a pixel, the brighter it gets.
 * A miss is "good" in the sense that we don't have to call the color producer.
 * It's "bad" in the sense that we didn't do any pre-processing to avoid it.
 *
 * @author markus@jevring.net
 */
public class MissRateZBufferedPainter implements DebugZBufferedPainter {
	private final int width;
	private final int height;
	private final double[] zBuffer;
	private final int[] pixels;

	public MissRateZBufferedPainter(int width, int height) {
		this.width = width;
		this.height = height;

		zBuffer = new double[width * height];
		pixels = new int[width * height];
		Arrays.fill(zBuffer, -Double.MAX_VALUE);
	}
	
	/**
	 * Defers the fetching of the color until the z-buffer test has been completed.
	 * No color is generated for pixels that "miss" the z-buffer.
	 */
	@Override
	public <V extends MutableVertex<?>> void draw(V mutableVertex, ColorProducer<V> colorProducer, CameraObject3D object) {
		int x = r(mutableVertex.x);
		int y = r(mutableVertex.y);
		if (x > 0 && y > 0 && x < width - 1 && y < height - 1) {
			// only draw things that are actually on the screen
			int i = y * width + x;
			double currentZValueForPixel = zBuffer[i];
			double z = mutableVertex.z;
			if (currentZValueForPixel < z) {
				zBuffer[i] = z;
				// this was a hit. Don't modify anything
			} else {
				// this was a miss. increase the intensity for this pixel
				pixels[i]++;
			}
		}
	}

	private static int r(double d) {
		return (int) Math.round(d);
	}

	@Override
	public int[] getPixels() {
		int max = getMaxMisses();
		int[] out = new int[pixels.length];
		for (int i = 0; i < pixels.length; i++) {
			int pixel = pixels[i];
			float intensity = (float) pixel / (float) max;
			out[i] = Color.getHSBColor(0f, 0f, intensity).getRGB();
		}
		return out;
	}

	public int getMaxMisses() {
		int max = 0;
		for (int pixel : pixels) {
			if (pixel > max) {
				max = pixel;
			}
		}
		return max;
	}

	@Override
    public double getZValueAt(int x, int y) {
        int i = y * width + x;
        return zBuffer[i];
    }

	@Override
	public Optional<CameraObject3D> getObjectAt(int x, int y) {
		return Optional.empty();
	}

	@Override
	public void highlight(CameraObject3D cameraObject3D, Optional<Color> outlineColor, Optional<Color> bodyColor) {
		// do nothing
	}

	@Override
	public List<String> getDebugMessages() {
		return Collections.singletonList(String.format("Max misses: %d", getMaxMisses()));
	}
}
