/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.painter;

import net.jevring.threedee.primitives.camera.CameraFace;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.ColorProducer;
import net.jevring.threedee.renderer.Colors;
import net.jevring.threedee.renderer.shader.MutableVertex;

import java.awt.*;
import java.util.Arrays;
import java.util.Optional;

/**
 * Paints pixels to the screen and maintains a z-buffer to ensure that only the
 * pixels closest to the camera are painted.
 *
 * @author markus@jevring.net
 */
public class DefaultZBufferedPainter implements ZBufferedPainter {
	private final int width;
	private final int height;
	private final double[] zBuffer;
	private final int[] pixels;
	private final CameraObject3D[] objects;

	public DefaultZBufferedPainter(int width, int height) {
		this.width = width;
		this.height = height;

		int size = width * height;
		zBuffer = new double[size];
		pixels = new int[size];
		objects = new CameraObject3D[size];
		Arrays.fill(zBuffer, -Double.MAX_VALUE);
	}

	/**
	 * Defers the fetching of the color until the z-buffer test has been completed.
	 * No color is generated for pixels that "miss" the z-buffer.
	 */
	@Override
	public <V extends MutableVertex<?>> void draw(V mutableVertex, ColorProducer<V> colorProducer, CameraObject3D object) {
		int x = r(mutableVertex.x);
		int y = r(mutableVertex.y);
		if (x > 0 && y > 0 && x < width - 1 && y < height - 1) {
			// only draw things that are actually on the screen
			int i = y * width + x;
			double currentZValueForPixel = zBuffer[i];
			double z = mutableVertex.z;
			if (currentZValueForPixel < z) {
				zBuffer[i] = z;
				// todo: we could defer this even more, until we know that we've rendered everything. 
				// then we take a line per CPU and just push out the colors.
				// this would mean that we'd have to copy the state, though.
				// will only really be valuable for expensive things like phong+bilinear filtering.
				pixels[i] = colorProducer.getColor(mutableVertex);
				// todo: it would be super cool if we could check the mouse-over object here and do the determination.
				// potentially based on the PREVIOUS frame's mouse location
				objects[i] = object;
			}
		}
	}

	private static int r(double d) {
		return (int) Math.round(d);
	}

	@Override
	public int[] getPixels() {
		return pixels;
	}

    @Override
    public double getZValueAt(int x, int y) {
        int i = y * width + x;
	    if (i > zBuffer.length) {
		    // this happens when we resize the window. 
		    // likely due to lack of synchronization somewhere
		    return -Double.MAX_VALUE;
	    }
        return zBuffer[i];
    }

	@Override
	public Optional<CameraObject3D> getObjectAt(int x, int y) {
		int i = y * width + x;
		if (i > zBuffer.length) {
			// this happens when we resize the window. 
			// likely due to lack of synchronization somewhere
			return Optional.empty();
		}
		return Optional.ofNullable(objects[i]);
	}

	@Override
	public void highlight(CameraObject3D object, Optional<Color> outlineColor, Optional<Color> bodyColor) {
		int xMin = width;
		int xMax = 0;
		int yMin = height;
		int yMax = 0;
		// only look inside the "screen space bounding box", as this is likely to be a MUCH smaller space.
		// the downside, of course, is that it might be costly to calculate the screen space bounding box 
		// for an object. As these are camera-space objects, they don't persist between frames, so we can't
		// store the calculated bounding box either. We also wouldn't want to, because this call is only
		// invoked once per frame, for a single object.

		for (CameraFace cameraFace : object.getFaces()) {
			for (CameraVertex cameraVertex : cameraFace.getVertices()) {
				xMin = Math.min(xMin, cameraVertex.getScreenX());
				xMax = Math.max(xMax, cameraVertex.getScreenX());
				yMin = Math.min(yMin, cameraVertex.getScreenY());
				yMax = Math.max(yMax, cameraVertex.getScreenY());
			}
		}
		xMin = Math.max(xMin, 0);
		yMin = Math.max(yMin, 0);
		xMax = Math.min(xMax, width);
		yMax = Math.min(yMax, height);

		for (int y = yMin; y < yMax; y++) {
			for (int x = xMin; x < xMax; x++) {
				int i = y * width + x;
				if (objects[i] == object) {
					if (bodyColor.isPresent()) {
						// We can't do a flood fill here, because it's perfectly possible that an object is partially obscured
						// by another. In this case, if the obscuring object "cuts" the highlighted object in two (or more)
						// parts, a flood fill algorithm would only fill the one connected to the mouse pointer.
						// Interestingly, however, even this incredibly simplistic approach almost doesn't even register in 
						// the profiler, so the incurred overhead is minimal.
						Color bc = bodyColor.get();
						// If this had been a collection, not an array, we could have done some cool stream stuff here...
						// todo: let this ratio be configurable
						pixels[i] = Colors.interpolate(new Color(pixels[i]), 0.75f, bc, 0.25f).getRGB();
					}
					if (outlineColor.isPresent()) {
						// todo: the thicker this gets, the weirder it looks...
						int outlineThickness = 2; // todo: configurable
						int oc = outlineColor.get().getRGB();

						// highlight everything 'outlineThickness' steps away from this, provided it's not the object itself.
						// we're likely painting some pixels multiple times, but since we're not blending, that's fine

						// todo: it still looks like things have been cut off at the edges

						int iYMin = Math.max(0, y - outlineThickness);
						int iXMin = Math.max(0, x - outlineThickness);
						// +1 to make sure we go one step beyond the pixel itself.
						int iYMax = Math.min(height, y + outlineThickness + 1);
						int iXMax = Math.min(width, x + outlineThickness + 1);
						for (int iy = iYMin; iy < iYMax; iy++) {
							for (int ix = iXMin; ix < iXMax; ix++) {
								int j = iy * width + ix;
								if (objects[j] != object) {
									pixels[j] = oc;
								}
							}
						}
					}
				}
			}
		}
	}
}
