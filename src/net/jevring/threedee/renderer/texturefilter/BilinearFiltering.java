/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.renderer.texturefilter;

import net.jevring.threedee.primitives.Texture;
import net.jevring.threedee.renderer.Colors;

import java.awt.*;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Bilinear filtering takes a weighted average of the 4 texels closest to the desired texture coordinates.
 * While this is significantly more costly than {@link NearestNeighbor}, it leads to a smoother result.
 *
 * @author markus@jevring.net
 */
public class BilinearFiltering implements TextureFilter {
	@Override
	public Color getColor(double u, double v, Texture texture) {
		if (!texture.isClamp()) {
			// https://en.wikipedia.org/wiki/Wrapping_%28graphics%29
			if (u < 0) {
				u = 1d - u;
			}
			u = u % 1d;
			if (v < 0) {
				v = 1d - v;
			}
			v = v % 1d;
		} else {
			// https://en.wikipedia.org/wiki/Clamping_%28graphics%29
			u = min(max(u, 0d), 1.0);
			v = min(max(v, 0d), 1.0);
		}

		// Apparently, OpenGL and Direct3D have different ways of interpreting the v-coordinate.
		// That's why we have to invert it, because whatever is generating our models works in one way,
		// and our texture map works in another.
		v = 1d - v;

		int w = texture.getWidth() - 1;
		int h = texture.getHeight() - 1;
		double ux = u * (double) w;
		double vy = v * (double) h;
		int x = (int) Math.floor(ux);
		int y = (int) Math.floor(vy);
		float rightProportion = (float) (ux - x);
		float topProportion = (float) (vy - y);

		int xPlus1 = min(x + 1, w);
		int yPlus1 = min(y + 1, h);

		// row 1
		int rx1 = texture.getColor(x, y);
		int lx1 = texture.getColor(xPlus1, y);

		// row 2
		int rx2 = texture.getColor(x, yPlus1);
		int lx2 = texture.getColor(xPlus1, yPlus1);

		Color crx1 = new Color(rx1);
		Color clx1 = new Color(lx1);
		Color crx2 = new Color(rx2);
		Color clx2 = new Color(lx2);

		float oneMinusRightProportion = 1f - rightProportion;
		Color row1 = Colors.interpolate(crx1, oneMinusRightProportion, clx1, rightProportion);
		Color row2 = Colors.interpolate(crx2, oneMinusRightProportion, clx2, rightProportion);

		return Colors.interpolate(row1, (1f - topProportion), row2, topProportion);
	}
}
