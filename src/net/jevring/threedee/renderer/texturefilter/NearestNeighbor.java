/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.texturefilter;

import net.jevring.threedee.primitives.Texture;

import java.awt.*;

import static java.lang.Math.round;

/**
 * This simple texture filter gets the one texel that corresponds to the texture coordinates.
 *
 * @author markus@jevring.net
 */
public class NearestNeighbor implements TextureFilter {
	@Override
	public Color getColor(double u, double v, Texture texture) {
		if (!texture.isClamp()) {
			// https://en.wikipedia.org/wiki/Wrapping_%28graphics%29
			if (u < 0) {
				u = 1d - u;
			}
			u = u % 1d;
			if (v < 0) {
				v = 1d - v;
			}
			v = v % 1d;
		} else {
			// https://en.wikipedia.org/wiki/Clamping_%28graphics%29
			u = Math.min(Math.max(u, 0d), 1.0);
			v = Math.min(Math.max(v, 0d), 1.0);
		}

		// Apparently, OpenGL and Direct3D have different ways of interpreting the v-coordinate.
		// That's why we have to invert it, because whatever is generating our models works in one way,
		// and our texture map works in another.
		v = 1d - v;


		int iu = r(u * (double) (texture.getWidth() - 1));
		int iv = r(v * (double) (texture.getHeight()- 1));

		return new Color(texture.getColor(iu, iv));
	}

	private static int r(double d) {
		return (int) round(d);
	}
}
