/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.reflectionmodel;

import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.LightSource;
import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.renderer.Colors;

import java.awt.*;
import java.util.List;

/**
 * An abstract base class for reflection models that provides methods for creating color.
 *
 * @author markus@jevring.net
 */
public abstract class AbstractReflectionModel implements ReflectionModel {

	protected Color specular(List<LightSource> lightSources,
	                         Material material,
	                         Point3D normal,
	                         Point3D point,
	                         Point3D camera,
	                         Point3D[] lightUnitVectors) {
		float[] cs = new float[4];
		float[] ks = material.getSpecularColor().getComponents(null);
		Point3D viewUnitVector = camera.minus(point).toUnit();

		for (int i = 0; i < lightSources.size(); i++) {
			LightSource lightSource = lightSources.get(i);
			float[] lsColor = lightSource.c.getComponents(null);

			// unit vector FROM the point TO the light source
			// http://engineersphere.com/math/unit-vector-between-two-points.html
			//Point3D lightUnitVector = lightSource.p.minus(point).toUnit();
			Point3D lightUnitVector = lightUnitVectors[i];

			// (L + V) / 2, according to http://www.nbb.cornell.edu/neurobio/land/oldstudentprojects/cs490-95to96/guo/report.html
			Point3D h = lightUnitVector.plus(viewUnitVector).divide(2);
			// "Lambertian reflectance"
			final double dotProductOfNormalToH = normal.dotProduct(h);
			if (dotProductOfNormalToH < 0) {
				// nVidia shows us that this should be > 0 here: http://http.developer.nvidia.com/CgTutorial/cg_tutorial_chapter05.html
				// search for "the diffuse term" and see: max(N . L, 0)

				// the surface faces away from the LIGHT SOURCE, and thus cannot be lit.
				// this is equivalent to back-face culling for the light source.
				continue;
			}
			// apply for each color component
			for (int j = 0; j < 3; j++) {
				// the algorithm doesn't specify the color of the light, but it if it should be included,
				// it makes sense that it should be included here.
				// HOWEVER, this will "taint" the material color, to a certain degree.
				// this is to be expected, however, when using non-white lights.
				cs[j] += ks[j] * Math.pow(dotProductOfNormalToH, material.getSpecularCoefficient()) * (lightSource.intensity * lsColor[j]);
			}
		}
		return Colors.color(cs);
	}

	protected Color diffuse(List<LightSource> lightSources, Material material, Point3D normal, Point3D[] lightUnitVectors) {
		float[] cs = new float[4];
		float[] kd = material.getDiffuseColor().getComponents(null);

		for (int i = 0; i < lightSources.size(); i++) {
			LightSource lightSource = lightSources.get(i);
			float[] lsColor = lightSource.c.getComponents(null);

			// unit vector FROM the point TO the light source
			// http://engineersphere.com/math/unit-vector-between-two-points.html
			//Point3D lightUnitVector = lightSource.p.minus(point).toUnit();
			Point3D lightUnitVector = lightUnitVectors[i];
			// "Lambertian reflectance"
			final double dotProductOfSurfaceNormalToLightUnitVector = normal.dotProduct(lightUnitVector);
			if (dotProductOfSurfaceNormalToLightUnitVector < 0) {
				// nVidia shows us that this should be > 0 here: http://http.developer.nvidia.com/CgTutorial/cg_tutorial_chapter05.html
				// search for "the diffuse term" and see: max(N . L, 0)

				// the surface faces away from the LIGHT SOURCE, and thus cannot be lit.
				// this is equivalent to back-face culling for the light source.
				continue;
			}
			// apply for each color component
			for (int j = 0; j < 3; j++) {
				// the algorithm doesn't specify the color of the light, but it if it should be included,
				// it makes sense that it should be included here.
				// HOWEVER, this will "taint" the material color, to a certain degree.
				// this is to be expected, however, when using non-white lights.
				cs[j] += kd[j] * dotProductOfSurfaceNormalToLightUnitVector * (lightSource.intensity * lsColor[j]);
			}
		}
		return Colors.color(cs);
	}

	@Override
	public String toString() {
		String s = getClass().getSimpleName();
		return s.substring(0, s.lastIndexOf("ReflectionModel"));
	}
}
