/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.renderer.reflectionmodel;

import net.jevring.threedee.Camera;
import net.jevring.threedee.primitives.Material;
import net.jevring.threedee.primitives.LightSource;
import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.renderer.Colors;

import java.awt.*;
import java.util.List;

/**
 * This reflection model adds ambient light, diffuse light from all light sources, and specular light from all light sources.
 *
 * @author markus@jevring.net
 */
public class HighlightOnReflectionModel extends AbstractReflectionModel {
	@Override
	public Color getColor(Material material, Point3D point, Point3D normal, List<LightSource> lightSources, LightSource ambientLight, Camera camera) {
		Point3D[] lightUnitVectors = new Point3D[lightSources.size()];
		for (int i = 0; i < lightSources.size(); i++) {
			LightSource lightSource = lightSources.get(i);
			Point3D lightUnitVector = lightSource.p.minus(point).toUnit();
			lightUnitVectors[i] = lightUnitVector;
		}

		// ambient
		Color ambient = Colors.multiply(material.getAmbientColor(), ambientLight.intensity);

		// diffuse
		Color diffuse = diffuse(lightSources, material, normal, lightUnitVectors);

		// specular
		Color specular = specular(lightSources, material, normal, point, camera, lightUnitVectors);

		Color c = Colors.add(ambient, diffuse, specular);

		return c;
	}
}
