/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.fileformats.ObjectLoader;
import net.jevring.threedee.fileformats.ObjectParserException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class Main {
	public static void main(String[] args) {
		final JFrame frame = new JFrame("3D!");
		frame.setLocation(700, 50); // if we load settings, this will be overwritten

		// the canvas is what we'll be drawing out world on.
		final GraphicsDevice defaultScreenDevice = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		final Canvas canvas = new Canvas(defaultScreenDevice.getDefaultConfiguration());
		canvas.setPreferredSize(new Dimension(640, 715));

		final World world = new World();
		final View view = new View();
		// ensure that the view is always in-line with the canvas we're drawing on
		canvas.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				view.setSize(canvas.getSize());
				frame.setTitle("3D! (" + canvas.getSize().getWidth() + "x" + canvas.getSize().getHeight() + ")");
			}
		});
		final InputController inputController = new InputController(canvas, view, world);
		final Engine engine = new Engine(view, world, canvas, inputController);

		ObjectLoader ol = new ObjectLoader();
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if ("-settings".equals(arg)) {
				Settings settings = new Settings(new File(args[++i]));
				settings.load(view, frame, canvas);
			} else if ("-load".equals(arg)) {
				File f = new File(args[++i]);
				if (f.isFile()) {
					try {
						world.add(ol.load(f), 100d);
					} catch (IOException | ObjectParserException e) {
						e.printStackTrace();
						System.exit(-1);
					}
				} else {
					ol.addPotentialDirectory(f);
				}
			}
		}

		// it's after the loading of the objects, so that there will be something in the list.
		// obviously, later, if we want a "real" interface, we'll have to do something more clever
		ControlPanel controlPanel = new ControlPanel(world, view, inputController, ol, engine, frame);

		// create the window for the application
		
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		//frame.setPreferredSize(new Dimension(1000, 700));
		frame.setLayout(new BorderLayout(2, 2));
		frame.add(controlPanel, BorderLayout.EAST);
		frame.add(canvas, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
		inputController.start();

		// run the engine on the main thread
		engine.run();
	}
}
