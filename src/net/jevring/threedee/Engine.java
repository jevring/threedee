/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.frustum.CullingResult;
import net.jevring.threedee.frustum.Frustum;
import net.jevring.threedee.primitives.*;
import net.jevring.threedee.primitives.camera.CameraFace;
import net.jevring.threedee.primitives.camera.CameraObject3D;
import net.jevring.threedee.primitives.camera.CameraVertex;
import net.jevring.threedee.renderer.painter.*;
import net.jevring.threedee.renderer.reflectionmodel.ColorOnAmbientOffReflectionModel;
import net.jevring.threedee.renderer.reflectionmodel.ColorOnAmbientOnReflectionModel;
import net.jevring.threedee.renderer.reflectionmodel.HighlightOnReflectionModel;
import net.jevring.threedee.renderer.reflectionmodel.ReflectionModel;
import net.jevring.threedee.renderer.shader.Shader;
import net.jevring.threedee.renderer.shader.flat.FlatShader;
import net.jevring.threedee.renderer.shader.gouraud.BlendingMode;
import net.jevring.threedee.renderer.shader.gouraud.GouraudShader;
import net.jevring.threedee.renderer.shader.gouraud.TextureOnlyGouraudShader;
import net.jevring.threedee.renderer.shader.gouraud.TexturedColoredGouraudShader;
import net.jevring.threedee.renderer.shader.phong.PhongShader;
import net.jevring.threedee.renderer.shader.phong.TexturePhongShader;
import net.jevring.threedee.renderer.shader.wireframe.WireframeShader;
import net.jevring.threedee.renderer.texturefilter.BilinearFiltering;
import net.jevring.threedee.renderer.texturefilter.NearestNeighbor;
import net.jevring.threedee.renderer.texturefilter.TextureFilter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The engine is the glue that binds all the components together.
 *
 * @author markus@jevring.net
 */
public class Engine implements Runnable {
	// todo: these should likely come from settings
	private static final Optional<Color> outlineHighlightColor = Optional.of(Color.RED.brighter());
	private static final Optional<Color> bodyHighlightColor = Optional.of(Color.GREEN.brighter());
	/**
	 * Orders objects along the z-axis with the items closest to the camera first.
	 * That is, the values with the <b>highest</b> Z values (as the z-axis is positive
	 * going <b>out</b> to the screen) come first.
	 * 
	 * todo: have a way of switching the order of this so we can see the result in the debug painter
	 */
	private static final Comparator<? super CameraObject3D> BY_Z = new Comparator<CameraObject3D>() {
		@Override
		public int compare(CameraObject3D o1, CameraObject3D o2) {
			return Double.compare(o2.getZMax(), o1.getZMax());
		}
	};
	private final Map<IlluminationModel, ReflectionModel> reflectionModels = new HashMap<>();
	private final View view;
	private final World world;
    private final Canvas canvas;
    private final InputController inputController;
    private RenderingMode renderingMode = RenderingMode.ColoredTexturedGouraudShading;
    private boolean limitSpeed = false;
    private boolean singleThreaded = false;
    private TextureFilter textureFilter = new NearestNeighbor();
    private volatile boolean running;
	private BlendingMode blendingMode = BlendingMode.Interpolative;

	public Engine(View view, World world, Canvas canvas, InputController inputController) {
        this.view = view;
        this.world = world;
        this.canvas = canvas;
        this.inputController = inputController;

        reflectionModels.put(IlluminationModel.ColorOnAmbientOff, new ColorOnAmbientOffReflectionModel());
        reflectionModels.put(IlluminationModel.ColorOnAmbientOn, new ColorOnAmbientOnReflectionModel());
        reflectionModels.put(IlluminationModel.HighlightOn, new HighlightOnReflectionModel());
    }

    @Override
    public void run() {
        // http://docs.oracle.com/javase/tutorial/extra/fullscreen/bufferstrategy.html

        running = true;

        // get the buffer strategy, start the rendering loop.
        canvas.setIgnoreRepaint(true);
        canvas.createBufferStrategy(3);

        final BufferStrategy bufferStrategy = canvas.getBufferStrategy();
        double fpsOfLastFrame = 0;
        while (running && !Thread.interrupted()) {
            long start = System.nanoTime();
            Graphics2D g = null;
            try {
	            g = (Graphics2D) bufferStrategy.getDrawGraphics();
	            // clear the graphics, otherwise we end up with the existing data in there.
	            // todo: this likely isn't necessary now that we're filling the screen with the sky box
	            g.clearRect(0, 0, view.getWidth(), view.getHeight());
	            synchronized (world) {
		            synchronized (view) {
			            render(g, fpsOfLastFrame);
		            }
	            }
            } catch (Throwable t) {
	            t.printStackTrace();
            } finally {
                if (g != null) {
                    // it should never be null, but still
                    g.dispose();
                }
            }
            bufferStrategy.show();


            // sleep a bit and aim for a target fps.
            if (limitSpeed) {
                long speedLimitTimestamp = System.nanoTime();
	            double targetFps = 60d;
                double maxPauseInSeconds = 1d / targetFps;
                double maxPauseInNanoseconds = maxPauseInSeconds * Math.pow(10, 9);
                double timeTakenSoFar = speedLimitTimestamp - start;
                double nanosecondsLeftToPause = maxPauseInNanoseconds - timeTakenSoFar;
                if (nanosecondsLeftToPause > 0) {
                    try {
                        TimeUnit.NANOSECONDS.sleep((long) nanosecondsLeftToPause);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //Toolkit.getDefaultToolkit().sync();
            }
            long end = System.nanoTime();
            double timeTaken = end - start;
            double dts = timeTaken / Math.pow(10, 9);
            fpsOfLastFrame = 1.0 / dts;
        }
    }

    private void render(Graphics2D g, double fpsOfLastFrame) {
        int height = view.getHeight();
        int width = view.getWidth();
	    ZBufferedPainter painter;
	    switch (view.getDebugPainter()) {
		    case MissRate:
			    painter = new MissRateZBufferedPainter(width, height);
			    break;
		    case OverdrawRate:
			    painter = new OverdrawRateZBufferedPainter(width, height);
			    break;
		    case None:
		    default:
			    painter = new DefaultZBufferedPainter(width, height);
			    break;

	    }
	    int totalNumberOfFaces = world.getTotalNumberOfFaces();
	    int drawnFaces = 0;
	    AtomicInteger objectsCompletelyOutsideFrustum = new AtomicInteger();
	    AtomicInteger objectsPartiallyOutsideFrustum = new AtomicInteger();
	    AtomicInteger facesLostToBackFaceCulling = new AtomicInteger();
	    Frustum frustum = view.getFrustum();

	    List<Object3D> survivingObjectsAfterFrustumCulling =
			    cullAgainstFrustum(world.getObjects(), frustum, objectsCompletelyOutsideFrustum, objectsPartiallyOutsideFrustum);
	    List<CameraObject3D> objectsInCameraCoordinates = cameraTransform(survivingObjectsAfterFrustumCulling, facesLostToBackFaceCulling);
	    Collections.sort(objectsInCameraCoordinates, BY_Z);
	    for (CameraObject3D object : objectsInCameraCoordinates) {
		    drawnFaces += draw(object, g, painter);
	    }
        drawSkyBox(painter, frustum);
	    Optional<CameraObject3D> cameraObjectAtMouse = painter.getObjectAt(r(inputController.getMouseLocation().getX()),
	                                                                       r(inputController.getMouseLocation().getY()));
	    if (cameraObjectAtMouse.isPresent()) {
		    painter.highlight(cameraObjectAtMouse.get(), outlineHighlightColor, bodyHighlightColor);
	    }
        drawRenderedImage(g, height, width, painter);

	    // render some OSD statistics
	    List<String> lines = new ArrayList<>();
	    String sfps;
	    if (fpsOfLastFrame < 1) {
		    sfps = String.valueOf(fpsOfLastFrame);
	    } else {
		    sfps = String.valueOf((int) fpsOfLastFrame);
	    }
	    lines.add(String.format("%s fps", sfps));
	    lines.add(String.format("%d/%d face(s) drawn", drawnFaces, totalNumberOfFaces));
	    lines.add(String.format("%d/%d face(s) lost to back-face culling", facesLostToBackFaceCulling.get(), totalNumberOfFaces));
	    lines.add(String.format("%d/%d object(s) completely outside frustum", objectsCompletelyOutsideFrustum.get(), world.getObjects().size()));
	    lines.add(String.format("%d/%d object(s) partially outside frustum", objectsPartiallyOutsideFrustum.get(), world.getObjects().size()));
	    lines.add(view.getCamera().toString());

	    double mouseZ = painter.getZValueAt(r(inputController.getMouseLocation().getX()),
	                                        r(inputController.getMouseLocation().getY()));
	    lines.add(String.format("Mouse Z: %f, 1/z = %f", mouseZ, 1 / mouseZ));
	    lines.add(String.format("Z-order: %s", objectsInCameraCoordinates.subList(0, Math.min(3, objectsInCameraCoordinates.size()))));
	    
	    if (cameraObjectAtMouse.isPresent()) {
		    lines.add("Camera object: " + cameraObjectAtMouse.get());
	    }
	    if (painter instanceof DebugZBufferedPainter) {
		    lines.addAll(((DebugZBufferedPainter) painter).getDebugMessages());
	    }

	    g.setColor(Color.getHSBColor((float) view.getOnScreenDisplayHue(), 1, 1));
	    for (int i = 0; i < lines.size(); i++) {
		    String line = lines.get(i);
		    g.drawString(line, 20, view.getHeight() - (20 + (i * 20)));
	    }
    }

	private List<CameraObject3D> cameraTransform(List<Object3D> objects, AtomicInteger facesLostToBackFaceCulling) {
		List<CameraObject3D> transformedObjects = new ArrayList<>();
		for (Object3D object : objects) {
			CameraObject3D cameraObject = new CameraObject3D(object);
			for (Face face : object.getFaces()) {
				List<Point3D> cameraSpacePoints = new ArrayList<>();
				List<Vertex> worldSpaceVertices = face.getVertices();
				for (Vertex vertex : worldSpaceVertices) {
					cameraSpacePoints.add(view.toCameraCoordinates(vertex.p));
				}


				// Perform back-face culling: Don't draw faces facing away from us.
				// https://en.wikipedia.org/wiki/Back-face_culling
				// See the last line of that link.
				// This is determined based on the "winding" of the face
				Point3D p1 = cameraSpacePoints.get(0);
				Point3D p2 = cameraSpacePoints.get(1);
				Point3D p3 = cameraSpacePoints.get(2);
				final Point3D surfaceNormalInCameraSpace = p2.minus(p1).crossProduct(p3.minus(p1));
				if (surfaceNormalInCameraSpace.dotProduct(p1) >= 0) {
					facesLostToBackFaceCulling.incrementAndGet();
				} else {
					List<CameraVertex> vertices = new ArrayList<>();
					for (int i = 0; i < worldSpaceVertices.size(); i++) {
						Vertex vertex = worldSpaceVertices.get(i);
						Point3D pointInCameraCoordinates = cameraSpacePoints.get(i);

						Point pointInViewport = view.project(pointInCameraCoordinates);
						pointInViewport.translate(r((double) view.getWidth() / 2d), r((double) view.getHeight() / 2d));
						vertices.add(new CameraVertex(vertex, pointInViewport.x, pointInViewport.y, pointInCameraCoordinates.z));
					}
					sortByY(vertices);
					cameraObject.add(new CameraFace(vertices));
				} // else: the face faces backwards, cull it
			}
			if (cameraObject.hasFaces()) {
				transformedObjects.add(cameraObject);
			}
		}
		return transformedObjects;
	}

	/**
	 * Sorts the vertices based on the y-coordinate in-place.
	 *
	 * @param vertices the vertices to sort
	 */
	protected void sortByY(List<CameraVertex> vertices) {
		Collections.sort(vertices, new Comparator<CameraVertex>() {
			@Override
			public int compare(CameraVertex o1, CameraVertex o2) {
				//noinspection SuspiciousNameCombination
				return Double.compare(o1.getScreenY(), o2.getScreenY());
			}
		});
	}

	private int r(double d) {
		return (int) Math.round(d);
	}

	private List<Object3D> cullAgainstFrustum(List<Object3D> objects,
	                                          Frustum frustum,
	                                          AtomicInteger objectsCompletelyOutsideFrustum,
	                                          AtomicInteger objectsPartiallyOutsideFrustum) {
		List<Object3D> result = new ArrayList<>();
		for (Object3D object : objects) {
			CullingResult cullingResult = frustum.cull(object);
			if (cullingResult.isCompletelyOutside()) {
				objectsCompletelyOutsideFrustum.incrementAndGet();
			} else {
				if (!cullingResult.isCompletelyInside()) {
					// not completely outside or inside means partially inside
					objectsPartiallyOutsideFrustum.incrementAndGet();
				}
				final AtomicInteger facesBehindCamera = new AtomicInteger();
				final AtomicInteger facesPartiallyBehindCamera = new AtomicInteger();
				final AtomicInteger facesDeleted = new AtomicInteger();
				final AtomicInteger originalFacesReturned = new AtomicInteger();
				final AtomicInteger newFacesReturned = new AtomicInteger();

				List<Plane> cullingPlanes = cullingResult.getCullingPlanes();
				Object3D drawableObject = cullAgainstPlanes(object,
				                                            cullingPlanes,
				                                            facesBehindCamera,
				                                            facesPartiallyBehindCamera,
				                                            facesDeleted,
				                                            originalFacesReturned,
				                                            newFacesReturned);

				result.add(drawableObject);
			}
		}
		return result;
	}


	private void drawRenderedImage(Graphics g, int height, int width, ZBufferedPainter painter) {
		// Lets separate this into a separate function to see if it shows up significantly in the sampler.
		// Indeed, extracting this to a separate method made render() drop down to almost nothing


        // with the other bottlenecks removed, there was significant difference between using MemoryImageSource
        // and using a BufferedImage and setRGB()
        MemoryImageSource mis = new MemoryImageSource(width, height, painter.getPixels(), 0, width);
        Image img = canvas.createImage(mis);

        g.drawImage(img, 0, 0, null);

	    if (inputController.shouldTakeScreenshot()) {
		    try {
			    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			    bi.getGraphics().drawImage(img, 0, 0, null);
			    SimpleDateFormat format = new SimpleDateFormat("YYYY.MM.DD-HH.mm.ss.SSS");
			    File output = new File(String.format("Screenshot-%s.png", format.format(new Date())));
			    System.out.println("Writing screenshot to: " + output.getAbsolutePath());
			    ImageIO.write(bi, "png", output);
		    } catch (IOException e) {
			    e.printStackTrace();
		    }
	    }
    }

    private void drawSkyBox(ZBufferedPainter painter, Frustum frustum) {
        // we don't do it in the main loop, because it gets special treatment
        Object3D skyBox = view.getSkyBox().getBox();
        Shader shader = new TextureOnlyGouraudShader(world, view, painter, singleThreaded, textureFilter);

	    Object3D culledSkyBox = cullAgainstPlanes(skyBox,
	                                              frustum.getSkyboxCullingPlanes(),
	                                              new AtomicInteger(),
	                                              new AtomicInteger(),
	                                              new AtomicInteger(),
	                                              new AtomicInteger(),
	                                              new AtomicInteger());
	    CameraObject3D transformedSkyBox = cameraTransform(Collections.singletonList(culledSkyBox), new AtomicInteger()).get(0);
	    for (CameraFace face : transformedSkyBox.getFaces()) {
		    // we pass in null as the object here, as we don't want the skybox to be highlightable
		    shader.draw(face, new ColorOnAmbientOffReflectionModel(), skyBox.getMaterial(), null);
	    }
    }

	private int draw(final CameraObject3D object, Graphics g, ZBufferedPainter painter) {
		if (object.getWorldSpaceObject().getMaterial() == null) {
			// this is a completely arbitrary material
			// the MTL docs state that, "if a material is missing, a white material is used".
            // no idea about any coefficients, but I guess we're just going with this
			object.getWorldSpaceObject().setMaterial(new MaterialBuilder().setName("default").setAmbientColor(Color.BLACK).setDiffuseColor(
					Color.WHITE).setSpecularColor(Color.WHITE).setSpecularCoefficient(0.35f).setTransparency(0).setIlluminationModel(
					IlluminationModel.HighlightOn).createMaterial());
		}


        final Shader shader;
        switch (renderingMode) {
            case Wireframe:
                shader = new WireframeShader(world, view, painter, singleThreaded, g);
                break;
            case FlatShading:
                shader = new FlatShader(world, view, painter, singleThreaded);
                break;
            case TexturePhongShading:
	            if (object.getWorldSpaceObject().hasTextureCoordinates()) {
		            shader = new TexturePhongShader(world, view, painter, singleThreaded, textureFilter, blendingMode);
		            break;
                }
            case PhongShading:
                shader = new PhongShader(world, view, painter, singleThreaded);
                break;
            case ColoredTexturedGouraudShading:
	            if (object.getWorldSpaceObject().hasTextureCoordinates()) {
		            shader = new TexturedColoredGouraudShader(world, view, painter, singleThreaded, textureFilter, blendingMode);
		            break;
                }
            case TextureOnlyGouraudShading:
	            if (object.getWorldSpaceObject().hasTextureCoordinates()) {
		            shader = new TextureOnlyGouraudShader(world, view, painter, singleThreaded, textureFilter);
		            break;
                }
            case GouraudShading:
                shader = new GouraudShader(world, view, painter, singleThreaded);
                break;
            default:
                throw new IllegalArgumentException("Unknown rendering mode: " + renderingMode);
        }
		IlluminationModel illuminationModel = object.getWorldSpaceObject().getMaterial().getIlluminationModel();
		if (illuminationModel == null) {
			illuminationModel = IlluminationModel.HighlightOn;
        }
        final ReflectionModel reflectionModel = reflectionModels.get(illuminationModel);

		for (CameraFace face : object.getFaces()) {
			shader.draw(face, reflectionModel, object.getWorldSpaceObject().getMaterial(), object);
		}

		// todo: this isn't strictly correct. because we do splitting of the faces, we might actually end up with MORE faces than are loaded into the world
		return object.getFaces().size();
	}

	private Object3D cullAgainstPlanes(Object3D object,
	                                   List<Plane> cullingPlanes,
	                                   AtomicInteger facesBehindCamera,
	                                   AtomicInteger facesPartiallyBehindCamera,
	                                   AtomicInteger facesDeleted,
	                                   AtomicInteger originalFacesReturned,
	                                   AtomicInteger newFacesReturned) {
		// todo: for *most* objects, z-ordering the faces is a waste of time, as they don't form complex curves.
		List<Face> drawableFaces = new ArrayList<>();
		for (Face face : object.getFaces()) {
			if (!cullingPlanes.isEmpty()) {
				Set<Face> facesInsideFrustum =
						cullAgainstPlanes(face, cullingPlanes, facesBehindCamera, facesPartiallyBehindCamera);
				if (facesInsideFrustum.isEmpty()) {
					facesDeleted.incrementAndGet();
				} else if (facesInsideFrustum.size() == 1 && facesInsideFrustum.contains(face)) {
					originalFacesReturned.incrementAndGet();
				} else {
					newFacesReturned.addAndGet(facesInsideFrustum.size());
				}

				for (Face f : facesInsideFrustum) {
					drawableFaces.add(f);
				}
			} else {
				drawableFaces.add(face);
			}
		}
		return object.cloneWithCulledFaces(drawableFaces);
	}

    private Set<Face> cullAgainstPlanes(Face face,
                                        List<Plane> cullingPlanes,
                                        AtomicInteger facesBehindCamera,
                                        AtomicInteger facesPartiallyBehindCamera) {
        Set<Face> facesInsideFrustum = new HashSet<>();
        facesInsideFrustum.add(face); // start with the original face
        for (Plane cullingPlane : cullingPlanes) {
            // loop over all the planes, and each time we cull something, keep track of the
            // remaining faces. Each face needs to be culled against all the planes
            Set<Face> newFaces = new HashSet<>();
            for (Face faceInsideFrustum : facesInsideFrustum) {
                // todo: better statistics collection
                Collection<Face> fs = cullAgainstPlane(faceInsideFrustum,
                        cullingPlane,
                        facesBehindCamera,
                        facesPartiallyBehindCamera);
                newFaces.addAll(fs);
            }
            // we now have a new set of faces we need to cull against the next plane
            facesInsideFrustum = newFaces;
        }
        return facesInsideFrustum;
    }

    /**
     * Takes a face with one or two points behind the camera and returns the face(s) that result
     * from intersecting the face with the back plane and retaining only those face(s) that are in front.
     * todo: improve javadoc
     */
    private Collection<Face> cullAgainstPlane(Face face,
                                              Plane plane,
                                              AtomicInteger completelyOutside,
                                              AtomicInteger partiallyOutside) {
        // todo: we only have to do culling etc if the scene changes or the camera moves!
        // we should trigger this culling based on these kinds of queues in the engine.
        /*
		For instance, when we're moving an object, check if it ended up outside the box, then perform culling. 
		Similarly, when we move the camera, check if any of the objects that were previously in the box have now 
		gone outside or vice versa.
		 */
        List<Face> faces = new ArrayList<>();

        List<Vertex> vertices = face.getVertices();
        Vertex v0 = vertices.get(0);
        Vertex v1 = vertices.get(1);
        Vertex v2 = vertices.get(2);

        Set<Vertex> inFront = new HashSet<>();
        Set<Vertex> behind = new HashSet<>();
        for (Vertex vertex : vertices) {
            if (!plane.isInFrontOf(vertex.p)) {
                inFront.add(vertex);
            } else {
                behind.add(vertex);
            }
        }

        // todo: how should we handle when we don't get any intersections. Just do nothing?
        // perhaps dropping a face is better than trying to hack something together

        // assuming a triangle with points A, B, C, with lines A->B, B->C, and C->A.
        // with this assumption, the variable names will make more sense.
        if (behind.size() == 1) {
            // we'll get TWO faces

            Vertex a;
            Vertex b; // the vertex behind the plane
            Vertex c;
            // we have to do this to maintain the winding of the faces, as otherwise they might
            // be culled as back faces, because their normals point the wrong way
            if (behind.contains(v0)) {
                a = v2;
                b = v0;
                c = v1;
            } else if (behind.contains(v1)) {
                a = v0;
                b = v1;
                c = v2;
            } else {
                a = v1;
                b = v2;
                c = v0;
            }

            Vertex.Intersection abIntersection = Vertex.intersection(a, b, plane);
            Vertex.Intersection bcIntersection = Vertex.intersection(b, c, plane);
            if (abIntersection.intersects()) {
                faces.add(new Face(Arrays.asList(a, abIntersection.getVertex(), c)));
                if (bcIntersection.intersects()) {
                    faces.add(new Face(Arrays.asList(abIntersection.getVertex(), bcIntersection.getVertex(), c)));
                } else {
                    System.err.println("Expected intersection but got: " + bcIntersection);
                }
            } else {
                System.err.println("Expected intersection but got: " + abIntersection);
            }
            partiallyOutside.incrementAndGet();
        } else if (behind.size() == 2) {
            // we'll get ONE face
            Vertex a; // in front
            Vertex b; // behind
            Vertex c; // behind
            // we have to do this to maintain the winding of the faces, as otherwise they might
            // be culled as back faces, because their normals point the wrong way
            if (inFront.contains(v0)) {
                a = v0;
                b = v1;
                c = v2;
            } else if (inFront.contains(v1)) {
                a = v1;
                b = v2;
                c = v0;
            } else {
                a = v2;
                b = v0;
                c = v1;
            }
            Vertex.Intersection abIntersection = Vertex.intersection(a, b, plane);
            Vertex.Intersection acIntersection = Vertex.intersection(a, c, plane);
            if (abIntersection.intersects() && acIntersection.intersects()) {
                faces.add(new Face(Arrays.asList(a, abIntersection.getVertex(), acIntersection.getVertex())));
            } else {
                System.err.println("Expected intersection but got: " + abIntersection + ", " + acIntersection);
            }
            partiallyOutside.incrementAndGet();
        } else if (behind.size() == 3) {
            // the entire thing is behind, don't render anything
            completelyOutside.incrementAndGet();
        } else if (behind.isEmpty()) {
            // the entire thing is in front. we don't need to do anything
            faces.add(face);
        }
        return faces;
    }

    public void setRenderingMode(RenderingMode renderingMode) {
        this.renderingMode = renderingMode;
    }

    public RenderingMode getRenderingMode() {
        return renderingMode;
    }

    public boolean isLimitSpeed() {
        return limitSpeed;
    }

    public void setLimitSpeed(boolean limitSpeed) {
        this.limitSpeed = limitSpeed;
    }

    public boolean isSingleThreaded() {
        return singleThreaded;
    }

    public void setSingleThreaded(boolean singleThreaded) {
        this.singleThreaded = singleThreaded;
    }

    public TextureFilter.Type getTextureFilter() {
        if (textureFilter instanceof NearestNeighbor) {
            return TextureFilter.Type.NearestNeighbor;
        } else {
            return TextureFilter.Type.BilinearFilter;
        }
    }

    public void setTextureFilter(TextureFilter.Type textureFilter) {
        if (textureFilter == TextureFilter.Type.NearestNeighbor) {
            this.textureFilter = new NearestNeighbor();
        } else {
            this.textureFilter = new BilinearFiltering();
        }
    }

	public void setBlendingMode(BlendingMode blendingMode) {
		this.blendingMode = blendingMode;
	}

	public BlendingMode getBlendingMode() {
		return blendingMode;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void stop() {
		System.out.println("Shutting down");
		this.running = false;
	}
}
