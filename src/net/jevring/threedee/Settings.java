/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Reads and writes settings information.
 *
 * @author markus@jevring.net
 */
public class Settings {
	public static final String CAMERA_X = "camera.x";
	public static final String CAMERA_Y = "camera.y";
	public static final String CAMERA_Z = "camera.z";
	public static final String CAMERA_RX = "camera.rx";
	public static final String CAMERA_RY = "camera.ry";
	public static final String CAMERA_RZ = "camera.rz";
	public static final String VIEW_WIDTH = "view.width";
	public static final String VIEW_HEIGHT = "view.height";
	public static final String LOCATION_SCREEN_X = "location.screen.x";
	public static final String LOCATION_SCREEN_Y = "location.screen.y";
	
	private final File settings;

	public Settings(File settings) {
		this.settings = settings;
	}

	public void store(View view, JFrame frame) {
		Properties p = new Properties();
		Camera c = view.getCamera();
		p.setProperty(CAMERA_X, String.valueOf(c.x));
		p.setProperty(CAMERA_Y, String.valueOf(c.y));
		p.setProperty(CAMERA_Z, String.valueOf(c.z));
		p.setProperty(CAMERA_RX, String.valueOf(c.rx));
		p.setProperty(CAMERA_RY, String.valueOf(c.ry));
		p.setProperty(CAMERA_RZ, String.valueOf(c.rz));
		
		p.setProperty(VIEW_WIDTH, String.valueOf(view.getWidth()));
		p.setProperty(VIEW_HEIGHT, String.valueOf(view.getHeight()));

		p.setProperty(LOCATION_SCREEN_X, String.valueOf(frame.getLocationOnScreen().x));
		p.setProperty(LOCATION_SCREEN_Y, String.valueOf(frame.getLocationOnScreen().y));
		
		try (FileOutputStream fos = new FileOutputStream(settings)) {
			p.store(fos, "settings");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void load(View view, JFrame frame, Canvas canvas) {
		try (FileInputStream fis = new FileInputStream(settings)) {
			Properties p = new Properties();
			p.load(fis);
			view.setCamera(new Camera(get(p, CAMERA_X),
			                          get(p, CAMERA_Y),
			                          get(p, CAMERA_Z),
			                          get(p, CAMERA_RX),
			                          get(p, CAMERA_RY),
			                          get(p, CAMERA_RZ)));
			Dimension size = new Dimension((int) get(p, VIEW_WIDTH), (int) get(p, VIEW_HEIGHT));
			view.setSize(size);
			canvas.setSize(size);
			frame.setLocation((int) get(p, LOCATION_SCREEN_X), (int) get(p, LOCATION_SCREEN_Y));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private double get(Properties p, String key) {
		return Double.parseDouble(p.getProperty(key, "0"));
	}
}
