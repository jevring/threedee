/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.primitives.Point3D;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;

/**
 * @author markus@jevring.net
 */
public class Size {
	public final double xMax;
	public final double xMin;

	public final double yMax;
	public final double yMin;

	public final double zMax;
	public final double zMin;

	public final double maxMax;

	public Size(double xMax, double xMin, double yMax, double yMin, double zMax, double zMin) {
		this.xMax = xMax;
		this.xMin = xMin;
		this.yMax = yMax;
		this.yMin = yMin;
		this.zMax = zMax;
		this.zMin = zMin;
		maxMax = max(xMax, max(yMax, zMax));
	}

    /**
     * Gets a set of 8 points that bound the object.
     *
     * @return a bounding box corresponding to this size
     */
    public List<Point3D> toBoundingBox() {
        List<Point3D> boundingBox = new ArrayList<>();
        // the max values make up one corner, and the min values make up the diagonally opposite corner

        // "front" quad
        boundingBox.add(new Point3D(xMin, yMin, zMin));
        boundingBox.add(new Point3D(xMax, yMin, zMin));
        boundingBox.add(new Point3D(xMin, yMax, zMin));
        boundingBox.add(new Point3D(xMax, yMax, zMin));

        // "rear" quad
        boundingBox.add(new Point3D(xMin, yMin, zMax));
        boundingBox.add(new Point3D(xMax, yMin, zMax));
        boundingBox.add(new Point3D(xMin, yMax, zMax));
        boundingBox.add(new Point3D(xMax, yMax, zMax));
        return boundingBox;
    }

	@Override
	public String toString() {
		return "Size{" +
				"xMax=" + xMax +
				", xMin=" + xMin +
				", yMax=" + yMax +
				", yMin=" + yMin +
				", zMax=" + zMax +
				", zMin=" + zMin +
				'}';
	}
}
