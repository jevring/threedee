/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.transformations.Rotator;

/**
 * @author markus@jevring.net
 */
public class Camera extends Point3D {
	private static final Point3D FORWARD = new Point3D(0, 0, -1);
	private static final Point3D UP = new Point3D(0, 1, 0);
	private static final Point3D RIGHT = new Point3D(1, 0, 0);
	public final double rx;
	public final double ry;
	public final double rz;
	public final Point3D normal;
	/**
	 * The "up" vector is a vector that represents the direction of "up" w.r.t the camera. If the
	 * camera is completely level, up is (0, 1, 0). Upside down, thus, is (0, -1, 0). This vector
	 * depends on the camera rotation. Since we have that, we will reverse-engineer the up vector
	 * from the camera rotation angles.
	 *
	 * <p><a href="http://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard-example-moving-around-the-world/">
	 *     http://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard-example-moving-around-the-world/</a>
	 *
	 * <p>This vector is normalized.
	 */
	public final Point3D up;
	public final Point3D right;

	public Camera(double x, double y, double z, double rx, double ry, double rz) {
		super(x, y, z);
		// x = pitch (up and down)
		// y = yaw (side to side)
		// z = roll (around the central axis)
		this.rx = (rx + 360) % 360;
		this.ry = (ry + 360) % 360;
		this.rz = (rz + 360) % 360;


		// the normal points IN to the screen.
		
		// This is the base direction of the camera (into the screen), which we then rotate
		// based on the angles of the camera, to get the normal, up and left vectors.
		// We use -1 here, as we use a left hand coordinate system, in which case
		// positive depth coordinates point OUT of the screen
		Rotator rotator = new Rotator(rx, ry, rz);
		this.normal = rotator.rotate(FORWARD).toUnit();
		this.up = rotator.rotate(UP).toUnit();
		this.right = rotator.rotate(RIGHT).toUnit();
	}

	public Camera rotate(double rx, double ry, double rz) {
		return new Camera(x, y, z, this.rx + rx, this.ry + ry, this.rz + rz);
	}

	public Camera setRotation(double rx, double ry, double rz) {
		return new Camera(x, y, z, rx, ry, rz);
	}

	@Override
	public Camera move(double x, double y, double z) {
		return new Camera(this.x + x, this.y + y, this.z + z, rx, ry, rz);
	}

	public Camera moveTo(double x, double y, double z) {
		return new Camera(x, y, z, rx, ry, rz);
	}

	@Override
	public String toString() {
		return "Camera{" + super.toString() +
				", rx=" + rx +
				", ry=" + ry +
				", rz=" + rz +
				'}';
	}
}
