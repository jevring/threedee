/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.primitives.WorldObject;
import net.jevring.threedee.transformations.Mover;
import net.jevring.threedee.transformations.Rotator;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.awt.event.*;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Handles input events from mouse and keyboard
 * @author markus@jevring.net
 */
public class InputController extends MouseAdapter {
	private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
		@Override
		public Thread newThread(@NotNull Runnable r) {
			Thread t = new Thread(r, "InputController");
			t.setDaemon(true); // this thread shouldn't prevent us from shutting down.
			return t;
		}
	});
	/**
	 * Used to register the input hooks on. Perhaps not ideal, but it'll work until we know better.
	 */
	private final Canvas canvas;
	private final View view;
	private final World world;

	// transient modification variables.
	private Point dragBase;
	private Camera cameraAtDragStart;
	private Mover moverAtDragStart;
	private Rotator rotatorAtDragStart;
	private boolean movementAffectsCamera = true;
	private boolean movementAffectsObject = false;
	private boolean rotate = false;
	private WorldObject selectedObject = null;
	private final Set<Integer> keysPressed = Collections.newSetFromMap(new ConcurrentHashMap<Integer, Boolean>());
	private final AtomicBoolean shouldTakeScreenshot = new AtomicBoolean(false);
	private Point mouseLocation = new Point(0, 0);

    // todo: add a context menu that lets us chose which object to manipulate.
	// ideally it should show us all things under the pointer, but that might be hard...

	public InputController(final Canvas canvas, final View view, final World world) {
		this.canvas = canvas;
		this.view = view;
		this.world = world;

		canvas.addMouseListener(this);
		canvas.addMouseWheelListener(this);
		canvas.addMouseMotionListener(this);
		canvas.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				// for unknown reasons, PRINTSCREEN only registers when it is RELEASED.
				// thus, we need the special case here
				if (e.getKeyCode() == KeyEvent.VK_PRINTSCREEN) {
					// this is unset when the screenshot is taken
					shouldTakeScreenshot.set(true);
				} else {
					keysPressed.remove(e.getKeyCode());
				}
				e.consume();
			}

			@Override
			public void keyPressed(KeyEvent e) {
				keysPressed.add(e.getKeyCode());
				e.consume();
			}
		});
	}

	/**
	 * Starts a thread that mutates the world and view every N time units (defaults to 50Hz).
	 */
	public void start() {
		executor.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					move();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, 0, 20, TimeUnit.MILLISECONDS);
	}

	/**
	 * Checks which keys are pressed and creates a combined movement for all those keys.
	 * Certain keys cancel each other out, like forward and backward movement, and left
	 * and right.
	 */
	private void move() {
		// We'd like to synchronize on 'world' and 'view' here as well, 
		// but since it's a) very likely that the fps drops below 50 and
		// b) all operations are atomic anyway, we simply can't, and we 
		// likely don't need to.
		// todo: maybe we could multiply the key presses with how long it's been since they were last used, and how long they were pressed. maybe too much work...
		int speed = 5;
		int forwardMovement = 0;
		int sidewaysMovement = 0;
		int cameraRotation = 0;
		int elevation = 0;

		if (keysPressed.contains(KeyEvent.VK_W)) {
			forwardMovement -= speed;
		}
		if (keysPressed.contains(KeyEvent.VK_S)) {
			forwardMovement += speed;
		}
		if (keysPressed.contains(KeyEvent.VK_D)) {
			sidewaysMovement -= speed;
		}
		if (keysPressed.contains(KeyEvent.VK_A)) {
			sidewaysMovement += speed;
		}
		if (keysPressed.contains(KeyEvent.VK_RIGHT)) {
			cameraRotation -= speed;
		}
		if (keysPressed.contains(KeyEvent.VK_LEFT)) {
			cameraRotation += speed;
		}
		if (keysPressed.contains(KeyEvent.VK_X)) {
			elevation -= speed;
		}
		if (keysPressed.contains(KeyEvent.VK_SPACE)) {
			elevation += speed;
		}
		if (sidewaysMovement != 0 || forwardMovement != 0 || elevation != 0) {
			if (keysPressed.contains(KeyEvent.VK_SHIFT)) {
				// rotate and pan
                view.rotateCamera(-forwardMovement, -sidewaysMovement, 0);
			} else {
				// move and strafe
				view.moveAlongLocalAxis(new Point3D(sidewaysMovement, elevation, forwardMovement));	
			}
		}
		if (cameraRotation != 0 && selectedObject != null) {
			view.setCameraRotationAround(cameraRotation, selectedObject.getPosition(), view.getCamera());
		}
	}

    @Override
    public void mouseMoved(MouseEvent e) {
        this.mouseLocation = e.getPoint();
    }

    @Override
	public void mouseClicked(MouseEvent e) {
		canvas.requestFocus();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		dragBase = e.getPoint();
		cameraAtDragStart = view.getCamera();
		rotate = e.getButton() == MouseEvent.BUTTON3;
		if (selectedObject != null) {
			moverAtDragStart = selectedObject.getMover();
			rotatorAtDragStart = selectedObject.getRotator();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		final Point p = e.getPoint();
		int dx = dragBase.x - p.x;
		int dy = dragBase.y - p.y;
		movementAffectsCamera = e.isControlDown();
		movementAffectsObject = !movementAffectsCamera;
		boolean rotateAroundSelectedObject = e.isAltDown();
		if (rotateAroundSelectedObject && selectedObject != null) {
			view.setCameraRotationAround(dx, selectedObject.getPosition(), cameraAtDragStart);
		} else if (rotate) {
			if (movementAffectsCamera) {
				view.setCamera(cameraAtDragStart.rotate(dy, -dx, 0));
			}
			if (movementAffectsObject && selectedObject != null) {
				final Rotator newRotator = new Rotator(-dy % 360, dx % 360, 0);
				// skip the rotation around the apparent axis.
				// move to quarternions instead, to avoid gimbal lock, and take it from there.
				// this is because we don't actually know where the origin of the object is anyway,
				// so even if we can figure it out, the rotation might still look weird, so lets just skip it.
				// If we do want to do it again, this is a pretty good resource:
				// http://stackoverflow.com/questions/7724840/is-it-possible-to-rotate-an-object-around-its-own-axis-and-not-around-the-base-c
				// likewise this: it's essentially rotation around an arbitrary vector, and we have to find that vector:
				//https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle

				Rotator r = rotatorAtDragStart.merge(newRotator);
				world.rotate(selectedObject, r);
			}
		} else {
			if (movementAffectsCamera) {
				view.setCamera(cameraAtDragStart.move(-dx, -dy, 0));
			}
			if (movementAffectsObject && selectedObject != null) {
				// this is another object vs. object at start issue.
				// obviously we can't just clone the (set of) object(s) we've moving, so we have to do something else
				Mover m = moverAtDragStart.merge(new Mover(dx, dy, 0, false));
				world.move(selectedObject, m);
			}
		}
		e.consume(); // doing this means we don't have any trailing input
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		movementAffectsCamera = e.isControlDown();
		movementAffectsObject = !movementAffectsCamera;
		// note: scrolling "up" means movement is negative
		final int movement = e.getWheelRotation() * 4;

		// this is just camera movement. We want to be able to do world movement as well
		if (movementAffectsCamera) {
			view.moveCamera(0, 0, movement);
		}
		if (movementAffectsObject && selectedObject != null) {
			world.move(selectedObject, selectedObject.getMover().merge(new Mover(0, 0, movement, false)));
		}
	}

    public void setSelectedObject(WorldObject selectedObject) {
		this.selectedObject = selectedObject;
	}

    public WorldObject getSelectedObject() {
        return selectedObject;
    }

    public Point getMouseLocation() {
        return mouseLocation;
    }

	public boolean shouldTakeScreenshot() {
		return shouldTakeScreenshot.getAndSet(false);
	}
}
