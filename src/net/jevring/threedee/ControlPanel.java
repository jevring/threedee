/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.fileformats.ObjectLoader;
import net.jevring.threedee.fileformats.ObjectParserException;
import net.jevring.threedee.primitives.Group;
import net.jevring.threedee.primitives.Object3D;
import net.jevring.threedee.primitives.Texture;
import net.jevring.threedee.renderer.painter.DebugPainterType;
import net.jevring.threedee.renderer.shader.gouraud.BlendingMode;
import net.jevring.threedee.renderer.texturefilter.TextureFilter;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class ControlPanel extends JPanel {
	public static final int DEFAULT_SCALE = 100;
	private final DefaultMutableTreeNode loadedObjectsRoot;
	private final DefaultTreeModel loadedObjectsTreeModel;
	private final JTree loadedObjectsTree;

	public ControlPanel(final World world,
	                    final View view,
	                    final InputController inputController,
	                    final ObjectLoader objectLoader,
	                    final Engine engine, JFrame frame) {
		loadedObjectsRoot = new DefaultMutableTreeNode();
		loadedObjectsTreeModel = new DefaultTreeModel(loadedObjectsRoot);
		loadedObjectsTree = new JTree(loadedObjectsTreeModel);
		loadedObjectsTree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {

				DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
				Object o = node.getUserObject();
				if (o != null) {
					if (o instanceof Object3D) {
						inputController.setSelectedObject((Object3D) o);
					} else if (o instanceof Group) {
						inputController.setSelectedObject((Group) o);
					}
				}
			}
		});

		JScrollPane loadedObjectsScrollPane = new JScrollPane(loadedObjectsTree);
		loadedObjectsScrollPane.setBorder(BorderFactory.createTitledBorder("Loaded objects"));

		JComboBox<File> potentialModels = new JComboBox<>();
		potentialModels.setBorder(BorderFactory.createTitledBorder("Potential Objects"));
		potentialModels.setModel(new DefaultComboBoxModel<>(objectLoader.getPotentialModels().toArray(new File[objectLoader.getPotentialModels().size()])));
		potentialModels.setSelectedIndex(-1);
		potentialModels.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					try {
						File file = (File) e.getItem();
						Group loadedObjects = objectLoader.load(file);
						world.add(loadedObjects, DEFAULT_SCALE);
						loadObjectInTree(loadedObjects);
					} catch (IOException | ObjectParserException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		JButton resetCamera = new JButton("Reset camera");
		resetCamera.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				view.resetCamera();
			}
		});

		JComboBox<RenderingMode> renderingMode = new JComboBox<>(RenderingMode.values());
		renderingMode.setBorder(BorderFactory.createTitledBorder("Rendering Mode"));
		renderingMode.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				engine.setRenderingMode((RenderingMode) e.getItem());
			}
		});
		renderingMode.setSelectedItem(engine.getRenderingMode());

		JComboBox<TextureFilter.Type> textureFilter = new JComboBox<>(TextureFilter.Type.values());
		textureFilter.setBorder(BorderFactory.createTitledBorder("Texture filtering"));
		textureFilter.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				engine.setTextureFilter((TextureFilter.Type) e.getItem());
			}
		});
		textureFilter.setSelectedItem(engine.getTextureFilter());
		
		JComboBox<BlendingMode> blendingMode = new JComboBox<>(BlendingMode.values());
		blendingMode.setBorder(BorderFactory.createTitledBorder("Blending mode"));
		blendingMode.addItemListener(e -> engine.setBlendingMode((BlendingMode) e.getItem()));
		blendingMode.setSelectedItem(engine.getBlendingMode());

		JButton lookAtSelectedObject = new JButton("Look at selected object");
		lookAtSelectedObject.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (inputController.getSelectedObject() != null) {
					view.lookAt(inputController.getSelectedObject().getPosition());
				}
			}
		});

		final JCheckBox singleThreaded = new JCheckBox("Single threaded");
		singleThreaded.setSelected(engine.isSingleThreaded());
		singleThreaded.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				engine.setSingleThreaded(singleThreaded.isSelected());
			}
		});
		final JCheckBox limitSpeed = new JCheckBox("Limit speed");
		limitSpeed.setSelected(engine.isLimitSpeed());
		limitSpeed.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				engine.setLimitSpeed(limitSpeed.isSelected());
			}
		});

		final JSlider fov = new JSlider(1, 179, (int) view.getFieldOfView());
		fov.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				view.setFieldOfView(fov.getValue());
			}
		});
		fov.setBorder(BorderFactory.createTitledBorder("Field of view (in degrees)"));
		fov.setLabelTable(fov.createStandardLabels(25));
		fov.setPaintLabels(true);

		final List<Path> skyBoxTextureFiles = new ArrayList<>();
		try {
			// todo: this isn't very elegant. Have a feature like the object loader that takes paths for this
			Files.walkFileTree(Paths.get("resources"), EnumSet.noneOf(FileVisitOption.class), 1, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					skyBoxTextureFiles.add(file);
					return super.visitFile(file, attrs);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		JComboBox<Path> skyboxTextures = new JComboBox<>();
		skyboxTextures.setBorder(BorderFactory.createTitledBorder("SkyBox textures"));
		skyboxTextures.setModel(new DefaultComboBoxModel<>(skyBoxTextureFiles.toArray(new Path[skyBoxTextureFiles.size()])));
		skyboxTextures.setSelectedIndex(-1);
		skyboxTextures.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					try {
						view.setSkyBoxTexture(new Texture(ImageIO.read(((Path) e.getItem()).toFile()), true));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		final JSlider hue = new JSlider(0, 100);
		hue.setBorder(BorderFactory.createTitledBorder("OSD Hue"));
		hue.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				view.setOnScreenDisplayHue(hue.getValue() / 100d);
			}
		});

		JComboBox<DebugPainterType> debugPainter = new JComboBox<>();
		debugPainter.setBorder(BorderFactory.createTitledBorder("Debug painter"));
		debugPainter.setModel(new DefaultComboBoxModel<>(DebugPainterType.values()));
		debugPainter.addActionListener(e -> {
			DebugPainterType selectedItem = (DebugPainterType) debugPainter.getSelectedItem();
			if (selectedItem != null) {
				view.setDebugPainter(selectedItem);
			}
		});
		
		JButton saveSettings = new JButton("Save settings");
		saveSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser(new File("."));
				fc.setSelectedFile(new File("./settings.properties"));
				int result = fc.showSaveDialog(ControlPanel.this);
				if (result == JFileChooser.APPROVE_OPTION) {
					Settings settings = new Settings(fc.getSelectedFile());
					System.out.println("Saving settings to: " + fc.getSelectedFile());
					settings.store(view, frame);
					
				}
			}
		});

		setLayout(new GridBagLayout());
		final Insets i = new Insets(1, 1, 1, 1);
		final int north = GridBagConstraints.NORTH;
		add(resetCamera,                new GridBagConstraints(0, 0, 1, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(saveSettings,               new GridBagConstraints(1, 0, 1, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(lookAtSelectedObject,       new GridBagConstraints(2, 0, 2, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(singleThreaded,             new GridBagConstraints(0, 1, 1, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(limitSpeed,                 new GridBagConstraints(1, 1, 1, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(debugPainter,               new GridBagConstraints(2, 1, 1, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(potentialModels,            new GridBagConstraints(0, 3, 2, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(skyboxTextures,             new GridBagConstraints(2, 3, 2, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(renderingMode,              new GridBagConstraints(0, 4, 2, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(textureFilter,              new GridBagConstraints(2, 4, 1, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(blendingMode,               new GridBagConstraints(3, 4, 1, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(fov,                        new GridBagConstraints(0, 5, 4, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(hue,                        new GridBagConstraints(0, 6, 4, 1, 0, 0, north, GridBagConstraints.HORIZONTAL, i, 2, 2));
		add(loadedObjectsScrollPane,    new GridBagConstraints(0, 7, 4, 1, 0, 1, north, GridBagConstraints.BOTH, i, 2, 2));
		add(new JLabel(),               new GridBagConstraints(0, 8, 4, 1, 0, 1, north, GridBagConstraints.BOTH, i, 2, 2));

		for (Group group : world.getGroups()) {
			loadObjectInTree(group);
		}
	}

	private void loadObjectInTree(Group loadedObjects) {
		// update the tree representation

		DefaultMutableTreeNode loadedObjectNode = new DefaultMutableTreeNode(loadedObjects);
		loadedObjectsTreeModel.insertNodeInto(loadedObjectNode, loadedObjectsRoot, loadedObjectsRoot.getChildCount());
		loadedObjectsRoot.add(loadedObjectNode);
		int i = 0;
		for (Object3D loadedObject : loadedObjects.getObjects()) {
			DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(loadedObject);
			loadedObjectsTreeModel.insertNodeInto(newChild, loadedObjectNode, i++);
			// must expand to parents, as it doesn't work for leaves, as per the documentation
			TreePath path = new TreePath(newChild.getPath()).getParentPath();
			loadedObjectsTree.expandPath(path);
			loadedObjectsTree.scrollPathToVisible(path);
		}
		loadedObjectsTree.setSelectionPath(new TreePath(loadedObjectNode.getPath()));
	}
}
