/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.primitives.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class SkyBox {
	private final Object3D box;
	private final Camera camera;

	/**
	 * Creates a skybox.
	 */
	public SkyBox(View view) {
		// the distance from the camera to the planes in the skybox.
		double d = view.getFarPlaneDistance() * 2;
		camera = view.getCamera();

		// the corners that will make up the sky box
		Point3D[] corners = new Point3D[8];
		corners[0] = camera.move(d, d, -d);
		corners[1] = corners[0].moveX(-2 * d);
		corners[2] = corners[1].moveZ(+2 * d);
		corners[3] = corners[2].moveX(+2 * d);
		// this is just a layer under the first
		corners[4] = corners[0].moveY(-2 * d);
		corners[5] = corners[1].moveY(-2 * d);
		corners[6] = corners[2].moveY(-2 * d);
		corners[7] = corners[3].moveY(-2 * d);

		// ok, THIS is how you do skybox graphics:
		// https://stackoverflow.com/questions/8116175/texture-coordinates-for-a-skybox

		double tt = 2d / 3d;
		double ot = 1d / 3d;

		Vertex[] vertices = new Vertex[14];
		// it looks like these coordinates are inverted, but that's because we see the texture
		// from the INSIDE. 
		vertices[0] = v(corners, 0, 0.5, tt);
		vertices[1] = v(corners, 1, 0.25, tt);
		vertices[2] = v(corners, 2, 0.25, 1);
		vertices[3] = v(corners, 3, 0.5, 1);
		vertices[4] = v(corners, 4, 0.5, ot);
		vertices[5] = v(corners, 5, 0.25, ot);
		vertices[6] = v(corners, 6, 0.25, 0);
		vertices[7] = v(corners, 7, 0.5, 0);
		vertices[8] = v(corners, 2, 1, tt); // 2'
		vertices[9] = v(corners, 2, 0, tt); // 2''
		vertices[10] = v(corners, 3, 0.75, tt); // 3'
		vertices[11] = v(corners, 6, 1, ot); // 6'
		vertices[12] = v(corners, 6, 0, ot); // 6''
		vertices[13] = v(corners, 7, 0.75, ot); // 7'


		List<Face> faces = new ArrayList<>();
		// the winding, if my sketch is correct, is COUNTER CLOCKWISE.
		// This is ONLY for the skybox, however, where the surface normals in camera space must be INVERTED

		// top
		faces.add(f(vertices, 2, 1, 0));
		faces.add(f(vertices, 0, 3, 2));

		// front
		faces.add(f(vertices, 1, 5, 4));
		faces.add(f(vertices, 4, 0, 1));

		// bottom
		faces.add(f(vertices, 5, 6, 7));
		faces.add(f(vertices, 7, 4, 5));

		// right
		faces.add(f(vertices, 0, 4, 13));
		faces.add(f(vertices, 13, 10, 0));

		// back
		faces.add(f(vertices, 10, 13, 11));
		faces.add(f(vertices, 11, 8, 10));

		// left
		faces.add(f(vertices, 9, 12, 5));
		faces.add(f(vertices, 5, 1, 9));

		box = new Object3D(faces, "SkyBox");
		MaterialBuilder materialBuilder = new MaterialBuilder();
		materialBuilder.setName("SkyBox");
		materialBuilder.setIlluminationModel(IlluminationModel.ColorOnAmbientOff);
		materialBuilder.setDiffuseColor(Color.WHITE);
		materialBuilder.setAmbientColor(Color.BLACK);
		materialBuilder.setSpecularColor(Color.WHITE);
		materialBuilder.setSpecularCoefficient(0.35f);
		materialBuilder.setTransparency(0);
		materialBuilder.setMapDiffuseColor(view.getSkyBoxTexture());
		box.setMaterial(materialBuilder.createMaterial());
	}

	private Face f(Vertex[] vertices, int a, int b, int c) {
		return new Face(vertices[a], vertices[b], vertices[c]);
	}

	private Vertex v(Point3D[] points, int p, double u, double v) {
		Point3D point = points[p];
		// set the normal to avoid calculating it later.
		// the normal points in towards the camera 
		return new Vertex(point, point.minus(camera.normal), new TextureCoordinate(u, v));
	}

	public Object3D getBox() {
		return box;
	}
}

