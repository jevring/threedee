/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.transformations;

import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.primitives.Vertex;

/**
 * @author markus@jevring.net
 */
public class Rotator {
	public final double rx;
	public final double ry;
	public final double rz;
	private final double cx;
	private final double cy;
	private final double cz;
	private final double sx;
	private final double sy;
	private final double sz;

	public Rotator(double rx, double ry, double rz) {
		this.rx = (rx + 360) % 360;
		this.ry = (ry + 360) % 360;
		this.rz = (rz + 360) % 360;
		//System.out.printf("rx = %f, ry = %f, rz = %f\n", rx, ry, rz);
		cx = Math.cos(Math.toRadians(rx));
		cy = Math.cos(Math.toRadians(ry));
		cz = Math.cos(Math.toRadians(rz));

		sx = Math.sin(Math.toRadians(rx));
		sy = Math.sin(Math.toRadians(ry));
		sz = Math.sin(Math.toRadians(rz));
	}

	public Vertex rotate(Vertex v) {
		return v.moveTo(rotate(v.p));
	}

	public Point3D rotate(Point3D p) {
		double x = p.x;
		double y = p.y;
		double z = p.z;

		// todo: this code is simply taken without being understood from some other place.
		// it would be nice to have some theory about this, but all rotation is done with
		// fucking matrices, which I know nothing about.
		// http://westhoffswelt.de/projects/dynamic_canvas_demo.html

		// todo: try to understand how this actually works. Should be easier than the camera transform

		// fairly certain this is something like spherical-to-carthesian conversion.
		// alternatively the other way around.

		// rotate around the x axis
		double xx = cz * x + sz * y;
		double xy = -sz * x + cz * y;
		x = xx;
		y = xy;

		// rotate around the y axis
		double yy = cx * y + -sx * z;
		double yz = sx * y + cx * z;
		y = yy;
		z = yz;

		// rotate around the z axis
		double zx = cy * x + sy * z;
		double zz = -sy * x + cy * z;
		x = zx;
		z = zz;

		return new Point3D(x, y, z);
	}

	/**
	 * Merges the effects of the two rotators. This is done so further rotator applications
	 * continue the rotation, rather than start from 0;
	 *
	 * @param rotator the rotator to merge with
	 * @return a new rotator containing the combined rotation of both
	 */
	public Rotator merge(Rotator rotator) {
		return new Rotator(rx + rotator.rx, ry + rotator.ry, rz + rotator.rz);
	}

	@Override
	public String toString() {
		return "Rotator{" +
				"rx=" + rx +
				", ry=" + ry +
				", rz=" + rz +
				'}';
	}
}
