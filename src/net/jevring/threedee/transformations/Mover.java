/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.transformations;

import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.primitives.Vertex;

/**
 * @author markus@jevring.net
 */
public class Mover {
	public final double x;
	public final double y;
	public final double z;
	public final boolean absolute;

	public Mover(double x, double y, double z, boolean absolute) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.absolute = absolute;
	}

	public Vertex move(Vertex v) {
		return v.moveTo(v.p.move(x, y, z));
	}

	public Point3D move(Point3D p) {
		return p.move(x, y, z);
	}

	public Mover merge(Mover mover) {
		return new Mover(x + mover.x, y + mover.y, z + mover.z, absolute || mover.absolute);
	}

	@Override
	public String toString() {
		return "Mover{" +
				"x=" + x +
				", y=" + y +
				", z=" + z +
				", absolute=" + absolute +
				'}';
	}
}
