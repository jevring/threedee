/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee;

import net.jevring.threedee.frustum.Frustum;
import net.jevring.threedee.primitives.Point3D;
import net.jevring.threedee.primitives.Texture;
import net.jevring.threedee.renderer.painter.DebugPainterType;
import net.jevring.threedee.transformations.Rotator;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.util.ConcurrentModificationException;

import static java.lang.Math.*;

/**
 * A view of the world. The view contains the camera, and knows the size of the viewport.<br>
 * NOTE: <b>ALL</b> methods that modify the view must be synchronized. This is because the {@link Engine}
 * will synchronize on the view and the world to avoid getting modifications during rendering,
 * which could, at least, lead to tearing, but at worst, a {@link ConcurrentModificationException}.
 *
 * @author markus@jevring.net
 */
public class View {
    private SkyBox skyBox;
    private int width;
    private int height;
    private Camera camera;
    /**
     * Horizontal field of view, in degrees.
     * todo: should we change to vertical, like in Hor+?
     * If we do, we'll need to use height, not width, for all the calculations
     * https://en.wikipedia.org/wiki/Field_of_view_in_video_games
     */
    private double fieldOfView;
    private double focalLength;
    private final double farPlaneDistance = 10000000;
    private final double nearPlaneDistance = 1;
    private Frustum frustum;
    private Texture skyBoxTexture;
	private double onScreenDisplayHue;
	private DebugPainterType debugPainter = DebugPainterType.None;

	public View() {
		try {
            this.skyBoxTexture = new Texture(ImageIO.read(getClass().getResourceAsStream("/Cube_Map_Diagram.jpg")), true);
        } catch (IOException e) {
            throw new IllegalStateException("Couldn't load skybox texture", e);
        }
        resetCamera();
    }

    public synchronized void resetCamera() {
	    // todo: shouldn't this reset to whatever view was specified on the command-line?
        // this camera is 600 units away from the origin, out towards the viewer
        camera = new Camera(0, 0, 600, 0, 0, 0);
        fieldOfView = 90;
        updateSkyBox();
        if (width != 0) {
            focalLength = 0; // set this to 0 to avoid triggering the camera move
            updateFocalLength();
            updateFrustum();
        }
    }

    public synchronized void setSize(Dimension size) {
        //System.out.println("Setting size to: " + size);
	    
	    // todo: if we grab the lock here, while we're trying to render, we get into a deadlock situation, strangely.
	    // I would expect one of the methods to simply finish, and then the other one could proceed, but for incomprehensible
	    // reasons, it just fucking stops executing.
	    // it doesn't matter if the renderer is multi-threaded or single-threaded.
	    // it only happens, strangely, when the view is VERY small. Any normal sized view works just fine.
	    // No idea why.
	    // todo: perhaps we should pause rendering when we do this. Alternatively, not allow resizing.
	    
	    
        this.width = size.width;
        this.height = size.height;
        updateFocalLength();
        updateFrustum();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Camera getCamera() {
        return camera;
    }

    /**
     * Projects a point in camera space in 3d onto the image plane, which is in 2d.
     *
     * @param d a point in camera space
     * @return a point in the image plane
     */
    public Point project(Point3D d) {
        // https://en.wikipedia.org/wiki/3D_projection#Perspective_projection
        // 'd' here is as specified in the link above, as the 'camera transform'
        // has already taken place

        // e is "the viewer's position relative to the display surface."
        // I don't really understand that. As a matter of fact, I don't understand it at all.
        // I'm assuming it has something to do with the image plane.

        double ez = focalLength;
        double ex = 0d; // as we're in the center of the projection, this is 0
        double ey = 0d; // as we're in the center of the projection, this is 0

        // for an orthographic projection, make ezDdz constant.
        double ezDdz = ez / d.z;
        //double ezDdz = -3;

        double bx = (d.x - ex) * ezDdz;
        double by = (d.y - ey) * ezDdz;

        return new Point((int) Math.round(bx), (int) Math.round(by));
    }

    /**
     * Translates a world coordinate to a camera space coordinate.
     *
     * @param p the point to translate
     * @return a point in the camera space
     */
    public Point3D toCameraCoordinates(Point3D p) {
        Camera c = camera;
        // https://en.wikipedia.org/wiki/3D_projection#Perspective_projection

        // the camera transform is a bitch...

        // todo: would be nice if I actually UNDERSTOOD the camera transform, rather than just applying it...

        // axial deltas
        double dpx = p.x - c.x;
        double dpy = p.y - c.y;
        double dpz = p.z - c.z;

        // camera angles
        double sinx = sin(toRadians(c.rx));
        double siny = sin(toRadians(c.ry));
        double sinz = sin(toRadians(c.rz));
        double cosx = cos(toRadians(c.rx));
        double cosy = cos(toRadians(c.ry));
        double cosz = cos(toRadians(c.rz));


        double sinzdpx = sinz * dpx;
        double sinzdpy = sinz * dpy;
        double sinydpz = siny * dpz;

        double coszdpy = cosz * dpy;
        double coszdpx = cosz * dpx;
        double cosydpz = cosy * dpz;

        double sinzdpyPcoszdpx = sinzdpy + coszdpx;
        double coszdpyMsinzdpx = coszdpy - sinzdpx;

        double dx = cosy * (sinzdpy + coszdpx) - sinydpz;
        double dy = sinx * (cosydpz + siny * sinzdpyPcoszdpx) + cosx * coszdpyMsinzdpx;
        double dz = cosx * (cosydpz + siny * sinzdpyPcoszdpx) - sinx * coszdpyMsinzdpx;

        return new Point3D(dx, dy, dz);
    }

    @SuppressWarnings("UnusedDeclaration") // kept for historical reasons
    private Point translateWithoutCameraAngle(Point3D p3d) {
        // actually, this is just the projection of a point in 3 dimensions to a point in 2 dimensions.
        // https://en.wikipedia.org/wiki/3D_projection#Diagram
        Camera c = camera;
        double az = c.z - p3d.z;
        double bz = focalLength;
        double ax = c.x - p3d.x;
        int bx = (int) Math.round(ax * (bz / az));
        double ay = c.y - p3d.y;
        int by = (int) Math.round(ay * (bz / az));

        return new Point((int) (bx + Math.round(width / 2d)), (int) (by + Math.round(height / 2d)));
    }

    public synchronized void moveAlongLocalAxis(Point3D movement) {
        // I knew it was something like this, but this pushed me over the top:
        // http://www.idevgames.com/forums/thread-2283-post-51235.html#pid51235
        Rotator r = new Rotator(camera.rx, camera.ry, camera.rz);
        Point3D rotatedMovement = r.rotate(movement);
        camera = camera.move(rotatedMovement.x, rotatedMovement.y, rotatedMovement.z);
        // new skybox because the camera location has changed
        updateSkyBox();
        updateFrustum();
    }

    public synchronized void setCameraRotationAround(int angle, Point3D p, Camera cameraAtDragStart) {
        // later, we'll do rotation around two angles, but for now, stick to one
        Point3D cameraToObject = cameraAtDragStart.minus(p);
        double cameraAngleAtStart = Math.atan2(cameraToObject.z, cameraToObject.x);

        //double d = c.distance(p);
        // with d defined as above, the distance to the object would increase proportional
        // to the object's y-coordinate for each rotation step
        double d = Math.sqrt(cameraToObject.x * cameraToObject.x + cameraToObject.z * cameraToObject.z);

        final double rotationDegrees = Math.toDegrees(cameraAngleAtStart) + angle;
        final double rotationRadians = Math.toRadians(rotationDegrees);
        // if the object is at the origin, we don't need p.x and p.z, but as soon as it has moved, they are needed
        double ax = Math.cos(rotationRadians) * d + p.x;
        double az = Math.sin(rotationRadians) * d + p.z;

        camera = camera.moveTo(ax, 0, az);
        updateSkyBox();

        // perform the look-at transform (after moving)
        lookAt(p);
    }

    public synchronized void lookAt(Point3D p) {
        // I finally found a proper treatment of lookat.
        // doesn't involve the up, right and forward vectors.
        // http://www.gamedev.net/topic/563474-lookat-functionality-for-fps-euler-angle-camera/

        // Basically, if I understand it correctly: we find a vector between the two points.
        // this vector has a certain angle in each dimension.
        // we take that angle and set the angle of the camera to that.
        // this works because one of the points is the camera

        Point3D v = camera.minus(p);
        double r = Math.sqrt(v.x * v.x + v.z * v.z); // this is essentially the "hypotenuse" in the triangle between the camera and the point
        // Math.atan2() doesn't care about degrees or radians, it's just a ratio.
        // this is also the reason we don't care about unit vectors either
        @SuppressWarnings("SuspiciousNameCombination")
        double yaw = Math.atan2(v.x, v.z);
        double pitch = Math.atan2(-v.y, r);
        camera = camera.setRotation(Math.toDegrees(pitch), Math.toDegrees(yaw), 0);

        // the reason "r" and "distance" here are different is because distance takes all 3 coordinates into account,
        // whereas r only takes two
        updateFrustum();
    }

    public void rotateCamera(int x, int y, int z) {
        camera = camera.rotate(x, y, z);
        updateFrustum();
    }

    public synchronized void setCamera(Camera camera) {
        this.camera = camera;
        updateSkyBox();
        updateFrustum();
    }

    public synchronized void setFieldOfView(double degrees) {
        fieldOfView = degrees;
        updateFocalLength();
        updateFrustum();
    }

    public synchronized void moveCamera(double x, double y, double z) {
        this.camera = camera.move(x, y, z);
        updateSkyBox();
        updateFrustum();
    }

    private void updateSkyBox() {
        // todo: see if we can get away with not RECREATING the skybox all the time
        this.skyBox = new SkyBox(this);
    }

    private void updateFrustum() {
        this.frustum = new Frustum(this);
    }

    /**
     * Sets the near plane distance as a function of viewpoint width and field of view.
     * This method must be invoked whenever we change either the viewpoint or the field of view.
     */
    private void updateFocalLength() {
        if (width == 0) {
            System.out.println("Width was 0, skipping");
            // the width isn't set yet, don't break anything...
            return;
        }
        // We must change the EFFECTIVE nearPlaneDistance. We do this by changing the distance, 
        // and by moving the camera accordingly, so that the near plane itself never moves w.r.t 
        // world coordinates.
        double oldFocalLength = focalLength;
        double newFocalLength = calculateFocalLength(fieldOfView);
        focalLength = newFocalLength;
        if (oldFocalLength != 0) {
            // only move the camera if this isn't the first time
            moveAlongLocalAxis(new Point3D(0, 0, -(oldFocalLength - newFocalLength)));
        }
        // setting the fov to 180 should set the near plane distance to 0
        // setting the fov to   0 should set the near plane distance to Infinity
        //System.out.println("Setting the fov to " + fieldOfView + " degrees and width to " + width + " changes the near plane distance to " + nearPlaneDistance + " with camera at " + camera.scale(1));
    }

    private double calculateFocalLength(double fieldOfView) {
        double angleInRadians = Math.toRadians(fieldOfView);
        double halfTheAngle = angleInRadians / 2.0;
        double halfTheWidth = width / 2.0;
        return halfTheWidth / Math.tan(halfTheAngle);
    }

    public double getFieldOfView() {
        return fieldOfView;
    }

    public double getFarPlaneDistance() {
        return farPlaneDistance;
    }

    public double getNearPlaneDistance() {
        return nearPlaneDistance;
    }

    public Frustum getFrustum() {
        return frustum;
    }

    public SkyBox getSkyBox() {
        return skyBox;
    }

    public Texture getSkyBoxTexture() {
        return skyBoxTexture;
    }

    public void setSkyBoxTexture(Texture skyBoxTexture) {
        this.skyBoxTexture = skyBoxTexture;
        updateSkyBox();
    }

	public void setOnScreenDisplayHue(double onScreenDisplayHue) {
		this.onScreenDisplayHue = onScreenDisplayHue;
	}

	public double getOnScreenDisplayHue() {
		return onScreenDisplayHue;
	}

	public DebugPainterType getDebugPainter() {
		return debugPainter;
	}

	public void setDebugPainter(DebugPainterType debugPainter) {
		this.debugPainter = debugPainter;
	}
}
