/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.fileformats.ply;

import net.jevring.threedee.primitives.*;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class PLYLoader {
	/**
	 * Loads a file found at {@code path} into the world.
	 * Currently only supports .ply files.
	 *
	 * @param path the path where we can find the object(s)
	 */
	public Group load(File path) throws IOException {
		//https://en.wikipedia.org/wiki/PLY_%28file_format%29
		try (FileInputStream fis = new FileInputStream(path)) {
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line;
			boolean endOfHeader = false;
			boolean foundPly = false;
			String format = null;
			String version = null;
			int numberOfVertices = -1;
			int numberOfFaces = -1;
			int numberOfFaceProperties = 0;
			boolean readingVertexProperties = false;
			boolean readingFaceProperties = false;
			List<String> vertexProperties = new ArrayList<>();
			// start by reading the header
			while ((line = br.readLine()) != null) {
				if (line.startsWith("comment")) continue;
				if (line.startsWith("obj_info")) continue;
				if ("ply".equalsIgnoreCase(line)) {
					foundPly = true;
					continue;
				}
				if (foundPly) {
					if (line.startsWith("format")) {
						String[] formatAndVersion = line.split("\\s+");
						format = formatAndVersion[1];
						version = formatAndVersion[2];
					} else if (line.equals("end_header")) {
						endOfHeader = true;
						/*
						if (numberOfVertexProperties != 3) {
							throw new IllegalArgumentException("We only support 3-dimensional coordinates");
						}
						*/
						break;
					} else if (line.startsWith("element vertex")) {
						String[] elementNameFormat = line.split("\\s+");
						numberOfVertices = Integer.parseInt(elementNameFormat[2]);
						readingVertexProperties = true;
						readingFaceProperties = false;
					} else if (line.startsWith("element face")) {
						String[] elementNameFormat = line.split("\\s+");
						numberOfFaces = Integer.parseInt(elementNameFormat[2]);
						readingFaceProperties = true;
						readingVertexProperties = false;
					} else if (line.startsWith("property")) {
						String[] parts = line.split("\\s+");
						if ("list".equalsIgnoreCase(parts[1])) {
							String typeOfEntriesNumber = parts[2];
							String listEntriesDataType = parts[3];
						} else {
							// we're not going to care about property type, though, we're going to store it all in doubles...
							String propertyType = parts[1];
							String propertyName = parts[2];

							if (readingVertexProperties) {
								// define a vertex
								vertexProperties.add(propertyName);
							} else if (readingFaceProperties) {
								numberOfFaceProperties++;
							}
						}
					}
				}
			}
			// done reading the header, read the body
			List<Vertex> vertices = new ArrayList<>(numberOfVertices);
			List<Face> faces = new ArrayList<>(numberOfFaces);
			int numberOfVerticesRead = 0;
			int numberOfFacesRead = 0;
			while ((line = br.readLine()) != null) {
				// read the data
				if (numberOfVerticesRead < numberOfVertices) {
					String[] vertexData = line.split("\\s+");
					if (vertexData.length != vertexProperties.size()) {
						throw new IllegalArgumentException(vertexProperties.size() + " vertex properties expected but found " + vertexData.length + " in " + path.getAbsolutePath());
					}
					Map<String, String> structuredVertexData = new HashMap<>();
					for (int i = 0; i < vertexProperties.size(); i++) {
						String propertyName = vertexProperties.get(i);
						structuredVertexData.put(propertyName, vertexData[i]);
					}

					Vertex v = buildVertex(structuredVertexData);
					vertices.add(v);
					numberOfVerticesRead++;
				} else if (numberOfFacesRead < numberOfFaces) {
					List<Vertex> vs = new ArrayList<>();
					String[] vertexIndexes = line.split("\\s+");
					int numberOfValues = Integer.parseInt(vertexIndexes[0]);
					for (int i = 1; i < vertexIndexes.length; i++) {
						String vertexIndex = vertexIndexes[i];
						int vi = Integer.parseInt(vertexIndex);
						vs.add(vertices.get(vi));
					}

					faces.addAll(Face.splitPolygon(vs));
					numberOfFacesRead++;
				} else {
					throw new IllegalArgumentException("There shouldn't be any more data, but we got: " + line);
				}
			}

			return new Group(new Object3D(faces, path.getName()));
		}
	}

	private Vertex buildVertex(Map<String, String> structuredVertexData) {
		String sx = structuredVertexData.get("x");
		String sy = structuredVertexData.get("y");
		String sz = structuredVertexData.get("z");
		String sa = structuredVertexData.get("alpha");
		String sr = structuredVertexData.get("red");
		String sg = structuredVertexData.get("green");
		String sb = structuredVertexData.get("blue");
		double x;
		double y;
		double z;
		int a = 255;
		int r = 0;
		int g = 0;
		int b = 0;
		if (sx != null && sy != null && sz != null) {
			x = Double.parseDouble(sx);
			y = Double.parseDouble(sy);
			z = Double.parseDouble(sz);
			if (sa != null) {
				// default alpha
				a = Integer.parseInt(sa);
			}
			if (sr != null && sg != null && sb != null) {
				// we either have all or no colors
				r = Integer.parseInt(sr);
				g = Integer.parseInt(sg);
				b = Integer.parseInt(sb);
			}
			return new Vertex(new Point3D(x, y, z), new Color(r, g, b, a));
		} else {
			throw new IllegalArgumentException("Data does not describe point in space: x = " + sx + ", y = " + sy + ", z = " + sz);
		}
	}
}
