/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.fileformats;

import net.jevring.threedee.fileformats.obj.OBJLoader;
import net.jevring.threedee.fileformats.ply.PLYLoader;
import net.jevring.threedee.primitives.Group;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Loads models of various formats into something that the engine understands.<br>
 * Currently supports <code>.obj/.mtl</code> and <code>.ply</code> files
 *
 * @author markus@jevring.net
 */
public class ObjectLoader {
	private final List<File> directories = new ArrayList<>();
	private final PLYLoader plyLoader = new PLYLoader();
	private final OBJLoader objLoader = new OBJLoader();

	public void addPotentialDirectory(File directory) {
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException(directory.getAbsolutePath() + " is not a directory");
		}
		directories.add(directory);
	}

	public List<File> getPotentialModels() {
		List<File> files = new ArrayList<>();
		for (File directory : directories) {
			Collections.addAll(files, directory.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					String suffix = file.getName().substring(file.getName().lastIndexOf('.') + 1);
					return "ply".equalsIgnoreCase(suffix) || "obj".equalsIgnoreCase(suffix);
				}
			}));
		}
		return files;
	}

	/**
	 * Loads a model file and returns the objects found therein.
	 *
	 * @param file the file to load
	 * @return a list of objects
	 * @throws IOException if the file couldn't be read
	 * @throws ObjectParserException if the file was malformed
	 */
	public Group load(File file) throws IOException, ObjectParserException {
		String suffix = file.getName().substring(file.getName().lastIndexOf('.') + 1);
		if ("ply".equalsIgnoreCase(suffix)) {
			return plyLoader.load(file);
		} else if ("obj".equalsIgnoreCase(suffix)) {
			return objLoader.load(file);
		} else {
			throw new IOException("Unknown format: " + file.getAbsolutePath());
		}
	}


}
