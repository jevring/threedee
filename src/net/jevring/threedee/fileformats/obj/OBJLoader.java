/*
 * Copyright (c) 2012, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.threedee.fileformats.obj;

import net.jevring.threedee.primitives.*;
import net.jevring.threedee.fileformats.ObjectParserException;

import javax.imageio.ImageIO;
import javax.imageio.spi.IIORegistry;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Loads .obj files, as well as any .mtl material files referenced in the .obj file.
 *
 * <p>http://www.martinreddy.net/gfx/3d/OBJ.spec</p>
 *
 * <p>http://local.wasp.uwa.edu.au/~pbourke/dataformats/mtl/</p>
 *
 * <p>https://en.wikipedia.org/wiki/Wavefront_.obj_file
 *
 * @author markus@jevring.net
 */
public class OBJLoader {
	private final Pattern facePattern = Pattern.compile("(-?\\d+)(?:/-?(\\d+))?(?:/-?(\\d+))?");
	private final Map<String, Material> materials = new HashMap<>();
	private final Map<File, Texture> textures = new HashMap<>();

	public OBJLoader() {
		IIORegistry iioRegistry = IIORegistry.getDefaultInstance();
		try {
			iioRegistry.registerServiceProvider(Class.forName("com.realityinteractive.imageio.tga.TGAImageReaderSpi").newInstance());
		} catch (Exception e) {
			// this can happen if the class isn't found.
			// normally it will be, but just in case we forget something
			e.printStackTrace();
		}
		System.out.println("Supported texture formats: " + Arrays.toString(ImageIO.getReaderFormatNames()));
	}

	/**
	 * Loads a .obj file and returns the objects found therein.
	 *
	 * @param path the file to load
	 * @return a list of objects
	 * @throws IOException if the file couldn't be read
	 * @throws ObjectParserException if the file was malformed
	 */
	public Group load(File path) throws IOException, ObjectParserException {
		List<Object3D> objects = new ArrayList<>();

		// global items are referenced by absolute numbers, starting at 1
		List<Point3D> globalVertices = new ArrayList<>();
		List<Point3D> globalVertexNormals = new ArrayList<>();
		List<TextureCoordinate> globalTextureCoordinates = new ArrayList<>();

		// local items are references by relative negative numbers,
		// and are discarded whenever a face is encountered
		List<Point3D> localVertices = new ArrayList<>();
		List<Point3D> localVertexNormals = new ArrayList<>();
		List<TextureCoordinate> localTextureCoordinates = new ArrayList<>();

		List<Face> faces = new ArrayList<>();
		String groupName = null;
		String objectName = null;
		Material material = null;
		boolean smoothing = false;
		long s = System.currentTimeMillis();
		long s1 = System.currentTimeMillis();
		try (FileInputStream fis = new FileInputStream(path)) {
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line;
			while ((line = br.readLine()) != null) {
				if (line.startsWith("#")) continue; // comment
				if (line.startsWith("vt")) {
					// vertex texture
					final TextureCoordinate textureCoordinate = newTextureCoordinate(line);
					globalTextureCoordinates.add(textureCoordinate);
					localTextureCoordinates.add(textureCoordinate);
				} else if (line.startsWith("vn")) {
					// vertex normal
					final Point3D vertexNormal = newPoint3D(line);
					globalVertexNormals.add(vertexNormal);
					localVertexNormals.add(vertexNormal);
				} else if (line.startsWith("vp")) {
				// parameter space vertex
				} else if (line.startsWith("v")) {
					// vertex
					// we can't start with the vertices, as they start with "v", but so do other
					// things, so they have to be matched last

					Point3D point = newPoint3D(line);
					globalVertices.add(point);
					localVertices.add(point);
				} else if (line.startsWith("f")) {
					// face
					String[] parts = line.split("\\s+");
					List<Vertex> faceVertices = new ArrayList<>(parts.length - 1);
					// start at 1 to skip the thing the line starts with
					for (int i = 1; i < parts.length; i++) {
						String part = parts[i];
						Matcher m = facePattern.matcher(part);
						Point3D v;
						TextureCoordinate vt = null;
						Point3D vn = null;
						if (m.matches()) {
							int vertexId = Integer.parseInt(m.group(1));
							if (vertexId < 0) {
								v = localVertices.get(localVertices.size() + vertexId);
							} else {
								v = globalVertices.get(vertexId - 1);
							}

							if (m.group(2) != null) {
								int textureId = Integer.parseInt(m.group(2));
								if (textureId < 0) {
									vt = localTextureCoordinates.get(localTextureCoordinates.size() + textureId);
								} else {
									vt = globalTextureCoordinates.get(textureId - 1);
								}
							}
							if (m.group(3) != null) {
								int normalId = Integer.parseInt(m.group(3));
								if (normalId < 0) {
									vn = localVertexNormals.get(localVertexNormals.size() + normalId);
								} else {
									vn = globalVertexNormals.get(normalId - 1);
								}
							}
							// the globalVertices references are 1-indexed
							// clear out local lists
							localTextureCoordinates.clear();
							localVertexNormals.clear();
							localVertices.clear();
							faceVertices.add(new Vertex(v, vn, vt));
						} else {
							throw new ObjectParserException("Face did not match pattern " + facePattern.pattern());
						}
					}
					faces.addAll(Face.splitPolygon(faceVertices));
				} else if (line.startsWith("g")) {
					if (groupName != null) {
						final Object3D o = new Object3D(faces, groupName);
						o.setMaterial(material);
						o.setSmoothing(smoothing);
						objects.add(o);
						long e = System.currentTimeMillis();
						System.out.println("Loaded object " + groupName + " in " + (e - s) + " ms");
						s = e; // reset
						faces = new ArrayList<>();
					}
					groupName = line.substring(2);
				} else if (line.startsWith("o")) {
					if (objectName != null) {
						// todo: actually both these cases need to check both group and object name.
						final Object3D o = new Object3D(faces, groupName);
						o.setMaterial(material);
						o.setSmoothing(smoothing);
						objects.add(o);
						long e = System.currentTimeMillis();
						System.out.println("Loaded object " + groupName + " in " + (e - s) + " ms");
						s = e; // reset
						faces = new ArrayList<>();
					}

					objectName = line.substring(2);
				} else if (line.startsWith("s")) {
					smoothing = !("off".equals(line.substring(2)) || "0".equals(line.substring(2)));
				} else if (line.startsWith("mtllib")) {
					loadMaterialLibrary(new File(path.getParent(), line.substring(7)));
				} else if (line.startsWith("usemtl")) {
					material = materials.get(line.substring(7));
				}
			}
		}
		if (!faces.isEmpty()) {
			final Object3D o = new Object3D(faces, groupName);
			o.setMaterial(material);
			o.setSmoothing(smoothing);
			objects.add(o);
			long e = System.currentTimeMillis();
			System.out.println("Loaded object " + groupName + " with " + o.getNumberOfFaces() + " faces and " + o.getNumberOfVertices() + " vertices in " + (e - s) + " ms");
		}
		long e = System.currentTimeMillis();
		int fs = 0;
		int vs = 0;
		for (Object3D object : objects) {
			fs += object.getNumberOfFaces();
			vs += object.getNumberOfVertices();
		}
		System.out.println("Loaded object file " + path.getAbsolutePath() + " in " + (e - s1) + " ms");

		return new Group(objects, path.getName());
	}

	private TextureCoordinate newTextureCoordinate(String line) {
		String[] parts = line.split("\\s+");
		double[] tcs = new double[parts.length - 1];
		for (int i = 1; i < parts.length; i++) {
			tcs[i - 1] = Double.parseDouble(parts[i]);
		}
		if (tcs.length == 3) {
			return new TextureCoordinate(tcs[0], tcs[1], tcs[2]);
		} else if (tcs.length == 2) {
			return new TextureCoordinate(tcs[0], tcs[1]);
		} else {
			return new TextureCoordinate(tcs[0]);
		}
	}

	@SuppressWarnings("ConstantConditions") // builder won't actually be null when we need it, unless the file is malformed
	private void loadMaterialLibrary(File f) throws IOException, ObjectParserException {
		System.out.println("Loading material library: " + f);
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)))) {
			String line;
			MaterialBuilder builder = null;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.startsWith("newmtl")) {
					if (builder != null) {
						Material material = builder.createMaterial();
						materials.put(material.getName(), material);
					}
					builder = new MaterialBuilder();
					builder.setName(line.substring(7));
				} else if (line.startsWith("Ns")){
					builder.setSpecularCoefficient(Float.parseFloat(line.substring(3)));
				} else if (line.startsWith("d")){
					builder.setTransparency(Float.parseFloat(line.substring(2)));
				} else if (line.startsWith("Tr")){
					builder.setTransparency(Float.parseFloat(line.substring(3)));
				} else if (line.startsWith("Tf")){
					//builder.setTransmissionFilter(Float.parseFloat(line.substring(3)));
				} else if (line.startsWith("illum")){
					builder.setIlluminationModel(IlluminationModel.values()[Integer.parseInt(line.substring(6))]);
				} else if (line.startsWith("Ka")){
					builder.setAmbientColor(newColor(line));
				} else if (line.startsWith("Kd")){
					builder.setDiffuseColor(newColor(line));
				} else if (line.startsWith("Ks")){
					builder.setSpecularColor(newColor(line));
				} else if (line.startsWith("map_Ka")){
					final String filename = line.substring(7);
					Texture texture = loadTexture(f, filename, builder.getClamp());
					builder.setMapAmbientColor(texture);
				} else if (line.startsWith("map_Kd")){
					final String filename = line.substring(7);
					Texture texture = loadTexture(f, filename, builder.getClamp());
					builder.setMapDiffuseColor(texture);
				} else if (line.startsWith("map_Ks")){
					// todo: there are other things than filename here. handle that later
					final String filename = line.substring(7);
					Texture texture = loadTexture(f, filename, builder.getClamp());
					builder.setMapSpecularColor(texture); // todo: change this to be something like setSpecularTexture()
				} else if (line.startsWith("map_Ns")){
					builder.setMapSpecularCoefficient(Float.parseFloat(line.substring(7)));
				} else if (line.startsWith("map_d")){
					builder.setMapTransparency(Float.parseFloat(line.substring(6)));
				} else if (line.startsWith("map_Tr")){
					builder.setMapTransparency(Float.parseFloat(line.substring(7)));
				}
			}
			if (builder != null && !materials.containsKey(builder.getName())) {
				Material material = builder.createMaterial();
				materials.put(material.getName(), material);
			}
		} catch (NullPointerException e) {
			// this isn't something you'd normally catch, but it's cleaner than checking builder for null each time.
			throw new ObjectParserException("Material file was malformed", e);
		}
	}

	private Texture loadTexture(File mtlFile, String textureFileName, boolean clamp) throws IOException {
		final File input = new File(mtlFile.getParent(), textureFileName);
		Texture texture = textures.get(input);
		if (texture == null) {
			long s = System.currentTimeMillis();
			System.out.println("Loading texture " + input.getAbsolutePath());
			final BufferedImage image = ImageIO.read(input);
			long e = System.currentTimeMillis();
			System.out.println("Loaded texture " + input.getAbsolutePath() + " in " + (e - s) + "ms");
			texture = new Texture(image, clamp);
			textures.put(input, texture);
		}
		return texture;
	}

	private Color newColor(String line) {
		String[] parts = line.split("\\s+");
		float r = Float.parseFloat(parts[1]);
		float g = Float.parseFloat(parts[2]);
		float b = Float.parseFloat(parts[3]);
		return new Color(r, g, b);
	}

	private Point3D newPoint3D(String line) {
		String[] xyz = line.split("\\s+");
		double x = Double.parseDouble(xyz[1]);
		double y = Double.parseDouble(xyz[2]);
		double z = Double.parseDouble(xyz[3]);
		return new Point3D(x, y, z);
	}
}
