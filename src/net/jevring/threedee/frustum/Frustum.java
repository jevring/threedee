/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.threedee.frustum;

import net.jevring.threedee.Camera;
import net.jevring.threedee.View;
import net.jevring.threedee.primitives.Object3D;
import net.jevring.threedee.primitives.Plane;
import net.jevring.threedee.primitives.Point3D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A frustum represents the space in a scene visible from the camera.
 * Anything outside the frustum should not be rendered.
 *
 * @author markus@jevring.net
 */
public class Frustum {
	private final List<Plane> planes;
	private final List<Plane> skyboxPlanes;
	private final Plane near;
	private final Plane far;
	private final Plane right;
	private final Plane left;
	private final Plane top;
	private final Plane bottom;

	public Frustum(View view) {
		Camera c = view.getCamera();
		Point3D p = c.moveX(0); // this just gets the point out of the camera. It makes toString() easier to read.

		// https://en.wikipedia.org/wiki/Field_of_view_in_video_games

		double fov = Math.toRadians(view.getFieldOfView());
		double farPlaneDistance = view.getFarPlaneDistance();
		double nearPlaneDistance = view.getNearPlaneDistance();
		double displayRatio = (double) view.getHeight() / (double) view.getWidth();

		// http://www.lighthouse3d.com/tutorials/view-frustum-culling/view-frustums-shape/
		double farPlaneWidth = 2.0 * Math.tan(fov / 2.0) * farPlaneDistance;
		double farPlaneHeight = farPlaneWidth * displayRatio;
		//double nearPlaneWidth = 2.0 * Math.tan(fov / 2.0) * nearPlaneDistance;
		//double nearPlaneHeight = nearPlaneWidth * displayRatio;
		/*
		System.out.printf("Near: %6.1f (%d) x %6.1f (%d) r: %1.1f Far: %6.1f x %6.1f\n",
		                  nearPlaneWidth,
		                  view.getWidth(),
		                  nearPlaneHeight,
		                  view.getHeight(),
		                  displayRatio,
		                  farPlaneWidth,
		                  farPlaneHeight);
		*/
		//System.out.println("farPlaneHeight = " + farPlaneHeight);
		//System.out.println("farPlaneWidth =  " + farPlaneWidth);

		// center of far plane
		Point3D fc = c.plus(c.normal.scale(farPlaneDistance));
		//System.out.println("fc = " + fc);
		// f: far
		// n: near
		// t: top
		// b: bottom
		// l: left
		// r: right
		Point3D farUpOffset = c.up.scale(farPlaneHeight / 2.0);
		//System.out.println("farUpOffset = " + farUpOffset);
		Point3D farRightOffset = c.right.scale(farPlaneWidth / 2.0);
		//System.out.println("farRightOffset = " + farRightOffset);
		Point3D ftl = fc.plus(farUpOffset).minus(farRightOffset);
		Point3D ftr = fc.plus(farUpOffset).plus(farRightOffset);
		Point3D fbl = fc.minus(farUpOffset).minus(farRightOffset);
		Point3D fbr = fc.minus(farUpOffset).plus(farRightOffset);

		// center of near plane
		Point3D nc = c.plus(c.normal.scale(nearPlaneDistance));
		//System.out.println("nc = " + nc);
/*		
		Point3D nearUpOffset = up.scale(nearPlaneHeight / 2.0);
		Point3D nearRightOffset = right.scale(nearPlaneWidth / 2.0);
		Point3D ntl = nc.plus(nearUpOffset).minus(nearRightOffset); 
		Point3D ntr = nc.plus(nearUpOffset).plus(nearRightOffset);
		Point3D nbl = nc.minus(nearUpOffset).minus(nearRightOffset);
		Point3D nbr = nc.minus(nearUpOffset).plus(nearRightOffset);
*/
		this.near = new Plane(nc, c.normal.scale(-1));
		this.far = new Plane(fc, c.normal);

		// http://ocw.mit.edu/ans7870/18/18.013a/textbook/HTML/chapter05/section04.html
		// You can compute a normal to Q by taking the cross product (P2 - P1)(P3 - P1).

		// the three points in the plane are p, and two other adjacent corners.
		// right: p, fbr, ftr
		// left: p, ftl, fbl
		// top: p, ftl, ftr
		// bottom: p, fbr, fbl
		Plane rightByCoordinates = fromCoordinates(p, fbr, ftr);
		Plane leftByCoordinates = fromCoordinates(p, ftl, fbl);
		// todo: top and bottom are confused. why? the effect is essentially fine, but they should be correct. Have I inverted an axis?
		Plane topByCoordinates = fromCoordinates(p, ftr, ftl);
		Plane bottomByCoordinates = fromCoordinates(p, fbl, fbr);

		// There's an absolutely brilliant observation about points in the plane towards the end of this:
		// http://www.lighthouse3d.com/tutorials/view-frustum-culling/geometric-approach-extracting-the-planes/
		// of course the camera point is a point in all the side planes! That's such an awesome optimization!
		// Unfortunately, I probably didn't understand it correctly, as applying it left me with useless frustum sides

		// Just to make life extra difficult, the author of http://www.lighthouse3d.com/tutorials/view-frustum-culling/geometric-approach-implementation/
		// decides to change all the names and the directions of the vectors when he moves from theory to implementation.
		
/*
		Plane rightOptimized = new Plane(p, c.up.crossProduct(fc.plus(farRightOffset).minus(c).toUnit()));
		Plane leftOptimized = new Plane(p, fc.minus(farRightOffset).minus(c).toUnit().crossProduct(c.up)); // flip so that normal point "in"
		Plane topOptimized = new Plane(p, c.up.crossProduct(fc.plus(farUpOffset).minus(c).toUnit()));
		Plane bottomOptimized = new Plane(p, fc.minus(farUpOffset).minus(c).toUnit().crossProduct(c.up)); // flip so that normal point "in"

		System.out.println("right");
		System.out.println(rightByCoordinates.normal);
		System.out.println(rightOptimized.normal);

		System.out.println("left");
		System.out.println(leftByCoordinates.normal);
		System.out.println(leftOptimized.normal);

		System.out.println("top");
		System.out.println(topByCoordinates.normal);
		System.out.println(topOptimized.normal);

		System.out.println("bottom");
		System.out.println(bottomByCoordinates.normal);
		System.out.println(bottomOptimized.normal);

		this.right = rightOptimized;
		this.left = leftOptimized;
		this.top = topOptimized;
		this.bottom = bottomOptimized;

*/
		this.right = rightByCoordinates;
		this.left = leftByCoordinates;
		this.top = topByCoordinates;
		this.bottom = bottomByCoordinates;
		this.planes = Arrays.asList(near, far, right, left, top, bottom);
		this.skyboxPlanes = Arrays.asList(near, right, left, top, bottom);
	}

	/**
	 * A list of all the planes except the far plane.
	 */
	public List<Plane> getSkyboxCullingPlanes() {
		return skyboxPlanes;
	}

	/**
	 * Gets a list of all the planes that the object needs to use to perform culling.
	 * If the object is completely inside the frustum, this will return an empty list.
	 * This is based on the bounding box.
	 *
	 * @param object the object to check
	 * @return a potentially empty list of planes behind which the object needs to be culled
	 */
	public CullingResult cull(Object3D object) {
		List<Point3D> boundingBox = object.getBoundingBox();
		List<Plane> cullingPlanes = new ArrayList<>();
		List<PlaneResult> results = new ArrayList<>();
		// it's possible that the object has all its points outside the frustum, 
		// like a sky-box, so we can't make any assumptions
		boolean completelyOutside = false;
		for (Plane plane : planes) {
			PlaneResult result = cullingRequired(boundingBox, plane);
			if (result == PlaneResult.Intersection) {
				cullingPlanes.add(plane);
			} else if (result == PlaneResult.Outside) {
				completelyOutside = true;
			}
			results.add(result);
		}
		return new CullingResult(completelyOutside, cullingPlanes, results);
	}

	private PlaneResult cullingRequired(List<Point3D> boundingBox, Plane plane) {
		// the issue on the top of this http://www.lighthouse3d.com/tutorials/view-frustum-culling/geometric-approach-testing-boxes/
		// is actually wrong. It's wrong, because the planes spread out indefinitely.
		// even though it looks like the right plane ends at the back plane, it doesn't.
		// as can be seen below, the plane stretches out, so the issue at the top would
		// actually be caught, because one point of the box is actually still on the 
		// inside of the plane, even though it's not inside the frustum.

		// If at least one point is outside, but not all, we'll give it to the culling algorithm.
		// If ALL corners are outside, we shouldn't cull it. We should disregard it

		int pointsOutside = 0;
		for (Point3D p : boundingBox) {
			if (plane.isInFrontOf(p)) {
				pointsOutside++;
			}
		}
		if (pointsOutside == boundingBox.size()) {
			return PlaneResult.Outside;
		} else if (pointsOutside == 0) {
			return PlaneResult.Inside;
		} else {
			return PlaneResult.Intersection;
		}
	}

	private Plane fromCoordinates(Point3D p1, Point3D p2, Point3D p3) {
		// http://ocw.mit.edu/ans7870/18/18.013a/textbook/HTML/chapter05/section04.html
		// You can compute a normal to Q by taking the cross product (P2 - P1)(P3 - P1).
		// This says the same: http://geomalgorithms.com/a04-_planes.html
		Point3D normal = p2.minus(p1).crossProduct(p3.minus(p1));
		return new Plane(p1, normal);
	}

	@Override
	public String toString() {
		return "Frustum{" +
				"\nnear=  " + near +
				"\nfar=   " + far +
				"\nright= " + right +
				"\nleft=  " + left +
				"\ntop=   " + top +
				"\nbottom=" + bottom +
				'}';
	}
}
